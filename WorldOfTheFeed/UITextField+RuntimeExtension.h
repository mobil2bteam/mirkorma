//
//  UITextField+MaxLength.h
//
//  Created by Johannes Luderschmidt on 04.02.15.
//

#import <UIKit/UIKit.h>

@interface UITextField (RuntimeExtension)
@property(nonatomic, assign) NSNumber* maxLength;
@property(nonatomic, assign) BOOL isEMailAddress;
@property(nonatomic, assign) BOOL isDecimalField;
@end
