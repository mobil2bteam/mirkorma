//
//  RPMenuItemCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPMenuItemCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;

@property (weak, nonatomic) IBOutlet UIView *borderView;

@end
