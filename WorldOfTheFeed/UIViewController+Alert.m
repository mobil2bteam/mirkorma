//
//  UIViewController+Alert.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+Alert.h"
#import <MBProgressHUD.h>

@implementation UIViewController (Alert)

- (void)showMessage:(NSString *)message withTitle:(NSString *)title{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^__nullable)(UIAlertAction *action))okHandler{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:okHandler];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^__nullable)(UIAlertAction *action))okHandler
   andRepeatHandler:(void (^__nullable)(UIAlertAction *action))repeatHandler{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:okHandler];
    UIAlertAction *repeatAction = [UIAlertAction
                                   actionWithTitle:@"Повторить"
                                   style:UIAlertActionStyleCancel
                                   handler:repeatHandler];
    [alertController addAction:repeatAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^__nullable)(UIAlertAction *action))repeatHandler
     andBackHandler:(void (^__nullable)(UIAlertAction *action))backHandler
{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Повторить"
                                   style:UIAlertActionStyleCancel
                                   handler:repeatHandler];
    [alertController addAction:cancelAction];
    UIAlertAction *backAction = [UIAlertAction
                                 actionWithTitle:@"Назад"
                                 style:UIAlertActionStyleDefault
                                 handler:backHandler];
    [alertController addAction:backAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^__nullable)(UIAlertAction *action))repeatHandler
{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Повторить"
                                   style:UIAlertActionStyleCancel
                                   handler:repeatHandler];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)addMBProgressHUD{
    if (![MBProgressHUD HUDForView:self.view]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

- (void)hideMBProgressHUD{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
