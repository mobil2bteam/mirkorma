//
//  RPOptions.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPQuestion;
@class RPLink;
@class RPUrl;
@class RPParam;
@class RPPhone;
@class RPSort;

@interface RPOptions : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *address_fiz;

@property (nonatomic, copy) NSString *address_jur;

@property (nonatomic, copy) NSString *email;

@property (nonatomic, strong) NSArray<RPPhone *> *phones;

@property (nonatomic, strong) NSArray<RPQuestion *> *questions;

@property (nonatomic, strong) NSArray<RPUrl *> *urls;

@property (nonatomic, strong) NSArray<RPSort *> *sort_type;

@property (nonatomic, assign) NSInteger cache;

@property (nonatomic, strong) NSDictionary *urlMethods;

- (void)configurePOSTMethods;

@end


@interface RPQuestion : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) RPLink *link;

@end


@interface RPPhone : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *value;

@end


@interface RPUrl : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, strong) NSArray<RPParam *> *param;

@end


@interface RPSort : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *sort;

@property (nonatomic, copy) NSString *dir;

@property (nonatomic, copy) NSString *name;

@end


