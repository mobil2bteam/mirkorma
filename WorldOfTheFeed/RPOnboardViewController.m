//
//  RPOnboardViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOnboardViewController.h"
#import "RPImageCollectionViewCell.h"
#import "RPRouter.h"

@interface RPOnboardViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *pagesCollectionView;

//массив изображений для первого показа
@property (strong, nonatomic) NSArray *imagesArray;

//текущая страница
@property (assign, nonatomic) NSInteger page;

@end

@implementation RPOnboardViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

#pragma mark - Methods

- (void)initialize{
    self.page = 0;
    self.imagesArray = @[@"Onboarding1", @"Onboarding2"];
}

#pragma mark - Actions

- (IBAction)nextButtonPressed:(id)sender {
    if (self.page + 1 < self.imagesArray.count) {
        self.page = self.page + 1;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.page inSection:0];
        [self.pagesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    } else {
        [RPRouter setUpStartController];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imagesArray.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
    cell.cellImageView.image = [UIImage imageNamed:self.imagesArray[indexPath.row]];
    cell.cellImageView.contentMode = UIViewContentModeScaleToFill;
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.pagesCollectionView.bounds), CGRectGetHeight(self.pagesCollectionView.bounds));
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

@end
