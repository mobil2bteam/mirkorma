//
//  RPAddressTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAddressTableViewCell.h"
#import "DBAddress+CoreDataProperties.h"

@implementation RPAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithAddress:(DBAddress *)address{
    self.fullAddressLabel.text = [NSString stringWithFormat:@"%@, %@, %@", address.index, address.city, address.fullAddress];
    self.regionLabel.text = address.region;
}

@end
