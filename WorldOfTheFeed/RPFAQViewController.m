//
//  RPFAQViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPFAQViewController.h"
#import "RPFAQTableViewCell.h"
#import "RPOptions.h"
#import "RPServerManager.h"
#import "RPMain.h"

@interface RPFAQViewController ()
@end

@implementation RPFAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Часто задаваемые вопросы";
    [self.faqTableView registerNib:[UINib nibWithNibName:@"RPFAQTableViewCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([RPFAQTableViewCell class])];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return APP_DELEGATE.options.questions.count;
}

- (RPFAQTableViewCell *)tableView:(UITableView *)tableView
              cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPFAQTableViewCell *cell = (RPFAQTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPFAQTableViewCell class]) forIndexPath:indexPath];
    cell.faqLabel.text = APP_DELEGATE.options.questions[indexPath.row].name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPLink *link = APP_DELEGATE.options.questions[indexPath.row].link;
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

@end
