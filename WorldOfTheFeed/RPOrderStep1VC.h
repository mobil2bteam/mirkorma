//
//  RPSelectDeliveryRegionViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderStep1VC : UIViewController

@property (strong, nonatomic) NSString *promoCode;

@end
