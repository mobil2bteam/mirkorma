//
//  NSDictionary+Convert.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDictionary+Convert.h"

@implementation NSDictionary (Convert)

- (NSString *)stringValue{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:0 error:&error];
    NSString *string =  [[NSString alloc] initWithData:jsonData
                                             encoding:NSUTF8StringEncoding];
    return string;
}

@end
