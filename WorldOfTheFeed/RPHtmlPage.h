//
//  RPHtmlPage.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPHtmlPage : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *url;

@end
