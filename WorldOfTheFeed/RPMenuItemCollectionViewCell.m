//
//  RPMenuItemCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMenuItemCollectionViewCell.h"

@implementation RPMenuItemCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.borderView.layer.masksToBounds = YES;
    self.borderView.layer.borderColor = RPLightBlueColor.CGColor;
    self.borderView.layer.borderWidth = 3.f;
    self.borderView.layer.cornerRadius = CGRectGetWidth(self.borderView.frame)/2;    
}

@end
