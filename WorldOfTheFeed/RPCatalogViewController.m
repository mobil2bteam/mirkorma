//
//  RPCatalogViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCatalogViewController.h"
#import "RPBrandHeaderCollectionViewCell.h"
#import "RPCategoryCollectionViewCell.h"
#import "RPImageCollectionViewCell.h"
#import "RPMain.h"
#import "RPString+Convert.h"
#import "RPString+MD5.h"
#import "RPCacheManager.h"
#import "RPCatalogList.h"
#import "RPSearchView.h"
#import "RPMenuView.h"
#import "UIViewController+Alert.h"
#import "RPMessage.h"
#import "RPRouter.h"


@implementation RPCatalogViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
    // if [Бренды] is selected tha show brandView and hide categoryView
    self.categoryLabel.text = APP_DELEGATE.main.menu.content[self.selectedContentIndex].name;
    self.catalogueID = APP_DELEGATE.main.menu.content[self.selectedContentIndex].ID;
    if ([APP_DELEGATE.main.menu.content[self.selectedContentIndex].name isEqualToString:@"Бренды"]) {
        self.brandView.hidden = NO;
        self.categoryView.hidden = YES;
    }
    // add shppingCartBarButton
    UIBarButtonItem *shoppingCartBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart"] style:UIBarButtonItemStylePlain target:self action:@selector(shoppingCartButtonTapped:)];
    self.navigationItem.rightBarButtonItem = shoppingCartBarButton;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.message) {
        [self showMessage:self.message.text withTitle:self.message.title];
        self.message = nil;
    }
    // update product's count in shopping cart
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu", (long)APP_DELEGATE.shoppingCartItemsCount];
}

#pragma mark - Actions

- (IBAction)shoppingCartButtonTapped:(id)sender {
    [RPRouter pushShoppingCartVC:self];
}

#pragma mark - Override UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.searchView showSegmentView:YES];
    if ([APP_DELEGATE.main.menu.content[self.selectedContentIndex].name isEqualToString:@"Бренды"]) {
        [self.searchView showSegmentView:NO];
    }
    self.searchView.hidden = NO;
    [self.searchBar setShowsCancelButton:YES];
    return YES;
}

#pragma mark - Methods

- (void)initialize{
    
    [self.brandCollectionView registerNib:[UINib nibWithNibName:@"RPBrandHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPBrandHeaderCollectionViewCell class])];
    
    [self.brandCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class])];
    
    [self.categoryCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPCategoryCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPCategoryCollectionViewCell class])];
    // add logo image to navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    
    // initialize variables
    self.menuView.selectedContentIndex = self.selectedContentIndex;
    self.menuView.delegate = self;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         [self.menuView setNeedsDisplay];
     } completion:nil];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - Properties

- (NSArray <NSArray<RPBrand *> *> *)sortedBrandArray{

    if (_sortedBrandArray) {
        return _sortedBrandArray;
    }

    _sortedBrandArray = [[NSMutableArray alloc]init];
    // сортируем бренды по алфавиту (если есть)
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortedBrands=[APP_DELEGATE.brands.brands sortedArrayUsingDescriptors:@[sort]];
    
    // применяем фильтр к отсортированным брендам
    NSMutableArray <NSMutableArray *> *lettersArray = [[NSMutableArray alloc]init];
    if (APP_DELEGATE.brands.brands.count) {
        for (RPBrand *brand in sortedBrands) {
            if (!lettersArray.count) { // if array is empty
                [lettersArray addObject:[[NSMutableArray alloc]init]];
                [[lettersArray lastObject] addObject:brand];
            } else {
                NSString *lastBrandFirstCharachter = [((RPBrand *)[[lettersArray lastObject]lastObject]).name substringToIndex:1];
                NSString *currentBrandFirstCharachter = [brand.name substringToIndex:1];
                if ([lastBrandFirstCharachter isNumeric] && [currentBrandFirstCharachter isNumeric]) {
                    [[lettersArray lastObject] addObject:brand];
                } else {
                    if ([((RPBrand *)[[lettersArray lastObject]lastObject]).name characterAtIndex:0] == [brand.name characterAtIndex:0] ){
                        [[lettersArray lastObject] addObject:brand];
                    } else { // if last brand
                        [lettersArray addObject:[[NSMutableArray alloc]init]];
                        [[lettersArray lastObject] addObject:brand];
                    }
                }
            }
        }
    }
    _sortedBrandArray = [lettersArray copy];
    return _sortedBrandArray;
}

#pragma mark - RPMenuViewProtocol

- (void)didSelectItemAtIndex:(NSInteger)index{
    self.categoryLabel.text = APP_DELEGATE.main.menu.content[index].name;
    self.selectedContentIndex = index;
    self.catalogueID = APP_DELEGATE.main.menu.content[self.selectedContentIndex].ID;
    // если выбран пункт [Бренды], то переключаем вью
    self.brandView.hidden = ![APP_DELEGATE.main.menu.content[index].name isEqualToString:@"Бренды"];
    self.categoryView.hidden = [APP_DELEGATE.main.menu.content[index].name isEqualToString:@"Бренды"];
    
    //если выбраны [Бренды], то переключаем поиск в режим [Искать везде]
    if ([APP_DELEGATE.main.menu.content[index].name isEqualToString:@"Бренды"]) {
        self.searchView.catalogueSegmentControl.selectedSegmentIndex = 0;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.categoryCollectionView reloadData];
    });
}

@end
