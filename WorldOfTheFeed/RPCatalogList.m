//
//  RPCatalogList.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCatalogList.h"

@implementation RPCatalogList

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPCatalog class] forKeyPath:@"catalog" forProperty:@"catalog"];
    }];
}

@end

@implementation RPCatalog

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"product_count",
                                          @"image",
                                          @"parent_id"]];
    }];
}

@end
