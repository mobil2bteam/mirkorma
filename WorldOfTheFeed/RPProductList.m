//
//  RPProductList.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductList.h"
#import "RPMain.h"

@implementation RPProductList

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPItem class] forKeyPath:@"products.items" forProperty:@"items"];
        [mapping hasMany:[RPFilter class] forKeyPath:@"products.filter.filters" forProperty:@"filters"];
        [mapping mapKeyPath:@"products.filter.catalogue_id" toProperty:@"catalogue_id"];
        [mapping mapKeyPath:@"products.filter.page" toProperty:@"page"];
        [mapping mapKeyPath:@"products.filter.count_page" toProperty:@"count_page"];
        [mapping mapKeyPath:@"products.title.text" toProperty:@"title"];
        [mapping mapKeyPath:@"products.filter.dir" toProperty:@"dir"];
        [mapping mapKeyPath:@"products.filter.sort" toProperty:@"sort"];
    }];
}

- (void)removeEmptyProducts{
    NSMutableArray<RPItem *> *filteredItems = [[NSMutableArray alloc]init];
    for (RPItem *item in self.items) {
        NSMutableArray<RPPrice *> *filteredPrices = [[NSMutableArray alloc]init];
        for (RPPrice *price in item.prices) {
            if (price.price) {
                [filteredPrices addObject:price];
            }
        }
        item.prices = [filteredPrices copy];
        if (item.prices.count) {
            [filteredItems addObject:item];
        }
    }
    self.items = [filteredItems copy];
}

@end

@implementation RPFilter

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPFilterItem class] forKeyPath:@"values" forProperty:@"values"];
        [mapping mapPropertiesFromArray:@[@"name"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


@implementation RPFilterItem

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"count",
                                          @"value",
                                          @"key",
                                          @"checked"]];
    }];
}

@end

