//
//  RPOrderCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderCollectionViewCell.h"
#import "RPMain.h"

@implementation RPOrderCollectionViewCell

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.masksToBounds = YES;
    self.layer.borderColor = RPLightBlueColor.CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 10.f;
}

- (void)configureCellWith:(RPPrice *)price{
    self.productCount = price.party_min;
    self.weightLabel.text = price.weight;
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)price.price];
    self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productCount];
    NSString *description = [NSString stringWithFormat:@"Можно заказать количество, кратное %ld шт.", (long)price.party_min];
    self.orderDescriptionLabel.text = description;
    if (price.old_price) {
        NSString *oldPrice = [NSString stringWithFormat:@"%ld руб.", (long)price.old_price];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:oldPrice];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        self.oldPriceLabel.attributedText = attributeString;
    }
}

- (IBAction)removeProductButtonPressed:(id)sender {
    if (self.productCount > self.price.party_min) {
        self.productCount = self.productCount - self.price.party_min;
        self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productCount];
    }
}

- (IBAction)addProductButtonPressed:(id)sender {
    self.productCount = self.productCount + self.price.party_min;
    self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productCount];
}

- (IBAction)buyProductButtonPressed:(id)sender {
    [self.delegate buyProductWithNumber:self.productCount price:self.price];
}

@end
