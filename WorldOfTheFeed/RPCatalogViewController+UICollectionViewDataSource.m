//
//  RPCatalogViewController+UICollectionViewDataSource.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCatalogViewController+UICollectionViewDataSource.h"
#import "RPBrandHeaderCollectionViewCell.h"
#import "RPImageCollectionViewCell.h"
#import "RPCategoryCollectionViewCell.h"
#import "RPString+MD5.h"
#import "RPServerManager.h"

@implementation RPCatalogViewController (UICollectionViewDataSource)

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.brandCollectionView){
        UICollectionReusableView *reusableview = nil;
        if (kind == UICollectionElementKindSectionHeader) {
            RPBrandHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPBrandHeaderCollectionViewCell class]) forIndexPath:indexPath];
            NSString *brandName = self.sortedBrandArray[indexPath.section][indexPath.row].name;
            // if brandName is numeric than letter will be 0-9
            if ([[brandName substringToIndex:1] isNumeric]) {
                headerView.letterLabel.text = @"0-9";
            } else {
                headerView.letterLabel.text = [brandName substringToIndex:1];
            }
            reusableview = headerView;
        }
        return reusableview;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (collectionView == self.brandCollectionView) {
        return CGSizeMake(CGRectGetWidth(self.brandCollectionView.frame), 100);
    }
    return CGSizeZero;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.brandCollectionView) {
        return self.sortedBrandArray[section].count;
    }
    NSArray <RPCatalog *> *catalogueArray = [APP_DELEGATE catalogueArrayByID:self.catalogueID];
    return catalogueArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (collectionView == self.brandCollectionView) {
        return self.sortedBrandArray.count;
    }
    if (APP_DELEGATE.filteredCatalogueDictionary) {
        return 1;
    }
    return 0;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.brandCollectionView) {
        RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
        cell.cellImageView.image = nil;
        [cell configureCellWithImageUrl:self.sortedBrandArray[indexPath.section][indexPath.row].image];
        cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
        return cell;
    }
    if (collectionView == self.categoryCollectionView) {
        RPCategoryCollectionViewCell *cell = (RPCategoryCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPCategoryCollectionViewCell class]) forIndexPath:indexPath];
        NSArray <RPCatalog *> *catalogueArray = [APP_DELEGATE catalogueArrayByID:self.catalogueID];
        [cell configureCellWith:catalogueArray[indexPath.row]];
        return cell;
    }
    return nil;
}

#pragma mark - Collection view delegates

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.brandCollectionView) {
        [[RPServerManager sharedManager]postRequestWithLink:self.sortedBrandArray[indexPath.section][indexPath.row].link fromController:self];
    }
    if (collectionView == self.categoryCollectionView) {
        RPLink *link = [[RPLink alloc]init];
        link.name = @"url_products";
        RPParam *param = [[RPParam alloc]init];
        param.name = @"catalogue_id";
        NSArray <RPCatalog *> *catalogueArray = [APP_DELEGATE catalogueArrayByID:self.catalogueID];
        param.value = [NSString stringWithFormat:@"%ld", (long)catalogueArray[indexPath.row].ID];
        link.param = @[param];
        [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.brandCollectionView) {
        return CGSizeMake((CGRectGetWidth(self.brandCollectionView.bounds) - 40)/3, 80);
    }
    return CGSizeMake((CGRectGetWidth(self.categoryCollectionView.bounds) - 60)/2, (CGRectGetHeight(self.categoryCollectionView.bounds)-40)/2);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.brandCollectionView) {
        return UIEdgeInsetsMake(20, 10, 20, 10); // top, left, bottom, right
    }
    return UIEdgeInsetsMake(0, 20, 20, 20); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.brandCollectionView) {
        return 10;
    }
    return 20.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.brandCollectionView) {
        return 2;
    }
    return 20.0;
}

@end
