//
//  RPUserOfficeViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPHamburgerViewController.h"
@class RPNewsListServerModel;

@interface RPUserOfficeViewController : RPHamburgerViewController

@property (weak, nonatomic) IBOutlet UICollectionView *messageCollectionView;

@property (strong, nonatomic) RPNewsListServerModel *newsList;

@end
