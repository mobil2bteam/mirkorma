//
//  RPProductList.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPItem;
@class RPFilter;
@class RPFilterItem;

@interface RPProductList : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPItem *> *items;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) NSInteger count_page;

@property (nonatomic, assign) NSInteger catalogue_id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *sort;

@property (nonatomic, copy) NSString *dir;

@property (nonatomic, strong) NSArray<RPFilter *> *filters;

- (void)removeEmptyProducts;

@end


@interface RPFilter : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPFilterItem *> *values;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@end


@interface RPFilterItem : NSObject <EKMappingProtocol>

@property (nonatomic, assign) BOOL checked;

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, copy) NSString *value;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *key;

@end
