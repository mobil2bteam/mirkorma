//
//  RPRegistrationViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

static NSString * const RPAgreementMessage = @"Для продолжения регистрации разрешите использовать ваши данные";

static NSString * const RPEmptyTextFieldMessage = @"Заполните все поля!";

static NSString * const RPDifferentPasswordsMessage = @"Пароли не совпадают!";

#import "RPRegistrationStep1VC.h"
#import "RPRegistrationStep2VC.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"

@interface RPRegistrationStep1VC ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *allTextFields;

@property (weak, nonatomic) IBOutlet UISwitch *agreementSwitch;

@end


@implementation RPRegistrationStep1VC

#pragma mark - Lifecycle

//меняем заголовок у navigationItem для того, чтоб на следующем экране он не сдвигал текст
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Регистрация";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - Actions

- (IBAction)continueRegistrationButtonPressed:(id)sender {
    //если пользователь не разрешил использовать свои данные
    if (!self.agreementSwitch.isOn) {
        [self showMessage:RPAgreementMessage withTitle:nil];
        return;
    }
    //если пользователь не заполнил все текстовые поля
    for (UITextField *textField in self.allTextFields) {
        if (!textField.text.length) {
            [self showMessage:RPEmptyTextFieldMessage withTitle:nil];
            return;
        }
    }
    //если пароль и подтверждение пароля не совпадают
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [self showMessage:RPDifferentPasswordsMessage withTitle:nil];
        return;
    }
    NSDictionary *params = [@{@"login":self.loginTextField.text,
                              @"password":self.passwordTextField.text,
                              @"confirm_password":self.passwordTextField.text,
                              @"first_name":self.nameTextField.text,
                              @"action":@"validate"} copy];
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postSignInWithParams:params onSuccess:^(RPUserInfo *userInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else {
            [self continueRegistration];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (void)continueRegistration{
    RPRegistrationStep2VC *step2VC = [[RPRegistrationStep2VC alloc]initWithNib];
    step2VC.password = self.passwordTextField.text;
    step2VC.firstName = self.nameTextField.text;
    step2VC.login = self.loginTextField.text;
    step2VC.isOrderAuthorization = self.isOrderAuthorization;
    [self.navigationController pushViewController:step2VC animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.loginTextField) {
        [self.nameTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.nameTextField) {
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.passwordTextField) {
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

@end
