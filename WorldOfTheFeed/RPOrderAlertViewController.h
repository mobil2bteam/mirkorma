//
//  RPOrderAlertViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPOrderAlertViewController : UIViewController

@property (strong, nonatomic) UIViewController *fromController;

@end
