//
//  RPRepeatOrderAlertVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 6/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPRepeatOrderAlertVC.h"
#import "DBProduct+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>
#import "UIViewController+Alert.h"
#import "RPOrderInfo.h"
#import "RPShoppingCartViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPCatalogViewController.h"
#import "RPMainViewController.h"

@interface RPRepeatOrderAlertVC ()

@property (weak, nonatomic) IBOutlet UIView *addProductsView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsArray;
@end

@implementation RPRepeatOrderAlertVC

- (void)viewDidLoad{
    [super viewDidLoad];
    NSArray <DBProduct *> *cartProducts = [DBProduct MR_findAll];
    // if shopping cart is empty hide addProductsView and add products from current order
    if (!cartProducts.count) {
        self.addProductsView.hidden = YES;
        [self addProductsToShoppingCart];
    }
    for (UIButton *button in self.buttonsArray) {
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}

#pragma mark - Actions

- (void)addProductsToShoppingCart{
    for (RPBag *bag in self.order.bag) {
        // если продукт уже есть в корзине, то обновляем его
        DBProduct *existProduct = [DBProduct MR_findFirstByAttribute:@"priceID" withValue:@(bag.ID)];
        if (!existProduct) {
            DBProduct *product = [DBProduct MR_createEntity];
            product.priceID = bag.ID;
            product.imageUrl = bag.image;
            product.name = bag.name;
            product.productDescription = bag.add_text;
            product.price = bag.price;
            product.number = bag.count;
            product.productID = bag.ID;
#warning fix party_min
            product.partyMin = 1;
        } else {
            existProduct.number = existProduct.number + bag.count;
        }
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
            if (error) {
                
            }
        }];
    }
}

- (IBAction)dontAddProductsButtonPressed:(id)sender {
    [DBProduct MR_truncateAll];
    __weak typeof(self) weakSelf = self;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            [weakSelf showMessage:@"Попробуйте, пожалуйста, еще раз!" withTitle:@"Произошла ошибка"];
        } else {
            weakSelf.addProductsView.hidden = YES;
            [weakSelf addProductsToShoppingCart];
        }
    }];
}

#pragma mark - Actions

- (IBAction)addProductsButtonPressed:(id)sender {
    self.addProductsView.hidden = YES;
    [self addProductsToShoppingCart];
}

- (IBAction)orderButtonPressed:(id)sender {
    RPShoppingCartViewController* shoppingCartVC = [[RPShoppingCartViewController alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:shoppingCartVC];
    self.slidingViewController.topViewController = navVC;
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)shoppingButtonPressed:(id)sender {
    RPMainViewController* mainVC = [[RPMainViewController alloc]initWithNib];
    RPCatalogViewController* catalogueVC = [[RPCatalogViewController alloc] initWithNib];
    catalogueVC.selectedContentIndex = 0;
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:mainVC];
    [navVC pushViewController:catalogueVC animated:NO];
    self.slidingViewController.topViewController = navVC;
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
