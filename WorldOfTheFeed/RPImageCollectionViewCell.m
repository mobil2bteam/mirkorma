//
//  RPIamgeCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPImageCollectionViewCell.h"

@implementation RPImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithImageUrl:(NSString *)url{
    self.cellImageView.image = nil;
    [self.cellImageView RP_setImageWithURL:url];
}

@end
