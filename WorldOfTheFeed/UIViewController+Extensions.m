//
//  UIViewController+Extensions.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+Extensions.h"

@implementation UIViewController (Extensions)

- (instancetype)initWithNib{
    NSString *nibBame = NSStringFromClass([self class]);
    return [self initWithNibName:nibBame bundle:nil];
}

@end
