//
//  RPProductViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPCollectionViewCellProtocol.h"
#import "RPOrderCollectionViewCell.h"

@class RPProduct;
@class RPMessage;

@interface RPProductViewController : UIViewController<RPCollectionViewCellProtocol, RPOrderCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *imagesCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *blocksCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blocksCollectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buyWithOneClickViewZeroHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIView *descriptionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UICollectionView *pricecCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pricecCollectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *saleView;

@property (weak, nonatomic) IBOutlet UILabel *saleLabel;

@property (strong, nonatomic) RPProduct *product;

@property (strong, nonatomic) RPMessage *message;

@property (strong, nonatomic) NSArray *photos;

@end
