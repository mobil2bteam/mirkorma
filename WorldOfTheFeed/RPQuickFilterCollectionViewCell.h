//
//  RPQuickFilterCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 5/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPQuickFilterCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *filterLabel;

@end
