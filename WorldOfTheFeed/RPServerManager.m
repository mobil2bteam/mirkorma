//
//  RPServerManager.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

//static const BOOL allowLogging = YES;

#import "RPServerManager.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "RPMain.h"
#import "RPOptions.h"
#import "RPString+Convert.h"
#import "UIViewController+Alert.h"
#import "RPCacheManager.h"
#import "RPRouter.h"
#import "RPSuggest.h"
#import "RPMessage.h"
#import "RPProductList.h"
#import "RPUserInfo.h"
#import "RPCatalogList.h"
#import "RPOrderList.h"
#import "RPLocations.h"
#import "RPOrderInfo.h"
#import "RPDeliveryList.h"
#import "AFHTTPSessionOperation.h"
#import "DBPromoCode+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPNewsListServerModel.h"
//#import "Promise.h"

@interface RPServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end

@implementation RPServerManager

+ (RPServerManager*) sharedManager {
    static RPServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPServerManager alloc] init];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", RPAddressSite]];
        manager.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        manager.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
        [manager.requestOperationManager.requestSerializer setTimeoutInterval:180];
    });
    return manager;
}

#pragma mark - API

#pragma mark - Options

// load options with urls

//- (PMKPromise *)loadOptions{
//    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
//        RPCache *optionsCache = [RPCacheManager cachedRequestWithUrl:APIKeyOptions andParams:nil];
//        if (optionsCache) {
//            NSDictionary *optionsData = [optionsCache.json dictionaryValue];
//            RPOptions *options = [EKMapper objectFromExternalRepresentation:optionsData withMapping:[RPOptions objectMapping]];
//            [options configurePOSTMethods];
//            APP_DELEGATE.options = options;
//            [APP_DELEGATE filterCatalogueList];
//            fulfill(options);
//        }
//
//        [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:@"GET" URLString:@"options.php?" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//            if (responseObject[OptionsID]) {
//                RPOptions *options = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPOptions objectMapping]];
//                [options configurePOSTMethods];
//                APP_DELEGATE.options = options;
//                [APP_DELEGATE filterCatalogueList];
//                if (responseObject[CacheID]) {
//                    [RPCacheManager cacheRequestWithUrl:APIKeyOptions andParams:nil andData:responseObject];
//                }
//                fulfill(options);
//            } else {
//                reject([[NSError alloc]init]);
//            }
//        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
//            if (allowLogging) {
//                NSLog(@"error options - %@", error.localizedDescription);
//            }
//            reject(error);
//        }] start];
//    }];
//}
//
//- (PMKPromise *)loadMain{
//    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
//        RPCache *optionsCache = [RPCacheManager cachedRequestWithUrl:APIKeyOptions andParams:nil];
//        if (optionsCache) {
//            NSDictionary *optionsData = [optionsCache.json dictionaryValue];
//            RPOptions *options = [EKMapper objectFromExternalRepresentation:optionsData withMapping:[RPOptions objectMapping]];
//            [options configurePOSTMethods];
//            APP_DELEGATE.options = options;
//            [APP_DELEGATE filterCatalogueList];
//            fulfill(options);
//        }
//        
//        [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:@"GET" URLString:@"options.php?" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//            if (responseObject[OptionsID]) {
//                RPOptions *options = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPOptions objectMapping]];
//                [options configurePOSTMethods];
//                APP_DELEGATE.options = options;
//                [APP_DELEGATE filterCatalogueList];
//                if (responseObject[CacheID]) {
//                    [RPCacheManager cacheRequestWithUrl:APIKeyOptions andParams:nil andData:responseObject];
//                }
//                fulfill(options);
//            } else {
//                reject([[NSError alloc]init]);
//            }
//        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
//            if (allowLogging) {
//                NSLog(@"error options - %@", error.localizedDescription);
//            }
//            reject(error);
//        }] start];
//    }];
//}


- (void)getOptionsOnSuccess:(void(^)()) success
                  onFailure:(void(^)()) failure {
    
    __block RPOptions *options = nil;

    RPCache *optionsCache = [RPCacheManager cachedRequestWithUrl:APIKeyOptions andParams:nil];

    if (optionsCache) {
        NSDictionary *optionsData = [optionsCache.json dictionaryValue];
        options = [EKMapper objectFromExternalRepresentation:optionsData withMapping:[RPOptions objectMapping]];
        [options configurePOSTMethods];
        
        APP_DELEGATE.options = options;
        [APP_DELEGATE filterCatalogueList];
        [self getOtherNeccessaryDataOnSuccess:^{
            success();
        } onFailure:^{
            failure();
        }];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", RPAddressSite]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
    [manager.requestSerializer setTimeoutInterval:180];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.name = @"Loading all options";
    
    NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        if (options) {
            APP_DELEGATE.options = options;
            [APP_DELEGATE.options configurePOSTMethods];
            [APP_DELEGATE filterCatalogueList];
            [self getOtherNeccessaryDataOnSuccess:^{
                success();
            } onFailure:^{
                failure();
            }];
        } else {
            failure ();
        }
    }];
    
    //options
    NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:@"options.php?" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[OptionsID]) {
            options = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPOptions objectMapping]];
            [options configurePOSTMethods];
            APP_DELEGATE.options = options;
            if (responseObject[CacheID]) {
                [RPCacheManager cacheRequestWithUrl:APIKeyOptions andParams:nil andData:responseObject];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
    }];
    [completionOperation addDependency:op1];
    [queue addOperations:@[op1] waitUntilFinished:false];
    [[NSOperationQueue mainQueue] addOperation:completionOperation];
}

// load brands, catalogue, main

- (void)getOtherNeccessaryDataOnSuccess:(void(^)()) success
                  onFailure:(void(^)()) failure {
    
    __block RPMain *main = nil;
    __block RPCatalogList *catalogueList = nil;
    __block RPBrands *brands = nil;
    __block RPLocations *locations = nil;
    
    RPCache *catalogueCache = [RPCacheManager cachedRequestWithUrl:APIKeyCatalogue andParams:nil];
    RPCache *mainCache = [RPCacheManager cachedRequestWithUrl:APIKeyMain andParams:nil];
    RPCache *brandsCache = [RPCacheManager cachedRequestWithUrl:APIKeyBrands andParams:nil];
    RPCache *locationsCache = [RPCacheManager cachedRequestWithUrl:APIKeyLocations andParams:nil];

    if (catalogueCache && mainCache && brandsCache && locationsCache) {
        
        NSDictionary *catalogueData = [catalogueCache.json dictionaryValue];
        catalogueList = [EKMapper objectFromExternalRepresentation:catalogueData withMapping:[RPCatalogList objectMapping]];
        
        NSDictionary *mainData = [mainCache.json dictionaryValue];
        main = [EKMapper objectFromExternalRepresentation:mainData[MainID] withMapping:[RPMain objectMapping]];
        
        NSDictionary *brandsData = [brandsCache.json dictionaryValue];
        brands = [EKMapper objectFromExternalRepresentation:brandsData withMapping:[RPBrands objectMapping]];
        
        NSDictionary *locationsData = [locationsCache.json dictionaryValue];
        locations = [EKMapper objectFromExternalRepresentation:locationsData withMapping:[RPLocations objectMapping]];
        
        APP_DELEGATE.main = main;
        APP_DELEGATE.catalogueList = catalogueList;
        APP_DELEGATE.brands = brands;
        APP_DELEGATE.locations = locations;
        
        [APP_DELEGATE filterCatalogueList];
        [self checkUserOnSuccess:^{
            [self getMessagesOnSuccess:^{
                success();
            }];
        }];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", RPAddressSite]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
    [manager.requestSerializer setTimeoutInterval:180];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        if (main && catalogueList && brands) {
            APP_DELEGATE.brands = brands;
            APP_DELEGATE.main = main;
            APP_DELEGATE.catalogueList = catalogueList;
            APP_DELEGATE.locations = locations;

            [APP_DELEGATE filterCatalogueList];
            [self checkUserOnSuccess:^{
                [self getMessagesOnSuccess:^{
                    success();
                }];
            }];
        } else {
            failure ();
        }
    }];
    
    //main
    NSOperation *op2 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:APP_DELEGATE.options.urlMethods[APIKeyMain] parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[MainID]) {
            main = [EKMapper objectFromExternalRepresentation:responseObject[MainID] withMapping:[RPMain objectMapping]];
            if (responseObject[CacheID]) {
                [RPCacheManager cacheRequestWithUrl:APIKeyMain andParams:nil andData:responseObject];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
    }];
    [completionOperation addDependency:op2];
    
    //catalogue
    NSOperation *op3 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:APP_DELEGATE.options.urlMethods[APIKeyCatalogue] parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[CatalogueID]) {
            catalogueList = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPCatalogList objectMapping]];
            [RPCacheManager cacheRequestWithUrl:APIKeyCatalogue andParams:nil andData:responseObject];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
    }];
    [completionOperation addDependency:op3];
    
    //brands
    NSOperation *op4 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:APP_DELEGATE.options.urlMethods[APIKeyBrands] parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[BrandsID]) {
            brands = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[RPBrands objectMapping]];
            [RPCacheManager cacheRequestWithUrl:APIKeyBrands andParams:nil andData:responseObject];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
    }];
    [completionOperation addDependency:op4];
    
    //locations
    NSOperation *op5 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:APP_DELEGATE.options.urlMethods[APIKeyLocations] parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[LocationsID]) {
            locations = [EKMapper objectFromExternalRepresentation:responseObject  withMapping:[RPLocations objectMapping]];
            [RPCacheManager cacheRequestWithUrl:APIKeyLocations andParams:nil andData:responseObject];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
    }];
    [completionOperation addDependency:op5];
    
    [queue addOperations:@[op2, op3, op4, op5] waitUntilFinished:false];
    [[NSOperationQueue mainQueue] addOperation:completionOperation];
}

// check if user is valid yet (if we'he cached [login] and [password]

- (void)checkUserOnSuccess:(void(^)()) success{
//    NSString *password = [UserDefaults valueForKey:RPPasswordKey];
//    NSString *login = [UserDefaults valueForKey:RPLoginKey];
    NSString *token = [UserDefaults valueForKey:RPTokenKey];
    
    if (token) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", RPAddressSite]];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
            success();
        }];

        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[RPTokenKey] = token;
        
        //login
        NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:APP_DELEGATE.options.urlMethods[APIKeyLogin] parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if (!responseObject[ErrorID]) {
                RPUserInfo *userInfo = [EKMapper objectFromExternalRepresentation:responseObject[UserInfoID] withMapping:[RPUserInfo objectMapping]];
                APP_DELEGATE.userInfo = userInfo;
                if (userInfo.token) {
                    [UserDefaults setValue:userInfo.token forKey:RPTokenKey];
                }
                [UserDefaults synchronize];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            success();
        }];
        [completionOperation addDependency:op1];
        [queue addOperations:@[op1] waitUntilFinished:false];
        [[NSOperationQueue mainQueue] addOperation:completionOperation];
    } else {
        success();
    }
}

- (void)getMessagesOnSuccess:(void(^)()) success{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager manager]initWithBaseURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    NSOperation *completionOperation = [NSBlockOperation blockOperationWithBlock:^{
        success();
    }];
    
    //messages
    NSOperation *op1 = [AFHTTPSessionOperation operationWithManager:manager HTTPMethod:@"GET" URLString:@"http://service.sergks.ru/api/news?key=83dd17ebcf1df9ed5288938964c57b34" parameters:@{} uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"news"]) {
            
            RPNewsListServerModel *newsList = [EKMapper objectFromExternalRepresentation:responseObject[@"news"] withMapping:[RPNewsListServerModel objectMapping]];
            APP_DELEGATE.newsList = newsList;
            
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        success();
    }];
    [completionOperation addDependency:op1];
    [queue addOperations:@[op1] waitUntilFinished:false];
    [[NSOperationQueue mainQueue] addOperation:completionOperation];
}

#pragma mark - Others

- (void)postSuggestWithLink:(RPLink *) link
                  onSuccess:(void(^)(RPSuggest *suggest, RPError *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    NSString *method = APP_DELEGATE.options.urlMethods[link.name];
    NSDictionary *params = [link buildParams];
    __block RPError *error = nil;
    __block RPSuggest *suggest = nil;
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[SuggestID]) {
                                      suggest = [EKMapper objectFromExternalRepresentation:data withMapping:[RPSuggest objectMapping]];
                                      if (data[CacheID]) {
                                          [RPCacheManager cacheRequestWithUrl:link.name andParams:params andData:data];
                                      }
                                  }
                                  if (data[MessageID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[MessageID]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (success) {
                                      success(suggest, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRequestWithLink:(RPLink *)link fromController:(UIViewController *)controller{
    NSString *method = APP_DELEGATE.options.urlMethods[link.name];
    NSDictionary *params = [link buildParams];
    RPCache *cache = [RPCacheManager cachedRequestWithUrl:link.name andParams:params];
    if (cache) {
        NSDictionary *data = [cache.json dictionaryValue];
        [RPRouter pushToControllerWithData:data withLink:link withParams:params fromController:controller];
        return;
    }
    [controller addMBProgressHUD];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  [RPRouter pushToControllerWithData:data withLink:link withParams:params fromController:controller];
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  [controller hideMBProgressHUD];
                                  [controller showMessage:ErrorMessage withOkHandler:nil andRepeatHandler:^(UIAlertAction *action) {
                                      [RPCacheManager clearCache];
                                      [controller addMBProgressHUD];
                                      [self getOptionsOnSuccess:^{
                                          [self postRequestWithLink:link fromController:controller];
                                      } onFailure:^{
                                          [self postRequestWithLink:link fromController:controller];
                                      }];
                                  }];
                              }];
}

- (void)postProductListParams:(NSDictionary *) params
                     isBrands:(BOOL)brands
                       onSuccess:(void(^)(RPProductList *productList, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPError *error = nil;
    __block RPProductList *productList = nil;
    NSString *method = APP_DELEGATE.options.urlMethods[brands ? APIKeyBrand : APIKeyProducts];
    [self.requestOperationManager GET:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[MessageID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[MessageID]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (data[CacheID]) {
                                      [RPCacheManager cacheRequestWithUrl:brands ? APIKeyBrands : APIKeyProducts andParams:params andData:data];
                                  }
                                  if (data[ProductsID]) {
                                      productList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPProductList objectMapping]];
                                      [productList removeEmptyProducts];
                                  }

                                  if (success) {
                                      success(productList, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
    
}

- (void)postOrderListOnSuccess:(void(^)()) success{
    __block NSString *error = nil;
    __block RPOrderList *orderList = nil;
    NSDictionary *params = @{@"token":APP_DELEGATE.userInfo.token};
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyOrderList];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data isKindOfClass:[NSDictionary class]]) {
                                        if (data[ErrorID] != nil) {
                                            error = data[ErrorID];
                                        } else if (data[@"order_active_list"] || data[@"order_active_list"]){
                                            orderList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPOrderList objectMapping]];
                                            APP_DELEGATE.orderList = orderList;
                                        }
                                  }
                                 
                                  if (success) {
                                      success();
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  success();
                              }];
}

- (void)postDeliveryListWithParams:(NSMutableDictionary *) params
                         onSuccess:(void(^)(RPDeliveryList *deliveryList, NSString *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block RPDeliveryList *deliveryList = nil;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyOrderDelivery];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[@"order"]) {
                                      deliveryList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPDeliveryList objectMapping]];
                                      
                                  }
                                  if (success) {
                                      success(deliveryList, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postConfirmOrderWithParams:(NSMutableDictionary *) params
                        validation:(BOOL)validation
                         onSuccess:(void(^)(RPDeliveryList *orderInfo, NSString *error)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    if ([UserDefaults valueForKey:RPPromoCodeKey]) {
        params[RPPromoCodeKey] = [UserDefaults valueForKey:RPPromoCodeKey];
    }
    __block NSString *error = nil;
    __block RPDeliveryList *deliveryList = nil;
    [self.requestOperationManager POST:@"order.php" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[@"order"]) {
                                      deliveryList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPDeliveryList objectMapping]];
                                  }
                                  if (success) {
                                      success(nil, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postOrderInfoWithID:(NSInteger) ID
                  onSuccess:(void(^)(RPOrderInfo *orderInfo, NSString *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block RPOrderInfo *orderInfo = nil;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = @(ID);
    if ([UserDefaults valueForKey:RPTokenKey]) {
        params[RPTokenKey] = [UserDefaults stringForKey:RPTokenKey];
    }
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyOrder];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[OrderInfoID]) {
                                      orderInfo = [EKMapper objectFromExternalRepresentation:data[OrderInfoID] withMapping:[RPOrderInfo objectMapping]];
                                      
                                  }
                                  
                                  if (success) {
                                      success(orderInfo, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postRepeatOrderWithID:(NSInteger) ID
                  onSuccess:(void(^)(RPOrderInfo *orderInfo, NSString *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block RPOrderInfo *orderInfo = nil;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"order_id"] = @(ID);
    if ([UserDefaults valueForKey:RPTokenKey]) {
        params[RPTokenKey] = [UserDefaults stringForKey:RPTokenKey];
    }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mirkorma.ru/app/"]];
    AFHTTPSessionManager *requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    [requestOperationManager.requestSerializer setTimeoutInterval:180];
    [requestOperationManager GET:@"order_repeat.php" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[@"order"]) {
                                      orderInfo = [EKMapper objectFromExternalRepresentation:data[@"order"] withMapping:[RPOrderInfo objectMapping]];
                                  }
                                  if (success) {
                                      success(orderInfo, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

#pragma mark - Registration/Authorization

- (void)postLogInWithParams:(NSDictionary *) params
                  onSuccess:(void(^)(RPUserInfo *userInfo, NSString *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block RPUserInfo *userInfo = nil;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyLogin];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[UserInfoID]) {
                                      // parse user
                                      userInfo = [EKMapper objectFromExternalRepresentation:data[UserInfoID] withMapping:[RPUserInfo objectMapping]];
                                      APP_DELEGATE.userInfo = userInfo;
                                      
                                      // save token
                                      if (userInfo.token) {
                                          [UserDefaults setValue:userInfo.token forKey:RPTokenKey];
                                      }
                                      [UserDefaults synchronize];
                                      
                                      // save promo code
                                      if (data[UserInfoID][RPPromoCodeKey]) {
                                          [self savePromoCode:data[UserInfoID][RPPromoCodeKey] forUser:userInfo];
                                      }
                                  }
                                  if (success) {
                                      success(userInfo, error);
                                  }
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)savePromoCode:(NSString *)promoCode forUser:(RPUserInfo *)user{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == [c] %@ AND promoCode == [c] %@", @(user.ID), promoCode];
    DBPromoCode *existPromoCode = [DBPromoCode MR_findFirstWithPredicate:predicate];
    if (!existPromoCode) {
        DBPromoCode *promoCodeDB = [DBPromoCode MR_createEntity];
        promoCodeDB.promoCode = promoCode;
        promoCodeDB.userID = user.ID;
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        }];
    }
}

- (void)postSignInWithParams:(NSDictionary *) params
                   onSuccess:(void(^)(RPUserInfo *userInfo, NSString *error)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block RPUserInfo *userInfo = nil;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyRegistration];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[ErrorID]) {
                                      error = data[ErrorID];
                                  } else if (data[UserInfoID]) {
                                      // parse user
                                      userInfo = [EKMapper objectFromExternalRepresentation:data[UserInfoID] withMapping:[RPUserInfo objectMapping]];
                                      APP_DELEGATE.userInfo = userInfo;
                                      
                                      // save token
                                      if (userInfo.token) {
                                          [UserDefaults setValue:userInfo.token forKey:RPTokenKey];
                                      }
                                      [UserDefaults synchronize];
                                      
                                      // save promo code
                                      if (data[UserInfoID][RPPromoCodeKey]) {
                                          [self savePromoCode:data[UserInfoID][RPPromoCodeKey] forUser:userInfo];
                                      }
                                  }
                                  if (success) {
                                      success(userInfo, error);
                                  }
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

#pragma mark - Password

- (void)postChangePasswordWithParams:(NSDictionary *) params
                           onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block BOOL isSuccess = NO;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyUserData];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data[@"success"] isEqualToString:@"ok"]) {
                                      isSuccess = YES;
                                  }
                                  
                                  if (data[@"error"]) {
                                      error = data[@"error"];
                                  }

                                  if (success) {
                                      success(isSuccess, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)postFeedbackWithParams:(NSDictionary *) params
                           onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block BOOL isSuccess = NO;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyFeedback];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if ([data[@"success"] isEqualToString:@"ok"]) {
                                      isSuccess = YES;
                                  }
                                  if (data[@"error"]) {
                                      error = data[@"error"];
                                  }
                                  
                                  if (success) {
                                      success(isSuccess, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (void)getMessageListOnSuccess:(void(^)(RPNewsListServerModel *newsLsit, RPError *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPError *error = nil;
    __block RPNewsListServerModel *newsList = nil;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", @""]];
    AFHTTPSessionManager *requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    [requestOperationManager.requestSerializer setTimeoutInterval:180];

    [requestOperationManager GET:@"http://service.sergks.ru/api/news?key=83dd17ebcf1df9ed5288938964c57b34" parameters:nil
                             progress:^(NSProgress * _Nonnull uploadProgress) {
                             } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
//                                 if (data[CacheID]) {
//                                     [RPCacheManager cacheRequestWithUrl:brands ? APIKeyBrands : APIKeyProducts andParams:params andData:data];
//                                 }
                                 if (data[@"news"]) {
                                     newsList = [EKMapper objectFromExternalRepresentation:data[@"news"] withMapping:[RPNewsListServerModel objectMapping]];
                                 }
                                 
                                 if (success) {
                                     success(newsList, error);
                                 }
                                 
                             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                 if (failure)
                                 {
                                     failure(error,error.code);
                                 }
                             }];
    
}

#pragma mark - Not used yet

- (void)postAddCommentWithParams:(NSDictionary *) params
                       onSuccess:(void(^)(RPMessage *message, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPError *error = nil;
    __block RPMessage *message = nil;
    NSString *method = APP_DELEGATE.options.urlMethods[APIKeyProductComment];
    [self.requestOperationManager POST:method parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[MessageID]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[MessageID]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (success) {
                                      success(message, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];

}


@end
