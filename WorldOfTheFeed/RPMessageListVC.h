//
//  RPMessageListVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsListServerModel;

@interface RPMessageListVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *messageCollectionView;

@property (strong, nonatomic) RPNewsListServerModel *newsList;

@end
