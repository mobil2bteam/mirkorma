//
//  RPSortViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPSort;

@protocol RPSortViewControllerDelegate <NSObject>

- (void)selectedSort:(RPSort *)sort;

@end

@interface RPSortViewController : UIViewController

@property (assign, nonatomic) id<RPSortViewControllerDelegate> delegate;

@end
