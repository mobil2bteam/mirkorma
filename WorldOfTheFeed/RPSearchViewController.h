//
//  RPSearchViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPSearchViewProtocol.h"
@class RPSearchView;

@interface RPSearchViewController : UIViewController <RPSearchViewProtocol>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) RPSearchView *searchView;

// id выбранного каталога товаров
@property (assign, nonatomic) NSInteger catalogueID;

// выбран ли параметр поиска [в текущем каталоге]
@property (assign, nonatomic) BOOL inCatalogueSelected;

@end
