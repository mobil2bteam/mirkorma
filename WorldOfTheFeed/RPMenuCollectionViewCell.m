//
//  RPMenuCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMenuCollectionViewCell.h"
#import "RPMain.h"

@implementation RPMenuCollectionViewCell

- (void)fillCellWithInfo:(RPContent *)content{
    self.menuTitleLabel.text = content.name;
    self.menuImageView.image = nil;
    [self.menuImageView RP_setImageWithURL:content.icon_1];
}

@end
