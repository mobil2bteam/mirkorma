//
//  RPChangePasswordAlerVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPChangePasswordAlerVC.h"
#import "RPServerManager.h"
#import "RPUserInfo.h"
#import "UIViewController+Alert.h"

@interface RPChangePasswordAlerVC ()

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@end

@implementation RPChangePasswordAlerVC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changePasswordButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField shake];
        return;
    }
    if (!self.confirmPasswordTextField.text.length) {
        [self.confirmPasswordTextField shake];
        return;
    }

    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [self showMessage:@"Пароли не совпадают" withTitle:nil];
        return;
    }
    NSDictionary *params = @{@"action":@"changepassword",
                             @"token":APP_DELEGATE.userInfo.token,
                             @"password":self.passwordTextField.text,
                             @"confirmpassword":self.confirmPasswordTextField.text};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postChangePasswordWithParams:params onSuccess:^(BOOL isSuccess, NSString *error) {
        [self hideMBProgressHUD];
        if (isSuccess) {
            [UserDefaults setValue:self.passwordTextField.text forKey:RPPasswordKey];
            [UserDefaults synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else{
            [self showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.passwordTextField) {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
