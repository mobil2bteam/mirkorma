//
//  RPOrderAlertViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderAlertViewController.h"
#import "RPSignInViewController.h"
#import "RPRegistrationStep1VC.h"

@interface RPOrderAlertViewController ()

@property (strong, nonatomic) CAGradientLayer *gradient;

@property (strong, nonatomic) UIColor *startColor;

@property (strong, nonatomic) UIColor *endColor;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;


@end

@implementation RPOrderAlertViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.startColor = [UIColor colorWithRed:0.000 green:0.557 blue:0.655 alpha:1.f];
    self.endColor = [UIColor colorWithRed:0.000 green:0.753 blue:0.990 alpha:1.f];
    self.view.backgroundColor = self.startColor;
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern1"]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.gradient = [CAGradientLayer layer];
    self.gradient.frame = self.view.bounds;
    self.gradient.startPoint = CGPointMake(0.f, 1.f);
    self.gradient.endPoint = CGPointMake(1.f, 0.f);
    self.gradient.colors = [NSArray arrayWithObjects:(id)self.startColor.CGColor, (id)self.startColor.CGColor, nil];
    [self.view.layer insertSublayer:self.gradient atIndex:0];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self animateLayer];
    });
}

#pragma mark - Methods

-(void)animateLayer
{
    NSArray *fromColors = self.gradient.colors;
    NSArray *toColors = @[(id)self.startColor.CGColor,
                          (id)self.endColor.CGColor];
    [self.gradient setColors:toColors];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"colors"];
    animation.fromValue             = fromColors;
    animation.toValue               = toColors;
    animation.duration              = 1.00;
    animation.removedOnCompletion   = YES;
    animation.fillMode              = kCAFillModeForwards;
    animation.timingFunction        = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [self.gradient addAnimation:animation forKey:@"animateGradient"];
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)firstOrderButtonPressed:(id)sender {
    RPRegistrationStep1VC *registrationStep1VC = [[RPRegistrationStep1VC alloc]initWithNib];
    registrationStep1VC.isOrderAuthorization = YES;
    [self.navigationController pushViewController:registrationStep1VC animated:YES];
}

- (IBAction)haveOrderedButtonPressed:(id)sender {
    RPSignInViewController *signInVC = [[RPSignInViewController alloc]initWithNib];
    signInVC.isOrderAuthorization = YES;
    [self.navigationController pushViewController:signInVC animated:YES];
}

@end
