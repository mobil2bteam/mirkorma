//
//  RPOrderStep6VC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/22/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
static NSString * const RPEmptyTextFieldMessage = @"Заполните все поля!";

#import "RPOrderStep6VC.h"
#import "RPRouter.h"
#import "RPServerManager.h"
#import "RPDeliveryList.h"
#import "UIImageView+AFNetworking.h"
#import "RPUserInfo.h"
#import "RPLocations.h"
#import "UIViewController+Alert.h"
#import "RPConfirmedOrderViewController.h"
#import "SHSPhoneLibrary.h"

@interface RPOrderStep6VC ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UIImageView *deliveryImageView;

@property (weak, nonatomic) IBOutlet UILabel *deliveryLabel;

@property (weak, nonatomic) IBOutlet UIImageView *paymentImageView;

@property (weak, nonatomic) IBOutlet UILabel *paymnetLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderLabel;

@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@property (weak, nonatomic) IBOutlet UILabel *deliveryPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalSumLabel;

@property (weak, nonatomic) IBOutlet UILabel *regionLabel;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation RPOrderStep6VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Подтвердите заказ";
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Подтвердите заказ";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - Methods

- (void)initialize{
    self.deliveryLabel.text = self.selectedDelivery.name;
    self.paymnetLabel.text = self.selectedPayment.name;
    self.regionLabel.text = [self regionByID:[self.orderData[@"location"] integerValue]];
    NSString *index = [self.orderData[@"adress"] substringWithRange:NSMakeRange(0, 6)];
    NSString *city = self.orderData[@"city"];
    NSString *fullAddress = [self.orderData[@"adress"] substringFromIndex:7];
    self.addressLabel.text = [NSString stringWithFormat:@"%@, %@, %@", index, city, fullAddress];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder.jpg"];
    NSURL *url = [NSURL URLWithString: self.selectedPayment.picture];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.paymentImageView setImageWithURLRequest:imageRequest
                                 placeholderImage:placeholderImage
                                          success:nil
                                          failure:nil];
    
    NSURL *url2 = [NSURL URLWithString: self.selectedDelivery.picture];
    NSURLRequest *imageRequest2 = [NSURLRequest requestWithURL:url2
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.deliveryImageView setImageWithURLRequest:imageRequest2
                                 placeholderImage:placeholderImage
                                          success:nil
                                          failure:nil];
    self.deliveryPriceLabel.text = [NSString stringWithFormat:@"%ld Руб.",(long)self.selectedDelivery.price];
    self.totalSumLabel.text = [NSString stringWithFormat:@"%ld Руб.",(long)self.deliveryList.order.total + (long)self.selectedDelivery.price];
    self.discountLabel.text = [NSString stringWithFormat:@"%ld Руб.",(long)self.deliveryList.order.discount];
    self.orderLabel.text = [NSString stringWithFormat:@"%ld Руб.",(long)self.deliveryList.order.summ];
    [self.phoneTextField.formatter setDefaultOutputPattern:@"+7 (###) ###-##-##"];
    if ([UserDefaults stringForKey:RPPhoneKey]) {
        [self.phoneTextField setFormattedText:[UserDefaults stringForKey:RPPhoneKey]];
    } else {
        [self.phoneTextField setFormattedText:APP_DELEGATE.userInfo.phone];
    }
    self.emailTextField.text = APP_DELEGATE.userInfo.email;
    self.nameTextField.text = APP_DELEGATE.userInfo.first_name;
}

- (NSString *)regionByID:(NSInteger)regionID{
    for (RPLocation *location in APP_DELEGATE.locations.locations) {
        if (location.ID == regionID) {
            if (location.region) {
                return location.region;
            } else {
                return location.country;
            }
        }
    }
    return @"";
}

#pragma mark - Action

- (IBAction)confirmOrderButtonPressed:(id)sender {
    if (!self.emailTextField.text.length || !self.phoneTextField.text.length) {
        [self showMessage:RPEmptyTextFieldMessage withTitle:nil];
        return;
    }
    if (self.phoneTextField.phoneNumberWithoutPrefix.length != 11) {
        [self showMessage:RPIncorrectPhoneMessage withTitle:nil];
        return;
    }
    // save phone
    [UserDefaults setObject:[self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1] forKey:RPPhoneKey];
    [UserDefaults synchronize];

    // send request
    self.orderData[@"name"] = self.nameTextField.text;
    self.orderData[@"phone"] = [self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1],
    self.orderData[@"email"] = self.emailTextField.text;
    self.orderData[@"action"] = @"order";
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postDeliveryListWithParams:self.orderData onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (deliveryList){
            [self goToConfirmedOrderWith:deliveryList];    
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (void)goToConfirmedOrderWith:(RPDeliveryList *)deliveryList{
    RPConfirmedOrderViewController *confirmedVC = [[RPConfirmedOrderViewController alloc]initWithNib];
    confirmedVC.deliveryList = deliveryList;
    [self.navigationController pushViewController:confirmedVC animated:YES];
}

@end
