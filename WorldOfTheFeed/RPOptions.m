//
//  RPOptions.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOptions.h"
#import "RPMain.h"

@implementation RPOptions

- (void)configurePOSTMethods{
    NSMutableDictionary *methods = [NSMutableDictionary dictionary];
    for (RPUrl *url in self.urls) {
        methods[url.name] = [url.url substringFromIndex:RPAddressSite.length];
    }
    self.urlMethods = [methods copy];
}

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPQuestion class] forKeyPath:@"options.questions" forProperty:@"questions"];
        [mapping hasMany:[RPUrl class] forKeyPath:@"options.urls" forProperty:@"urls"];
        [mapping hasMany:[RPPhone class] forKeyPath:@"options.phone" forProperty:@"phones"];
        [mapping hasMany:[RPSort class] forKeyPath:@"options.sort_type" forProperty:@"sort_type"];
        [mapping mapKeyPath:@"options.address_fiz" toProperty:@"address_fiz"];
        [mapping mapKeyPath:@"options.address_jur" toProperty:@"address_jur"];
        [mapping mapKeyPath:@"cache" toProperty:@"cache"];
        [mapping mapKeyPath:@"options.email" toProperty:@"email"];
    }];
}

@end

@implementation RPQuestion

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[RPLink class] forKeyPath:@"link" forProperty:@"link"];
        [mapping mapPropertiesFromArray:@[@"name"]];
    }];
}

@end

@implementation RPPhone

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"value"]];
    }];
}

@end


@implementation RPUrl

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPParam class] forKeyPath:@"param" forProperty:@"param"];
        [mapping mapPropertiesFromArray:@[@"name", @"url"]];
    }];
}

@end

@implementation RPSort

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"sort", @"dir", @"name"]];
    }];
}

@end
