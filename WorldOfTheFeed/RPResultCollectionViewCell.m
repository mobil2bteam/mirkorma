//
//  RPResultCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPResultCollectionViewCell.h"
#import "RPMain.h"

@implementation RPResultCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.resultNameLabel.textColor = RPLightBlueColor;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.resultImageView.layer.masksToBounds = YES;
    self.resultImageView.layer.borderColor = [UIColor colorWithRed:0.290 green:0.792 blue:0.902 alpha:1.0].CGColor;
    self.resultImageView.layer.borderWidth = 2.f;
    self.resultImageView.layer.cornerRadius = 10.f;
}

- (void)configureCellWith:(RPItem *)item{
    self.resultDescriptionLabel.text = item.additional_text;
    self.resultNameLabel.text = item.name;
    [self.resultImageView RP_setImageWithURL:item.image];
}

@end
