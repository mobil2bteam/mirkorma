//
//  RPBlockCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPCollectionViewCellProtocol.h"

@class RPBlock;

@interface RPBlockCollectionViewCell : UICollectionViewCell<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *blockCollectionView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UILabel *blockTitleLabel;

@property (strong, nonatomic) RPBlock *block;

@property (weak, nonatomic) id <RPCollectionViewCellProtocol> delegate;

- (void)fillCellWithInfo:(RPBlock *)block;

@end
