//
//  RPFeedbackVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/26/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPFeedbackVC.h"
#import "SHSPhoneLibrary.h"
#import "RPUserInfo.h"
#import "RPServerManager.h"
#import "UIViewController+Alert.h"

@interface RPFeedbackVC ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneTextField;

@end

@implementation RPFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.phoneTextField.formatter setDefaultOutputPattern:@"+7 (###) ###-##-##"];
    if (APP_DELEGATE.userInfo) {
        self.nameTextField.text = APP_DELEGATE.userInfo.first_name;
        [self.phoneTextField setFormattedText:APP_DELEGATE.userInfo.phone];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.nameTextField) {
        [self.phoneTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Action

- (IBAction)callButtonPressed:(id)sender {
    [self.view endEditing:YES];
    // name, message, phone or email are required
    if (!self.nameTextField.text.length) {
        [self.nameTextField shake];
        return;
    }
    if (self.phoneTextField.phoneNumberWithoutPrefix.length != 11) {
        [self.phoneTextField shake];
        return;
    }
    NSDictionary *params = @{@"name":self.nameTextField.text,
                             @"phone":[self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1]};
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager]postFeedbackWithParams:params onSuccess:^(BOOL isSuccess, NSString *error) {
        [self hideMBProgressHUD];
        if (isSuccess) {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
                weakSelf.successCallback();
            }];
        } else{
            [self showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
