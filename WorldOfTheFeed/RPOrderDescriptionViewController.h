//
//  RPOrderDescriptionViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOrderInfo;

@interface RPOrderDescriptionViewController : UIViewController

@property (strong, nonatomic) RPOrderInfo *orderInfo;

@property (assign, nonatomic) BOOL isRepeatOrder;

@end
