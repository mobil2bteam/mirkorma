//
//  RPTempCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/17/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPNewsCollectionViewCell.h"
#import "RPMain.h"

@implementation RPNewsCollectionViewCell

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.masksToBounds = YES;
    self.layer.borderColor = RPLightBlueColor.CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 15.f;
}

- (void)fillCellWithInfo:(RPNews *)news{
    self.newsDesciptionLabel.text = news.text;
    self.dateLabel.text = news.date;
    self.newsImageView.image = nil;
    if (news.image.length) {
        self.newsImageViewWidthConstraint.priority = 250;
        self.newsImageView.hidden = NO;
        [self.newsImageView RP_setImageWithURL:news.image];
    } else {
        self.newsImageViewWidthConstraint.priority = 750;
        self.newsImageView.hidden = YES;
    }
    [self layoutIfNeeded];
}

@end
