//
//  RPFilterCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPFilterCollectionViewCell.h"

@implementation RPFilterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [UIColor colorWithRed:0.612 green:0.612 blue:0.612 alpha:1.0].CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = 10.f;
}

- (void)checkFilter:(BOOL)check{
    if (check) {
        self.layer.borderColor = RPLightBlueColor.CGColor;
        self.filterLabel.textColor = RPLightBlueColor;
    } else {
        self.layer.borderColor = [UIColor colorWithRed:0.612 green:0.612 blue:0.612 alpha:1.0].CGColor;
        self.filterLabel.textColor = [UIColor colorWithRed:0.612 green:0.612 blue:0.612 alpha:1.0];
    }
}

@end
