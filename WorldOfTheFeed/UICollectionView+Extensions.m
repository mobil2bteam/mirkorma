//
//  UICollectionView+Extensions.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UICollectionView+Extensions.h"

@implementation UICollectionView (Extensions)

@dynamic cellName;

- (void)registerCell:(Class)classType{
    NSString *nibName = NSStringFromClass(classType);
    [self registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:nibName];
}

- (void)setCellName:(NSString *)cellName{
    [self registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellWithReuseIdentifier:cellName];
}

@end
