//
//  RPOrderStep4VC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderStep4VC.h"
#import "RPDeliveryList.h"
#import "RPDeliveryTableViewCell.h"
#import "RPRouter.h"

@interface RPOrderStep4VC ()

@property (weak, nonatomic) IBOutlet UITableView *deliveryTableView;

@end

@implementation RPOrderStep4VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Способ доставки";
    [self.deliveryTableView registerNib:[UINib nibWithNibName:@"RPDeliveryTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPDeliveryTableViewCell"];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Способ доставки";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.deliveryList.delivery_list.count;
}

- (RPDeliveryTableViewCell *)tableView:(UITableView *)tableView
                        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPDeliveryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPDeliveryTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithDelivery:self.deliveryList.delivery_list[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPDelivery *selectedDelivery = self.deliveryList.delivery_list[indexPath.row];
    [RPRouter pushOrderStep5VC:self withDeliveryList:self.deliveryList location:self.selectedLocation data:self.orderData delivery:selectedDelivery];
}

@end
