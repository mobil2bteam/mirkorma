//
//  RPLaunchViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPLaunchViewController.h"
#import "UIViewController+Alert.h"
#import "RPCacheManager.h"
#import "RPServerManager.h"
#import "RPRouter.h"

@implementation RPLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDate *date = [calendar dateBySettingHour:1 minute:0 second:0 ofDate:[NSDate date] options:0];
    if ([[NSDate date] compare:date] == NSOrderedDescending) {
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            [format setDateFormat:@"MMM/dd/yyyy"];
            NSDate *now = [[NSDate alloc] init];
            NSString *dateString = [format stringFromDate:now];
            if (![[UserDefaults stringForKey:@"lastCachedDate"] isEqualToString:dateString]) {
                [RPCacheManager clearCache];
                [UserDefaults setObject:dateString forKey:@"lastCachedDate"];
                [UserDefaults synchronize];
            }
    }
    [RPCacheManager updateCache];
    [self loadOptions];
}

- (void)loadOptions{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]getOptionsOnSuccess:^{
        [self hideMBProgressHUD];
        [RPRouter setUpStartController];
    } onFailure:^{
        [self showMessage:ErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [self loadOptions];
        }];
    }];
}

@end
