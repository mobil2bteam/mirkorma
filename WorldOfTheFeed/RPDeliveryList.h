//
//  RPDeliveryList.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
#import "RPOrderInfo.h"

@class RPDeliveryOrder;
@class RPPayment;
@class RPDelivery;

@interface RPDeliveryList : NSObject <EKMappingProtocol>

+(EKObjectMapping *)objectMapping;

@property (nonatomic, strong) NSArray<RPBag *> *bag;

@property (nonatomic, strong) NSArray<RPDelivery *> *delivery_list;

@property (nonatomic, strong) NSArray<RPPayment *> *payment_list;

@property (nonatomic, strong) RPDeliveryOrder *order;

@end


@interface RPDelivery : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *deliveryDescription;

@property (nonatomic, copy) NSString *picture;

@end


@interface RPPayment : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *paymentDescription;

@property (nonatomic, copy) NSString *picture;

@end


@interface RPDeliveryOrder : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger summ;

@property (nonatomic, assign) NSInteger total;

@property (nonatomic, assign) NSInteger discount;

@property (nonatomic, assign) NSInteger delivery_price;

@property (nonatomic, assign) NSInteger weight;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *promo_code;

@property (nonatomic, copy) NSString *friend_promo_code;

@property (nonatomic, copy) NSString *url_payment;

@end




