//
//  RPDeliveryRegionTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPDeliveryRegionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *deliveryRegionLabel;

@end
