//
//  RPOrderStep3VC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

static NSString * const RPEmptyTextFieldMessage = @"Заполните все поля!";
static NSString * const RPIncorrectIndexMessage = @"Индекс должен содержать 6 цифр";

#import "RPOrderStep3VC.h"
#import "RPLocations.h"
#import "RPServerManager.h"
#import "RPDeliveryList.h"
#import "UIViewController+Alert.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DBAddress+CoreDataProperties.h"
#import "DBPromoCode+CoreDataProperties.h"
#import "DBProduct+CoreDataProperties.h"
#import "RPRouter.h"
#import "RPUserInfo.h"

@interface RPOrderStep3VC ()

@property (weak, nonatomic) IBOutlet UILabel *regionLabel;

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UITextField *cityTextField;

@property (weak, nonatomic) IBOutlet UITextField *indexTextField;

@property (weak, nonatomic) IBOutlet UITextView *fullAddressTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cityViewZeroHeightConstraint;

@end

@implementation RPOrderStep3VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Полный адрес";
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Полный адрес";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - Methods

- (void)initialize{
    switch (self.addressType) {
        case AddressTypeCountry:
        {
            self.cityViewZeroHeightConstraint.priority = 250;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view layoutIfNeeded];
            });
        }
            break;
        case AddressTypeRegion:
        {
            self.regionLabel.text = self.selectedLocation.region;
            self.cityViewZeroHeightConstraint.priority = 250;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view layoutIfNeeded];
            });
        }
            break;
        case AddressTypeCity:
        {
            self.regionLabel.text = self.selectedLocation.region;
            self.cityLabel.text = self.selectedLocation.city;
        }
            break;
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.cityTextField) {
        [self.indexTextField becomeFirstResponder];
        return NO;
    }
    if (textField == self.indexTextField) {
        [self.fullAddressTextView becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)nextButtonPressed:(id)sender {
    //если не все поля заполнены
    if (!self.fullAddressTextView.text.length || !self.indexTextField.text.length) {
        [self showMessage:ErrorMessageEmptyTextField withTitle:nil];
        return;
    }
    if (self.addressType != AddressTypeCity) {
        if (!self.cityTextField.text.length) {
            [self showMessage:ErrorMessageEmptyTextField withTitle:nil];
            return;
        }
    }
    if (self.indexTextField.text.length != 6) {
        [self showMessage:RPIncorrectIndexMessage withTitle:nil];
        return;
    }
    [self continueOrderWith];
}

- (void)continueOrderWith{
    NSMutableDictionary *orderData = [[NSMutableDictionary alloc]init];
    orderData[@"adress"] = [NSString stringWithFormat:@"%@, %@", self.indexTextField.text, self.fullAddressTextView.text];
    orderData[@"bag"] = APP_DELEGATE.bagValue;
    orderData[@"token"] = APP_DELEGATE.userInfo.token;
    
    //если пользователь ввел промо-код, то передаем его, иначе проверяем есть ли промо-код в БД
    if (self.promoCode.length) {
        orderData[RPPromoCodeKey] = self.promoCode;
    } else {
        DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
        if (promoCode) {
            orderData[RPPromoCodeKey] = promoCode.promoCode;
        }
    }
    switch (self.addressType) {
        case AddressTypeCountry:
        {
            orderData[@"city"] = self.cityTextField.text;
            orderData[@"location"] = @(1);
        }
            break;
        case AddressTypeRegion:
        {
            orderData[@"city"] = self.cityTextField.text;
            orderData[@"location"] = @(self.selectedLocation.ID);
        }
            break;
        case AddressTypeCity:
        {
            orderData[@"city"] = self.selectedLocation.city;
            orderData[@"location"] = @(self.selectedLocation.ID);
        }
            break;
        default:
            break;
    }
    [self saveAddressWithData:orderData];
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postDeliveryListWithParams:orderData onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (deliveryList){
            [RPRouter pushOrderStep4VC:self withDeliveryList:deliveryList location:self.selectedLocation data:orderData];
        } else {
            [self showMessage:RPErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}

- (void)saveAddressWithData:(NSDictionary *)data{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(city == %@) && (locationID == %ld) && (index == %@) && (fullAddress == %@)", data[@"city"], [data[@"location"] integerValue], self.indexTextField.text, self.fullAddressTextView.text];
    DBAddress *existAddress = [DBAddress MR_findFirstWithPredicate:predicate];
    if (existAddress) {
        return;
    }
    DBAddress *newAddress = [DBAddress MR_createEntity];
    newAddress.locationID = [data[@"location"] integerValue];
    newAddress.city = data[@"city"];
    newAddress.index = self.indexTextField.text;
    if (self.selectedLocation) {
        newAddress.region = self.selectedLocation.region;
    } else {
        newAddress.region = @"Россия";
    }
    newAddress.userID = APP_DELEGATE.userInfo.ID;
    newAddress.fullAddress = self.fullAddressTextView.text;
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
    }];
}

@end
