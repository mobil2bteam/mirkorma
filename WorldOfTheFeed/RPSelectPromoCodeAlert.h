//
//  RPSelectPromoCodeAlert.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsServerModel;

@interface RPSelectPromoCodeAlert : UIViewController

@property (nonatomic, copy) void (^callback)(NSString *promoCode);

@property (strong, nonatomic) NSArray <RPNewsServerModel *> *promoCodes;

@end
