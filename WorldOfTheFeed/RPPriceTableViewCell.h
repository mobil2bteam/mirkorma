//
//  RPPriceTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPPrice;

@interface RPPriceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *weightLabel;

@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentPriceLabel;

- (void)fillCellWithInfo:(RPPrice *)price;

@end
