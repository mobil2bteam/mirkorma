//
//  RPShoppingCartViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPHamburgerViewController.h"
#import "DBProduct+CoreDataProperties.h"
#import "UICountingLabel.h"

@interface RPShoppingCartViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;

@property (strong, nonatomic) NSArray <DBProduct *> *productsArray;

@property (weak, nonatomic) IBOutlet UICountingLabel *totalSumLabel;

@property (assign, nonatomic) BOOL isMenuBarButtonHidden;

@end
