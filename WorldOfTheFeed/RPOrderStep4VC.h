//
//  RPOrderStep4VC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPDeliveryList;
@class RPLocation;

@interface RPOrderStep4VC : UIViewController

@property (strong, nonatomic) RPDeliveryList *deliveryList;

@property (strong, nonatomic) NSMutableDictionary *orderData;

@property (strong, nonatomic) RPLocation *selectedLocation;

@end
