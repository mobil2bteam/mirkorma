//
//  RPCustomButton.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface RPCustomButton : UIButton

@property (nonatomic) IBInspectable CGFloat length;

@property (nonatomic) IBInspectable UIColor *contextColor;

@end
