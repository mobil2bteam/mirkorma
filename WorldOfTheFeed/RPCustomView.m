//
//  RPCustomView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCustomView.h"

@implementation RPCustomView

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
    CGContextSetFillColorWithColor(context, self.contextColor.CGColor);
    CGRect rrect = self.bounds;
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect), __unused midy = CGRectGetMidY(rrect),  maxy = CGRectGetMaxY(rrect);

    CGContextBeginPath(context);

    CGContextMoveToPoint(context, minx, miny);
    CGContextAddLineToPoint(context, maxx, miny);
    CGContextAddLineToPoint(context, maxx, maxy - self.height);
    
    CGPoint bezierStart = {maxx, maxy - self.height};
    CGPoint bezierEnd = {minx, maxy - self.height};
    CGPoint bezierHelper1 = {midx - 20, maxy};
    CGPoint bezierHelper2 = {midx + 20, maxy};
    CGContextMoveToPoint(context, bezierStart.x, bezierStart.y);
    CGContextAddCurveToPoint(context,
                             bezierHelper1.x, bezierHelper1.y,
                             bezierHelper2.x, bezierHelper2.y,
                             bezierEnd.x, bezierEnd.y);
    CGContextAddLineToPoint(context, minx, miny);
    CGContextClosePath(context);
    CGContextFillPath(context);
    CGContextStrokePath(context);    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    if (!self.contextColor) {
        self.contextColor = self.backgroundColor;
        self.backgroundColor = [UIColor clearColor];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
