//
//  RPOrederList.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderList.h"

@implementation RPOrderList

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPOrder class] forKeyPath:@"order_active_list" forProperty:@"order_active_list"];
        [mapping hasMany:[RPOrder class] forKeyPath:@"order_arhive_list" forProperty:@"order_arhive_list"];
    }];
}

@end


@implementation RPOrder

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"date", @"number", @"total", @"status", @"discount"]];
    }];
}

@end
