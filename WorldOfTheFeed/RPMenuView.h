//
//  RPMenuView.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPMenuViewProtocol.h"

@class RPContent;

@interface RPMenuView : UIView

// массив кнопок меню
@property (strong, nonatomic) NSMutableArray <UIButton *> *buttonsArray;

// массив кнопок меню
@property (strong, nonatomic) NSMutableArray <UIImageView *> *imageViewArray;

// ширина елемента меню, ширина == высоте
@property (assign, nonatomic) NSInteger itemWidth;

// массив данных для заполнения
@property (nonatomic, strong) NSArray<RPContent *> *contentArray;

//выбранный пункт меню
@property (assign, nonatomic) NSInteger selectedContentIndex;

@property (weak, nonatomic) id<RPMenuViewProtocol> delegate;

@end
