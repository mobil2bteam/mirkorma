//
//  RPOrderStep6VC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/22/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPDeliveryList;
@class RPLocation;
@class RPDelivery;
@class RPPayment;

@interface RPOrderStep6VC : UIViewController

@property (strong, nonatomic) RPDeliveryList *deliveryList;

@property (strong, nonatomic) NSMutableDictionary *orderData;

@property (strong, nonatomic) RPLocation *selectedLocation;

@property (strong, nonatomic) RPDelivery *selectedDelivery;

@property (strong, nonatomic) RPPayment *selectedPayment;

@end
