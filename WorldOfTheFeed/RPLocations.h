//
//  RPLocations.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
@class RPLocation;

@interface RPLocations : NSObject <EKMappingProtocol>

+(EKObjectMapping *)objectMapping;

@property (nonatomic, strong) NSArray<RPLocation *> *locations;

@end


@interface RPLocation : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *country;

@property (nonatomic, copy) NSString *region;

@property (nonatomic, copy) NSString *city;

@end
