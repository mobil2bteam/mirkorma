//
//  UIImageView+ImageURL.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 2/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kPlaceholderImage = @"placeholder.jpg";

@interface UIImageView (ImageURL)

- (void)RP_setImageWithURL:(NSString *)url;

- (void)RP_setImageWithURL:(NSString *)url cropToSquare:(BOOL) crop;

@end
