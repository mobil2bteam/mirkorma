//
//  RPOrderTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPOrder;

@interface RPOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;

- (void)configureCellWith:(RPOrder *)order;

@end
