//
//  RPSignInViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSignInViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPRegistrationStep1VC.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"
#import "MBProgressHUD.h"
#import "RPRouter.h"
#import "RPRestorePasswordVC.h"

@interface RPSignInViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation RPSignInViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern1"]];
    // если это не короткий вход для оформления заказа, то добавляем кнопку в нав бар
    if (!self.isOrderAuthorization) {
        UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
        self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Вход";
    self.navigationController.navigationBar.hidden = NO;
    if (!self.isOrderAuthorization) {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

#pragma mark - Methods

- (void)successfulLogIn{
    if (self.isOrderAuthorization) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderNotification" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [APP_DELEGATE.menuVC reloadMenu];
        [APP_DELEGATE.menuVC showUserOffice];
    }
}

#pragma mark - Actions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Actions

- (IBAction)signInButtonPressed:(id)sender {
    if (!self.emailTextField.text.length) {
        [self.emailTextField shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField shake];
        return;
    }
    NSDictionary *params = [@{@"login":self.emailTextField.text,
                              @"password":self.passwordTextField.text} copy];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postLogInWithParams:params onSuccess:^(RPUserInfo *userInfo, NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (userInfo) {
            [self successfulLogIn];
        } else {
            if (error) {
                [self showMessage:error withTitle:nil];
            }else {
                [self showMessage:ErrorMessage withTitle:nil];
            };
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (IBAction)registerPerMailButtonPressed:(id)sender {
    RPRegistrationStep1VC* registrationVC = [[RPRegistrationStep1VC alloc]     initWithNibName:@"RPRegistrationStep1VC" bundle:nil];
    [self.navigationController pushViewController:registrationVC animated:YES];
}

- (IBAction)restorePasswordButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPRestorePasswordVC *restorePasswordVC = [[RPRestorePasswordVC alloc]initWithNib];
    restorePasswordVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    restorePasswordVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    restorePasswordVC.successCallback = ^{
        [weakSelf showMessage:@"Данные для входа отправлены вам на почту" withTitle:@"Восстановление пароля"];
    };
    [self presentViewController:restorePasswordVC animated:YES completion:nil];
}

- (IBAction)registerPerFBButtonPressed:(id)sender {
    
}

- (IBAction)registerPerVKButtonPressed:(id)sender {
    
}

@end
