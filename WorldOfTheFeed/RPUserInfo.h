//
//  RPUserInfo.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 11/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

//@class RPCart;
//@class RPAdress;

@interface RPUserInfo : NSObject <EKMappingProtocol>

+(EKObjectMapping *)objectMapping;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *token;

@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSString *active;

@property (nonatomic, copy) NSString *first_name;

@property (nonatomic, copy) NSString *second_name;

@property (nonatomic, copy) NSString *last_name;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, assign) NSInteger status;

//@property (nonatomic, strong) NSArray<RPCart *> *cart;
//
//@property (nonatomic, strong) NSArray<RPAdress *> *address;

@end


//@interface RPCart : NSObject <EKMappingProtocol>
//
//@property (nonatomic, copy) NSString *name;
//
//@end

