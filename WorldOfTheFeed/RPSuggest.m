//
//  RPSuggest.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSuggest.h"
#import "RPMain.h"
@implementation RPSuggest

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPItem class] forKeyPath:@"suggest" forProperty:@"suggestList"];
        [mapping mapPropertiesFromArray:@[@"cache"]];
    }];
}

- (void)addSuggests:(RPSuggest *)suggest{
    self.suggestList = [self.suggestList arrayByAddingObjectsFromArray:suggest.suggestList];
}
@end
