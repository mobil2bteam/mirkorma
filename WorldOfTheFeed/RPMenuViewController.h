//
//  RPMenuViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPMenuViewController : UIViewController

- (void)reloadMenu;

- (void)showUserOffice;

- (void)showSignInController;

- (void)showMainController;

- (void)showAboutUsController;

@end
