//
//  RPMessage.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMessage.h"

@implementation RPMessage

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"title",
                                          @"text"]];
    }];
}

@end
