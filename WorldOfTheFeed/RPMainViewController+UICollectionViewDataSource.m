//
//  RPMainViewController+UICollectionViewDataSource.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMainViewController+UICollectionViewDataSource.h"
#import "RPNewsCollectionViewCell.h"
#import "RPMenuCollectionViewCell.h"
#import "RPBlockCollectionViewCell.h"
#import "RPImageCollectionViewCell.h"
#import "RPMain.h"
#import "RPServerManager.h"
#import "RPRouter.h"

@implementation RPMainViewController (UICollectionViewDataSource)

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.newsCollectionView) {
        return APP_DELEGATE.main.news.count;
    }
    if (collectionView == self.menuCollectionView) {
        return APP_DELEGATE.main.menu.content.count;
    }
    if (collectionView == self.bannersCollectionView) {
        return APP_DELEGATE.main.banners.count;
    }
    if (collectionView == self.advantageCollectionView) {
        return APP_DELEGATE.main.advantage.count;
    }
    if (collectionView == self.brandCollectionView) {
        return APP_DELEGATE.brands.top.count;
    }
    if (collectionView == self.blocksCollectionView) {
        return APP_DELEGATE.main.blocks.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.newsCollectionView) {
        RPNewsCollectionViewCell *cell = (RPNewsCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPNewsCollectionViewCell class]) forIndexPath:indexPath];
        [cell fillCellWithInfo:APP_DELEGATE.main.news[indexPath.row]];
        return cell;
    }
    if (collectionView == self.menuCollectionView) {
        RPMenuCollectionViewCell *cell = (RPMenuCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPMenuCollectionViewCell class]) forIndexPath:indexPath];
        [cell fillCellWithInfo:APP_DELEGATE.main.menu.content[indexPath.row]];
        return cell;
    }
    if (collectionView == self.bannersCollectionView) {
        RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWithImageUrl:APP_DELEGATE.main.banners[indexPath.row].background_img];
        cell.cellImageView.contentMode = UIViewContentModeScaleToFill;
        return cell;
    }
    if (collectionView == self.advantageCollectionView) {
        RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
        cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
        [cell configureCellWithImageUrl:APP_DELEGATE.main.advantage[indexPath.row].background_img];
        return cell;
    }
    if (collectionView == self.brandCollectionView) {
        RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWithImageUrl:APP_DELEGATE.brands.top[indexPath.row].image];
        cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
        return cell;
    }
    if (collectionView == self.blocksCollectionView) {
        RPBlockCollectionViewCell *cell = (RPBlockCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPBlockCollectionViewCell class]) forIndexPath:indexPath];
        cell.block = APP_DELEGATE.main.blocks[indexPath.row];
        [cell fillCellWithInfo:APP_DELEGATE.main.blocks[indexPath.row]];
        cell.tag = indexPath.row;
        cell.delegate = self;
        return cell;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.newsCollectionView) {
        if (APP_DELEGATE.main.news[indexPath.row].link) {
            [[RPServerManager sharedManager]postRequestWithLink:APP_DELEGATE.main.news[indexPath.row].link fromController:self];
        }
    }
    if (collectionView == self.advantageCollectionView) {
        if (APP_DELEGATE.main.advantage[indexPath.row].link) {
            [[RPServerManager sharedManager]postRequestWithLink:APP_DELEGATE.main.advantage[indexPath.row].link fromController:self];
        }
    }
    if (collectionView == self.bannersCollectionView) {
        if (APP_DELEGATE.main.banners[indexPath.row].link) {
            [[RPServerManager sharedManager]postRequestWithLink:APP_DELEGATE.main.banners[indexPath.row].link fromController:self];
        }
    }
    if (collectionView == self.brandCollectionView) {
        if (APP_DELEGATE.brands.top[indexPath.row].link) {
            [[RPServerManager sharedManager]postRequestWithLink:APP_DELEGATE.brands.top[indexPath.row].link fromController:self];
        }
    }
    if (collectionView == self.menuCollectionView){
        [RPRouter pushToCatalogueControllerWithContentIndex:indexPath.row fromController:self];
    }
    if (collectionView == self.blocksCollectionView){
        if (APP_DELEGATE.main.blocks[indexPath.row].link) {
            [[RPServerManager sharedManager]postRequestWithLink:APP_DELEGATE.main.blocks[indexPath.row].link fromController:self];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = width = 0;
    if (IDIOM == IPAD) {
        if (collectionView == self.newsCollectionView) {
            height = 120;
            width = 320;
            return CGSizeMake(width * 0.7, height);
        }
        if (collectionView == self.menuCollectionView) {
            height = CGRectGetHeight(self.menuCollectionView.bounds)/2;
            width = CGRectGetWidth(self.menuCollectionView.bounds)/3;
        }
        if (collectionView == self.bannersCollectionView) {
            height = CGRectGetHeight(self.bannersCollectionView.bounds);
            width = CGRectGetWidth(self.bannersCollectionView.bounds);
        }
        if (collectionView == self.blocksCollectionView) {
            height = 400;
            width = CGRectGetWidth(self.blocksCollectionView.bounds);
        }
        if (collectionView == self.advantageCollectionView) {
            height = CGRectGetHeight(self.advantageCollectionView.bounds);
            width = CGRectGetWidth(self.advantageCollectionView.bounds) * 0.7;
        }
        if (collectionView == self.brandCollectionView) {
            height = CGRectGetHeight(self.brandCollectionView.bounds);
            width = CGRectGetWidth(self.brandCollectionView.bounds) * 0.3;
        }
    } else {
        if (collectionView == self.newsCollectionView) {
            height = 120;
            width = 320;
            return CGSizeMake(width * 0.7, height);
        }
        if (collectionView == self.menuCollectionView) {
            height = CGRectGetHeight(self.menuCollectionView.bounds)/2;
            width = CGRectGetWidth(self.menuCollectionView.bounds)/3;
        }
        if (collectionView == self.bannersCollectionView) {
            height = CGRectGetHeight(self.bannersCollectionView.bounds);
            width = CGRectGetWidth(self.bannersCollectionView.bounds);
        }
        if (collectionView == self.blocksCollectionView) {
            height = 400;
            width = CGRectGetWidth(self.blocksCollectionView.bounds);
        }
        if (collectionView == self.advantageCollectionView) {
            height = CGRectGetHeight(self.advantageCollectionView.bounds);
            width = CGRectGetWidth(self.advantageCollectionView.bounds) * 0.7;
        }
        if (collectionView == self.brandCollectionView) {
            height = CGRectGetHeight(self.brandCollectionView.bounds);
            width = CGRectGetWidth(self.brandCollectionView.bounds) / 4;
        }
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.newsCollectionView) {
        return UIEdgeInsetsMake(0, 20, 0, 20); // top, left, bottom, right
    }
    if (collectionView == self.brandCollectionView) {
        return UIEdgeInsetsMake(0, 4, 0, 4); // top, left, bottom, right
    }
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.blocksCollectionView) {
        return 100;
    }
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.newsCollectionView) {
        return 15.0;
    }
    return 10.0;
}

@end
