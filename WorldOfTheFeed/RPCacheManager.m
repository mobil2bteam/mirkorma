//
//  RPCacheManager.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCacheManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPMain.h"
#import "RPCache+CoreDataProperties.h"
#import "RPString+Convert.h"
#import "RPDictionary+Convert.h"

@implementation RPCacheManager

+ (RPCache *)cachedRequestWithUrl:(NSString *)url andParams:(NSDictionary *)params{
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        if ([cache.key isEqualToString:url]) {
            if (params) {
                if ([cache.params isEqualToString:[params stringValue]]) {
                    return cache;
                }
                continue;
            }
            return cache;
        }
    }
    return nil;
}

+ (void)updateCache{
//    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
//    NSDate *date = [calendar dateBySettingHour:12 minute:0 second:0 ofDate:[NSDate date] options:0];
//    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
//    NSDate *dateInLocalTimezone = [date dateByAddingTimeInterval:timeZoneSeconds];
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        // если кэш устарел, то удаляем его
        NSDate *cacheDate = [cache.date dateByAddingTimeInterval:[cache.seconds doubleValue]];
        if ([cacheDate compare:[NSDate date]] == NSOrderedAscending) {
            [cache MR_deleteEntity];
        }
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)clearCache{
    NSArray *allCache = [RPCache MR_findAll];
    for (RPCache *cache in allCache) {
        [cache MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)cacheRequestWithUrl:(NSString *)url andParams:(NSDictionary *)params andData:(NSDictionary *)json{
    RPCache *cache = [RPCache MR_createEntity];
    cache.date = [NSDate date];
    cache.key = url;
    cache.json = [json stringValue];
    if ([json[CacheID] integerValue]) {
        cache.seconds = @([json[CacheID] integerValue]);
    } else {
        cache.seconds = @(8600);
    }
    if (params) {
        cache.params = [params stringValue];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

@end
