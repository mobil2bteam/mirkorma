//
//  RPBrandHeaderCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPBrandHeaderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UILabel *letterLabel;

@end
