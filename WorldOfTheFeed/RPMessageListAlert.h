//
//  RPMessageListAlert.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsServerModel;

@interface RPMessageListAlert : UIViewController

@property (nonatomic, copy) void (^callback)(NSString *promoCode);

@property (strong, nonatomic) NSArray <RPNewsServerModel *> *promoCodes;

@end
