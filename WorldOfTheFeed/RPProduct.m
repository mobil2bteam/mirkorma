//
//  RPProduct.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProduct.h"
#import "RPMain.h"

@implementation RPProduct

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPBlock class] forKeyPath:@"blocks" forProperty:@"blocks"];
        [mapping hasMany:[RPImage class] forKeyPath:@"images" forProperty:@"images"];
        [mapping hasMany:[RPComment class] forKeyPath:@"comments" forProperty:@"comments"];
        [mapping hasMany:[RPPrice class] forKeyPath:@"prices" forProperty:@"prices"];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"description" toProperty:@"descriptionProduct"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"additional_text",
                                          @"check_comment",
                                          @"composition"]];
    }];
}

- (void)removeEmptyProducts{
    NSMutableArray<RPBlock *> *filteredBlocks = [[NSMutableArray alloc]init];
    for (RPBlock *block in self.blocks) {
        
        NSMutableArray<RPItem *> *filteredItems = [[NSMutableArray alloc]init];
        for (RPItem *item in block.items) {
            NSMutableArray<RPPrice *> *filteredPrices = [[NSMutableArray alloc]init];
            for (RPPrice *price in item.prices) {
                if (price.price) {
                    [filteredPrices addObject:price];
                }
            }
            item.prices = [filteredPrices copy];
            if (item.prices.count) {
                [filteredItems addObject:item];
            }
        }
        block.items = [filteredItems copy];
        if (block.items.count) {
            [filteredBlocks addObject:block];
        }
    }
}

@end

@implementation RPImage

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"image"]];
    }];
}

@end

@implementation RPComment

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"comment",
                                          @"bal"]];
    }];
}

@end
