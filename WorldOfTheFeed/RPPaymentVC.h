//
//  RPPaymentVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPPaymentVC : UIViewController

@property (assign, nonatomic) NSInteger orderID;

@property (nonatomic, copy) void (^successCallback)(void);

@property (strong, nonatomic) NSString *paymentURL;

@end
