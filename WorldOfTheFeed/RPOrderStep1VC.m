//
//  RPSelectDeliveryRegionViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderStep1VC.h"
#import "RPDeliveryRegionTableViewCell.h"
#import "RPLocations.h"
#import "RPRouter.h"

@interface RPOrderStep1VC ()

@property (weak, nonatomic) IBOutlet UIView *shadowLineView;

@property (weak, nonatomic) IBOutlet UITableView *deliveryRegionsTableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSArray <RPLocation *> *allRegionsArray;

@property (strong, nonatomic) NSArray <RPLocation *> *filteredRegionsArray;

@end

@implementation RPOrderStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.shadowLineView.layer.masksToBounds = NO;
    self.shadowLineView.layer.shadowOffset = CGSizeMake(0.f, 1.f);
    self.shadowLineView.layer.shadowRadius = 1;
    self.shadowLineView.layer.shadowOpacity = 0.5;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Выбрать регион";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (void)initialize{
    self.navigationItem.title = @"Выбрать регион";
    [self.deliveryRegionsTableView registerNib:[UINib nibWithNibName:@"RPDeliveryRegionTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPDeliveryRegionTableViewCell"];
    [self.searchBar setTintColor:[UIColor whiteColor]];
    self.allRegionsArray = [[NSArray alloc]init];
    self.filteredRegionsArray = [[NSArray alloc]init];
    NSMutableArray *regionsArray = [[NSMutableArray alloc]init];
    for (RPLocation *location in APP_DELEGATE.locations.locations) {
        if (!location.city.length && location.region.length) {
            [regionsArray addObject:location];
        }
    }
    self.filteredRegionsArray = [regionsArray copy];
    self.allRegionsArray = [regionsArray copy];
    [self.deliveryRegionsTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.filteredRegionsArray.count;
}

- (RPDeliveryRegionTableViewCell *)tableView:(UITableView *)tableView
              cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPDeliveryRegionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPDeliveryRegionTableViewCell class]) forIndexPath:indexPath];
    cell.deliveryRegionLabel.text = self.filteredRegionsArray[indexPath.row].region;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [RPRouter pushOrderStep2VC:self withLocation:self.filteredRegionsArray[indexPath.row] promoCode:self.promoCode];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO];
    [self.searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self filterRegions];
}

- (void)filterRegions{
    if ([self.searchBar.text isEqualToString:@""]) {
        self.filteredRegionsArray = self.allRegionsArray;
        [self.deliveryRegionsTableView reloadData];
        return;
    }
    self.filteredRegionsArray = [[NSArray alloc]init];
    NSMutableArray *regionsArray = [[NSMutableArray alloc]init];
    for (RPLocation *location in self.allRegionsArray) {
        if ([location.region rangeOfString:self.searchBar.text options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [regionsArray addObject:location];
        }
    }
    self.filteredRegionsArray = [regionsArray copy];
    [self.deliveryRegionsTableView reloadData];
}

#pragma mark - Actions

- (IBAction)addressButtonPressed:(id)sender {
    [RPRouter pushOrderStep3VC:self withLocation:nil addressType:AddressTypeCountry promoCode:self.promoCode];
}

@end
