//
//  RPNewsListServerModel.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPNewsListServerModel.h"

@implementation RPNewsListServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPNewsServerModel class] forKeyPath:@"items"];
    }];
}

- (NSArray<RPNewsServerModel *> *)promoCodes{
    NSMutableArray <RPNewsServerModel *> *codes = [[NSMutableArray alloc]init];
    
    for (RPNewsServerModel *news in self.items) {
        [codes addObject:news];

        if (news.promocode.value) {
            [codes addObject:news];
        }
    }
    
    return [codes copy];
}
@end


@implementation RPNewsServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"text", @"image", @"image_big", @"cityId", @"promo", @"name", @"add_text", @"date", @"date_from", @"date_to", @"url"]];
        [mapping hasOne:[RPPromoCodeServerModel class] forKeyPath:@"promocode"];
    }];
}

@end


@implementation RPPromoCodeServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[ @"value", @"discount"]];
    }];
}

@end

