//
//  AppDelegate.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "AppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPMainViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "RPRouter.h"
#import "RPMain.h"
#import "RPCatalogList.h"
#import "RPOptions.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <AFNetworkActivityIndicatorManager.h>
#import <VKSdk.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "RPCacheManager.h"
#import "RPLaunchViewController.h"
#import "DBProduct+CoreDataProperties.h"


#import "RPConfirmedOrderViewController.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self registerRemoteNotification:application];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

    [Fabric with:@[[Crashlytics class]]];

    // add keyboardmanager to automatically move up/down view
    [IQKeyboardManager sharedManager].enable = YES;

    //Setup data base
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    [self customize];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    RPLaunchViewController *launchVC = [[RPLaunchViewController alloc]initWithNib];
    self.window.rootViewController = launchVC;
    [self.window makeKeyAndVisible];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    return YES;
}

#pragma mark - Methods

- (void)customize{
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTintColor:RPDimRedColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : RPLightBlueColor}];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UISearchBar appearance]setBackgroundImage:[[UIImage alloc]init]];
    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"search"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [[UISearchBar appearance]setTintColor:RPDimRedColor];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:RPDimRedColor];
}

- (void)filterCatalogueList{
    NSMutableDictionary <NSString *,  NSArray<RPCatalog *> *> *mutableFilteredCatalogueDictionary = [NSMutableDictionary dictionary];
    for (RPContent *content in self.main.menu.content) {
        NSMutableArray <RPCatalog *> *catalogueArray = [NSMutableArray array];
        for (RPCatalog *catalogue in self.catalogueList.catalog) {
            if (content.ID == catalogue.parent_id) {
                [catalogueArray addObject:catalogue];
            }
        }
        NSString *stringID = [NSString stringWithFormat:@"%ld", (long)content.ID];
        mutableFilteredCatalogueDictionary[stringID] = catalogueArray;
    }
    self.filteredCatalogueDictionary = [mutableFilteredCatalogueDictionary copy];
}

- (NSArray <RPCatalog *> *)catalogueArrayByID:(NSInteger)ID{
    NSString *stringID = [NSString stringWithFormat:@"%ld", (long)ID];
    return self.filteredCatalogueDictionary[stringID];
}

- (NSString *)urlFromName:(NSString *)name{
    for (RPUrl *url in self.options.urls) {
        if ([url.name isEqualToString:name]) {
            return url.url;
        }
    }
    return @"";
}

- (NSString *)bagValue{
    NSArray <DBProduct *> *reservedProducts = [DBProduct MR_findAll];
    NSMutableString *bagString = [@"" mutableCopy];
    for (NSInteger i = 0; i < reservedProducts.count; i++){
        DBProduct *currentProduct = reservedProducts[i];
        if (i != reservedProducts.count - 1) {
            [bagString appendString:[NSString stringWithFormat:@"%ld-%d;", (long)currentProduct.priceID, currentProduct.number]];
        } else {
            [bagString appendString:[NSString stringWithFormat:@"%ld-%d", (long)currentProduct.priceID, currentProduct.number]];
        }
    }
    return [bagString copy];
}


- (NSInteger)shoppingCartItemsCount{
    return  [DBProduct MR_findAll].count;
}


#pragma mark - Firebase

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    //    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    //    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    //    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    //    NSLog(@"%@", userInfo);
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    //    NSDictionary *userInfo = notification.request.content.userInfo;
    //    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Print full message.
    //    NSLog(@"%@", userInfo);
    //    completionHandler(UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    //    NSDictionary *userInfo = response.notification.request.content.userInfo;
    //    NSLog(@"%@", userInfo);
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    //    NSLog(@"%@", [remoteMessage appData]);
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
//        NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            //            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            [[FIRMessaging messaging]subscribeToTopic:@"/topics/news"];
        }
    }];
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // With swizzling disabled you must set the APNs token here.
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [FBSDKAppEvents activateApp];
    [self connectToFcm];
}

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
}
// [END disconnect_from_fcm]




 - (void)registerRemoteNotification:(UIApplication *)application{
     // Register for remote notifications
     if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
         // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
         UIRemoteNotificationType allNotificationTypes =
         (UIRemoteNotificationTypeSound |
          UIRemoteNotificationTypeAlert |
          UIRemoteNotificationTypeBadge);
         [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
     } else {
         // iOS 8 or later
         // [START register_for_notifications]
         if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
             UIUserNotificationType allNotificationTypes =
             (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
             UIUserNotificationSettings *settings =
             [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
             [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
         } else {
             // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
             UNAuthorizationOptions authOptions =
             UNAuthorizationOptionAlert
             | UNAuthorizationOptionSound
             | UNAuthorizationOptionBadge;
             [[UNUserNotificationCenter currentNotificationCenter]
              requestAuthorizationWithOptions:authOptions
              completionHandler:^(BOOL granted, NSError * _Nullable error) {
              }
              ];
             
             // For iOS 10 display notification (sent via APNS)
             [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
             // For iOS 10 data message (sent via FCM)
             [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
         }
         
         [[UIApplication sharedApplication] registerForRemoteNotifications];
         // [END register_for_notifications]
     }
     
     // [START configure_firebase]
     [FIRApp configure];
     // [END configure_firebase]
     // Add observer for InstanceID token refresh callback.
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                  name:kFIRInstanceIDTokenRefreshNotification object:nil];
 }


@end
