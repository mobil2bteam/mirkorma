//
//  RPConfirmedOrderViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPConfirmedOrderViewController.h"
#import "RPDeliveryList.h"
#import "RPRouter.h"
#import "RPOrderDescriptionViewController.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DBProduct+CoreDataProperties.h"
#import "DBPromoCode+CoreDataProperties.h"
#import "RPUserInfo.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "FacebookActivity.h"
#import <VKSdk.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "RPPaymentVC.h"


@interface RPConfirmedOrderViewController ()<VKSdkUIDelegate, VKSdkDelegate, FBSDKSharingDelegate>

@property (weak, nonatomic) IBOutlet UIView *shadowLineView;

@property (weak, nonatomic) IBOutlet UILabel *friendPromoCodeLabel;

@end

@implementation RPConfirmedOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // добавляем логотип в navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    self.friendPromoCodeLabel.text = self.deliveryList.order.friend_promo_code;
    //hide back button
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [self.navigationItem setHidesBackButton:YES];
    [self clearShoppingCartAndPromoCode];
    if (_deliveryList.order.url_payment.length) {
        [self presentPaymentVC];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[VKSdk initializeWithAppId:kVKIdentifier] registerDelegate:self];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:kVKIdentifier];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.shadowLineView.layer.masksToBounds = NO;
    self.shadowLineView.layer.shadowOffset = CGSizeMake(0.f, 1.f);
    self.shadowLineView.layer.shadowRadius = 1;
    self.shadowLineView.layer.shadowOpacity = 0.5;
}

#pragma mark - Methods

- (void)presentPaymentVC{
    RPPaymentVC *paymentVC = [[RPPaymentVC alloc]initWithNib];
    paymentVC.orderID = self.deliveryList.order.ID;
    paymentVC.paymentURL = self.deliveryList.order.url_payment;
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:paymentVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)clearShoppingCartAndPromoCode{
    //очищаем корзину
    NSArray *productsArray = [DBProduct MR_findAll];
    for (DBProduct *product in productsArray) {
        [product MR_deleteEntity];
    }
    //удаляем промо код, если он есть
    DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
    if (promoCode) {
        [promoCode MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
    }];
}

#pragma mark - Actions

- (IBAction)shareButtonPressed:(id)sender {
    NSString *firstName = APP_DELEGATE.userInfo.first_name;
    NSString *secondName = APP_DELEGATE.userInfo.second_name;
    NSString *userName;
    if (firstName){
        userName = [NSString stringWithFormat:@"%@", firstName];
    }
    if (secondName){
        userName = [NSString stringWithFormat:@"%@ %@", userName, secondName];
    }
    NSString *promoCode = self.deliveryList.order.friend_promo_code;
    NSString *text = [NSString stringWithFormat:@"Ваш друг, %@, подарил(а) Вам скидку 7%% на весь ассортимент товаров интернет-зоомагазина \"Мир Корма\". Используйте промо код \"%@\" на сайте mirkorma.ru", userName, promoCode];
    NSURL *url = [NSURL URLWithString:@"http://www.mirkorma.ru"];
    __weak typeof(self) weakSelf = self;
    FacebookActivity *facebookShare = [[FacebookActivity alloc]init];
    facebookShare.successCallback = ^{
        if ([FBSDKAccessToken currentAccessToken])
        {
            [weakSelf showFaceBookShareDialog];
        } else {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login
             logInWithReadPermissions: @[@"public_profile", @"email"]
             fromViewController:weakSelf
             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                 if (error) {
                 } else if (!result.isCancelled) {
                     [weakSelf showFaceBookShareDialog];
                 }
             }];
        }
    };
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[text, url]
     applicationActivities:@[[VKActivity new], facebookShare]];
    controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    
    [self presentViewController:controller animated:YES completion:nil];
    return;
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [self logOutViaFaceBook];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    [self logOutViaFaceBook];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    [self logOutViaFaceBook];
}

- (void)logOutViaFaceBook{
//    [[FBSDKLoginManager new] logOut];
}

- (void)showFaceBookShareDialog{
    NSString *firstName = APP_DELEGATE.userInfo.first_name;
    NSString *secondName = APP_DELEGATE.userInfo.second_name;
    NSString *userName;
    if (firstName){
        userName = [NSString stringWithFormat:@"%@", firstName];
    }
    if (secondName){
        userName = [NSString stringWithFormat:@"%@ %@", userName, secondName];
    }
    NSString *promoCode = self.deliveryList.order.friend_promo_code;
    NSString *text = [NSString stringWithFormat:@"Ваш друг, %@, подарил(а) Вам скидку 7%% на весь ассортимент товаров интернет-зоомагазина \"Мир Корма\". Используйте промо код \"%@\" на сайте mirkorma.ru", userName, promoCode];
    
    NSURL *url = [NSURL URLWithString:@"http://www.mirkorma.ru"];

    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeAutomatic;
    dialog.fromViewController = self;
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
    content.contentTitle = @"Поделиться";
    content.contentDescription = text;
    content.contentURL = url;
    dialog.shareContent = content;
    dialog.delegate = self;
    if (![dialog canShow]) {
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

- (IBAction)goShoppingButtonPressed:(id)sender {
    [APP_DELEGATE.menuVC showMainController];
}

- (IBAction)showOrderButtonPressed:(id)sender {
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postOrderInfoWithID:self.deliveryList.order.ID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            [RPRouter pushToOrderDescriptionControllerWithOrderInfo:orderInfo isRepeat:NO fromController:self];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

#pragma mark - VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{

}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}

- (void)vkSdkUserAuthorizationFailed{
    [self showMessage:RPErrorMessage withTitle:nil];
}

@end
