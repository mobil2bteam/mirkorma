//
//  RPTempCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/17/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNews;

@interface RPNewsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *newsDesciptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *newsImageViewWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *newsImageViewProportionalWidthConstraint;

- (void)fillCellWithInfo:(RPNews *)news;

@end
