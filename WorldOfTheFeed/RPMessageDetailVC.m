//
//  RPMessageDetailVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessageDetailVC.h"
#import "RPNewsListServerModel.h"
#import "UILabel+HTML.h"
#import "UIColor+HEX.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "FacebookActivity.h"
#import <VKSdk.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIViewController+Alert.h"

@interface RPMessageDetailVC () <VKSdkUIDelegate, VKSdkDelegate, FBSDKSharingDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *messageImageView;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@end

@implementation RPMessageDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // добавляем логотип в navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    [self.messageImageView RP_setImageWithURL:self.selectedNews.image];
    self.dateLabel.text = self.selectedNews.date;
    self.promoCodeLabel.text = self.selectedNews.promocode.value;
    self.nameLabel.textColor = [UIColor darkTextColor];
    self.nameLabel.text = self.selectedNews.name;
    [self.descriptionLabel setHTML:self.selectedNews.text];
    
    // если нет url, то скрываем кнопку
    if (self.selectedNews.url.length == 0) {
        [self.shareButton removeFromSuperview];
        [self.view layoutIfNeeded];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[VKSdk initializeWithAppId:kVKIdentifier] registerDelegate:self];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:kVKIdentifier];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
}

- (NSString *)text{
    return [NSString stringWithFormat:@"Ваш друг подарил(а) Вам скидку на весь ассортимент товаров интернет-зоомагазина ''Мир Корма''. Используйте промо код %@ в мобильном приложении mirkorma.ru", self.selectedNews.promocode.value];
}

#pragma mark - Actions

- (IBAction)shareButtonPressed:(id)sender {
    NSString *text = [self text];
    NSURL *url = [NSURL URLWithString:self.selectedNews.url];
    __weak typeof(self) weakSelf = self;
    FacebookActivity *facebookShare = [[FacebookActivity alloc]init];
    facebookShare.successCallback = ^{
        if ([FBSDKAccessToken currentAccessToken])
        {
            [weakSelf showFaceBookShareDialog];
        } else {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login
             logInWithReadPermissions: @[@"public_profile"]
             fromViewController:weakSelf
             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                 if (error) {
                 } else if (!result.isCancelled) {
                     [weakSelf showFaceBookShareDialog];
                 }
             }];
        }
    };
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[text, url]
     applicationActivities:@[[VKActivity new], facebookShare]];
    controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypePostToFacebook,
                                         UIActivityTypePrint,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    
    [self presentViewController:controller animated:YES completion:nil];
    return;
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [self logOutViaFaceBook];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    [self logOutViaFaceBook];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    [self logOutViaFaceBook];
}

- (void)logOutViaFaceBook{
    //    [[FBSDKLoginManager new] logOut];
}

- (void)showFaceBookShareDialog{

    NSURL *url = [NSURL URLWithString:self.selectedNews.url];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeAutomatic;
    dialog.fromViewController = self;
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
    content.contentTitle = @"Поделиться";
    content.contentDescription = [self text];
    content.contentURL = url;
    dialog.shareContent = content;
    dialog.delegate = self;
    if (![dialog canShow]) {
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

#pragma mark - VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
    
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}

- (void)vkSdkUserAuthorizationFailed{
    [self showMessage:RPErrorMessage withTitle:nil];
}


@end
