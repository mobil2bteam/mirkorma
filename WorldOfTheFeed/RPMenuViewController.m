//
//  RPMenuViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMenuViewController.h"
#import "RPMainViewController.h"
#import "RPSignInViewController.h"
#import "RPUserOfficeViewController.h"
#import "RPShoppingCartViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPContactsViewController.h"
#import "RPMessageListVC.h"

@interface RPMenuViewController ()

@property (nonatomic, strong) UINavigationController *transitionsNavigationController;

@property (strong, nonatomic) CAGradientLayer *gradient;

@property (strong, nonatomic) UIColor *startColor;

@property (strong, nonatomic) UIColor *endColor;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIView *shoppingCartView;

@property (weak, nonatomic) IBOutlet UIView *logInView;

@property (weak, nonatomic) IBOutlet UIView *userOfficeView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userOfficeHeightZeroConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logInHeightZeroConstraint;

@end

@implementation RPMenuViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadMenu];
    self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
}

#pragma mark - Methods

- (void)reloadMenu{
    if (APP_DELEGATE.userInfo) {
        self.logInView.hidden = YES;
        self.logInHeightZeroConstraint.priority = 750;
        self.userOfficeView.hidden = NO;
        self.userOfficeHeightZeroConstraint.priority = 250;
    } else {
        self.logInView.hidden = NO;
        self.logInHeightZeroConstraint.priority = 250;
        self.userOfficeView.hidden = YES;
        self.userOfficeHeightZeroConstraint.priority = 750;
    }
    [self.view layoutIfNeeded];
}

- (void)showUserOffice{
    RPUserOfficeViewController* mainVC = [[RPUserOfficeViewController alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:mainVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (void)showMainController{
    RPMainViewController* mainVC = [[RPMainViewController alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:mainVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (void)showSignInController{
    RPSignInViewController* mainVC = [[RPSignInViewController alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:mainVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (void)showAboutUsController{
    RPContactsViewController *contactsVC = [[RPContactsViewController alloc]initWithNib];
    contactsVC.showHamburgerButton = YES;
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:contactsVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

#pragma mark - Actions

- (IBAction)mainButtonPressed:(id)sender {
    [self showMainController];
}

- (IBAction)registerButtonPressed:(id)sender {
    [self showSignInController];
}

- (IBAction)messageButtonPressed:(id)sender {
    RPMessageListVC *messageListVC = [[RPMessageListVC alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:messageListVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (IBAction)aboutUsButtonPressed:(id)sender {
    RPContactsViewController *contactsVC = [[RPContactsViewController alloc]initWithNib];
    contactsVC.showHamburgerButton = YES;
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:contactsVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (IBAction)shoppingCartButtonPressed:(id)sender {
    RPShoppingCartViewController* shoppingCartVC = [[RPShoppingCartViewController alloc]initWithNib];
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:shoppingCartVC];
    self.slidingViewController.topViewController = navVC;
    [self.slidingViewController resetTopViewAnimated:YES];
}

- (IBAction)userOfficeButtonPressed:(id)sender {
    [self showUserOffice];
}

@end
