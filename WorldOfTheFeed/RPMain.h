//
//  RPMain.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPMenu;
@class RPContent;
@class RPParam;
@class RPBanner;
@class RPLink;
@class RPItem;
@class RPPrice;
@class RPBlock;
@class RPBrand;
@class RPBrands;
@class RPNews;

@interface RPMain : NSObject <EKMappingProtocol>

@property (nonatomic, strong) RPMenu *menu;

@property (nonatomic, strong) NSArray<RPBanner *> *banners;

@property (nonatomic, strong) NSArray<RPBanner *> *advantage;

@property (nonatomic, strong) NSArray<RPBlock *> *blocks;

@property (nonatomic, strong) NSArray<RPNews *> *news;

@end


@interface RPMenu : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPContent *> *content;

@end



@interface RPBrands : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPBrand *> *brands;

@property (nonatomic, strong) NSArray<RPBrand *> *top;

@end


@interface RPContent : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *icon_1;

@property (nonatomic, copy) NSString *icon_2;

@property (nonatomic, copy) NSString *icon_3;

@end


@interface RPBanner : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *background_color;

@property (nonatomic, copy) NSString *background_img;

@property (nonatomic, copy) NSString *font_color;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, strong) RPLink *link;

@end


@interface RPItem : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *additional_text;

@property (nonatomic, strong) NSArray<RPPrice *> *prices;

@end



@interface RPBlock : NSObject <EKMappingProtocol>

@property (nonatomic, strong) RPLink *link;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSArray<RPItem *> *items;

@end


@interface RPBrand : NSObject <EKMappingProtocol>

@property (nonatomic, strong) RPLink *link;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@end


@interface RPNews : NSObject <EKMappingProtocol>

@property (nonatomic, strong) RPLink *link;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *image;

@end


@interface RPLink : NSObject <EKMappingProtocol>

+ (RPLink *) initWithName:(NSString *)name;

- (NSDictionary *)buildParams;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, strong) NSArray<RPParam *> *param;

@end


@interface RPParam : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *value;

@end


@interface RPPrice : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *weight;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, assign) NSInteger old_price;

@property (nonatomic, assign) NSInteger party_min;

@property (nonatomic, assign) NSInteger ID;

@end
