//
//  RPRepeatOrderAlertVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 6/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPOrderInfo;

@interface RPRepeatOrderAlertVC : UIViewController

@property (nonatomic, copy) void (^callback)(void);
@property (nonatomic, strong) RPOrderInfo *order;

@end
