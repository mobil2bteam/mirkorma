//
//  RPMenuView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMenuView.h"
#import "RPCacheManager.h"
#import "RPMain.h"
#import "Masonry.h"
#import "RPString+Convert.h"


@interface RPMenuView()

@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation RPMenuView

- (void)drawRect:(CGRect)rect {
    self.buttonsArray = nil;
    self.buttonsArray = [[NSMutableArray alloc]init];
    self.imageViewArray = [[NSMutableArray alloc]init];
    [self addScrollView];
}

#pragma mark - Properties

- (void)addScrollView{
    if (self.scrollView) {
        [self.scrollView removeFromSuperview];
    }
    self.scrollView = [[UIScrollView alloc]init];
    [self addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.equalTo(self);
        make.centerY.equalTo(self);
    }];
    UIView *contentView = [[UIView alloc]init];
    [self.scrollView addSubview:contentView];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView);
        make.right.equalTo(self.scrollView);
        make.height.equalTo(self.scrollView);
        make.centerY.equalTo(self.scrollView);
    }];
    [self addItemsToContentView:contentView];
}

- (void)addItemsToContentView:(UIView *)contentView{
    for (NSInteger i = 0; i< self.contentArray.count; i++) {
        UIButton *button = [[UIButton alloc] init];
        button.backgroundColor = [UIColor clearColor];
        [contentView addSubview:button];
        button.tag = i;
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.itemWidth);
            make.centerY.mas_equalTo(contentView);
            make.width.mas_equalTo(button.mas_height);
        }];
        if (!i) {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(contentView).with.offset(0);
            }];
        }
        if (i) {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo([self.buttonsArray lastObject].mas_right).with.offset(0);
            }];
        }
        if (i == self.contentArray.count - 1){
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(contentView).with.offset(0);
            }];
        }
        [self.buttonsArray addObject:button];
        [button addTarget:self action:@selector(menuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addImageViewToButtonItem:button];
    }
}

- (void)addImageViewToButtonItem: (UIButton *)button{
    UIImageView *imageView = [[UIImageView alloc]init];
    [button addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(button);
        make.centerX.mas_equalTo(button);
        make.height.mas_equalTo(button).multipliedBy(0.6);
        make.width.mas_equalTo(imageView.mas_height);
    }];
    NSString *url = self.contentArray[button.tag].icon_2;
    if (button.tag == self.selectedContentIndex) {
        url = self.contentArray[button.tag].icon_1;
    }
    [imageView RP_setImageWithURL:url];
    [self.imageViewArray addObject:imageView];
}

#pragma mark - Properties

- (NSInteger)itemWidth{
    return CGRectGetWidth(self.bounds)/6;
}

- (NSArray<RPContent *> *)contentArray{
    return APP_DELEGATE.main.menu.content;
}

#pragma mark - Actions

- (IBAction)menuButtonPressed:(UIButton *)button {
    if (self.selectedContentIndex == button.tag) {
        return;
    }
    [self.delegate didSelectItemAtIndex:button.tag];
    self.selectedContentIndex = button.tag;
    
    for (NSInteger i = 0; i < self.buttonsArray.count; i++) {
        UIButton *button2 = self.buttonsArray[i];
        NSString *imageUrlString;
        if (i == button.tag) {
            imageUrlString = self.contentArray[button2.tag].icon_1;

        } else {
            imageUrlString = self.contentArray[button2.tag].icon_2;
        }
        
        // анимация смены картинки в кнопке меню
        UIImageView *imageView = self.imageViewArray[button2.tag];
        [imageView RP_setImageWithURL:imageUrlString];
    }
}

@end
