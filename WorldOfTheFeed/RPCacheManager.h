//
//  RPCacheManager.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPCache.h"

@class RPLink;

@interface RPCacheManager : NSObject

//возвращает кэш (если есть) по url и параметрам
//может вернуть nil
+ (RPCache *)cachedRequestWithUrl:(NSString *)url andParams:(NSDictionary *)params;

//удаляет устаревший кэш
+ (void)updateCache;

//очищает кэш
+ (void)clearCache;

// кэширует запрос
+ (void)cacheRequestWithUrl:(NSString *)url andParams:(NSDictionary *)params andData:(NSDictionary *)json;

@end
