//
//  RPReservedProductTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
@class DBProduct;
@class RPBag;

@interface RPReservedProductTableViewCell : MGSwipeTableCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@property (weak, nonatomic) IBOutlet UIView *labelBackgroundView;

@property (weak, nonatomic) IBOutlet UILabel *productNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;

@property (weak, nonatomic) IBOutlet UIView *saleView;

@property (weak, nonatomic) IBOutlet UILabel *saleLabel;

- (void)configureCellWith:(DBProduct *)product;

- (void)configureCellWithBag:(RPBag *)bag;

@end
