//
//  RPPaymentTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPaymentTableViewCell.h"
#import "RPDeliveryList.h"
#import "UIImageView+AFNetworking.h"

@implementation RPPaymentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configureCellWithPayment:(RPPayment *)payment{
    self.paymentNameLabel.text = payment.name;
    self.paymentDescriptionLabel.text = payment.paymentDescription;
    NSURL *url = [NSURL URLWithString: payment.picture];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder.jpg"];
    [self.paymentImageView setImageWithURLRequest:imageRequest
                                  placeholderImage:placeholderImage
                                           success:nil
                                           failure:nil];
}

@end
