//
//  RPCatalogList.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPCatalog;

@interface RPCatalogList : NSObject<EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPCatalog *> *catalog;

@end

@interface RPCatalog : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger parent_id;

@property (nonatomic, assign) NSInteger product_count;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@end
