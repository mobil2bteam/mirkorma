//
//  RPOrderInfo.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderInfo.h"

@implementation RPOrderInfo

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"date", @"number", @"total", @"status", @"summ", @"discount", @"promo_code", @"address", @"city", @"delivery_price", @"url_payment"]];
        [mapping hasMany:[RPBag class] forKeyPath:@"bag" forProperty:@"bag"];
    }];
}

@end


@implementation RPBag

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"name", @"add_text", @"total", @"image", @"count", @"price", @"summ", @"discount"]];
    }];
}

@end
