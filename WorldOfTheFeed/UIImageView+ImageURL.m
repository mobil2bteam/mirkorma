//
//  UIImageView+ImageURL.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 2/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIImageView+ImageURL.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+PRV_Crop.h"

@implementation UIImageView (ImageURL)

- (void)RP_setImageWithURL:(NSString *)url{
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: url]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self setImageWithURLRequest:imageRequest
                              placeholderImage:[UIImage imageNamed:kPlaceholderImage]
                                       success:nil
                                       failure:nil];

}

- (void)RP_setImageWithURL:(NSString *)url cropToSquare:(BOOL) crop{
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: url]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    __weak typeof(self) weakSelf = self;
    if (crop) {
        [self setImageWithURLRequest:imageRequest
                    placeholderImage:[[UIImage imageNamed:kPlaceholderImage] cropToSquare]
                             success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                 if (image) {
                                     weakSelf.image = [image cropToSquare];
                                 }
                             }
                             failure:nil];
    } else {
        [self setImageWithURLRequest:imageRequest
                    placeholderImage:[UIImage imageNamed:kPlaceholderImage]
                             success:nil
                             failure:nil];
    }

}

@end
