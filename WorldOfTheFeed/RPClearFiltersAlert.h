//
//  RPClearFiltersAlert.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPClearFiltersAlert : UIViewController

@property (nonatomic, copy) void (^callback)(void);

@end
