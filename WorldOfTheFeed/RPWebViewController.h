//
//  RPWebViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPHtmlPage;

@interface RPWebViewController : UIViewController

@property (strong, nonatomic) RPHtmlPage *page;

@end
