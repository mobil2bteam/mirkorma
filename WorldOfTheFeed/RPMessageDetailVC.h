//
//  RPMessageDetailVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsServerModel;

@interface RPMessageDetailVC : UIViewController

@property (strong, nonatomic) RPNewsServerModel *selectedNews;

@end
