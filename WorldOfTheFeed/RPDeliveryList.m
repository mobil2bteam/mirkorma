//
//  RPDeliveryList.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDeliveryList.h"

@implementation RPDeliveryList

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPBag class] forKeyPath:@"bag" forProperty:@"bag"];
        [mapping hasMany:[RPDelivery class] forKeyPath:@"delivery_list" forProperty:@"delivery_list"];
        [mapping hasMany:[RPPayment class] forKeyPath:@"payment_list" forProperty:@"payment_list"];
        [mapping hasOne:[RPDeliveryOrder class] forKeyPath:@"order"];
    }];
}

@end

@implementation RPDelivery

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"description" toProperty:@"deliveryDescription"];
        [mapping mapPropertiesFromArray:@[ @"name", @"picture", @"price"]];
    }];
}

@end

@implementation RPPayment

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapKeyPath:@"description" toProperty:@"paymentDescription"];
        [mapping mapPropertiesFromArray:@[ @"name", @"picture"]];
    }];
}

@end

@implementation RPDeliveryOrder

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"summ", @"discount", @"total", @"weight", @"promo_code", @"delivery_price", @"friend_promo_code", @"url_payment"]];
    }];
}

@end
