//
//  RPUserOfficeViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPUserOfficeViewController.h"
#import "RPRouter.h"
#import "RPOrderTableViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+Alert.h"
#import "RPUserInfo.h"
#import "RPServerManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DBPromoCode+CoreDataProperties.h"
#import "RPOrderList.h"
#import "RPChangePasswordAlerVC.h"
#import "RPNewsListServerModel.h"

@interface RPUserOfficeViewController ()

@property (weak, nonatomic) IBOutlet UIView *stockListView;

@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (strong, nonatomic) RPOrderList *orderList;

@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@property (weak, nonatomic) IBOutlet UIButton *logOutButton;

@property (weak, nonatomic) IBOutlet UIView *topImageViewBorderView;

@property (weak, nonatomic) IBOutlet UILabel *noPromoCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *promoCodeValueLabel;

@property (weak, nonatomic) IBOutlet UIView *separatorLineView;

@property (strong, nonatomic) UIRefreshControl *scrollViewRefreshControl;

@end

@implementation RPUserOfficeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.newsList = APP_DELEGATE.newsList;
    if (self.newsList.items.count == 0) {
        self.stockListView.hidden = YES;
    }
    [self.messageCollectionView registerNib:[UINib nibWithNibName:@"RPMessageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RPMessageCollectionViewCell"];
    
    [self initialize];
    [self customize];
    if (APP_DELEGATE.orderList) {
        self.orderList = APP_DELEGATE.orderList;
    } else {
    [self addMBProgressHUD];
     [[RPServerManager sharedManager] postOrderListOnSuccess:^{
         [self hideMBProgressHUD];
         self.orderList = APP_DELEGATE.orderList;
     }];
    }
}

/*
 меняем заголовок у navigationItem для того, чтоб на следующем экране он не сдвигал текст
 */
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Личный кабинет";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - Methods

- (void)customize{
    for (UILabel *label in @[self.noPromoCodeLabel, self.promoCodeLabel]) {
        label.textColor = RPGreyColor;
    }
    for (UIButton *button in @[self.changePasswordButton, self.logOutButton]) {
        button.backgroundColor = RPGreyColor;
    }
    self.promoCodeValueLabel.textColor = RPDimRedColor;
    self.separatorLineView.backgroundColor = RPLightBlueColor;
    self.segmentControl.tintColor = RPDimRedColor;
}

- (void)initialize{
    self.scrollViewRefreshControl =  [[UIRefreshControl alloc] init];
    [self.scrollViewRefreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.ordersTableView addSubview:self.scrollViewRefreshControl];

    [self.ordersTableView registerNib:[UINib nibWithNibName:@"RPOrderTableViewCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([RPOrderTableViewCell class])];
    
    NSString *firstName = APP_DELEGATE.userInfo.first_name;
    NSString *secondName = APP_DELEGATE.userInfo.second_name;
    
    NSString *userName;
    if (firstName){
        userName = [NSString stringWithFormat:@"%@ \n", firstName];
    }
    if (secondName){
        userName = [NSString stringWithFormat:@"%@%@", userName, secondName];
    }
    self.userNameLabel.text = [userName uppercaseString];
    //проверяем наличие промо-кода
    self.promoCodeLabel.hidden = YES;
    self.promoCodeValueLabel.hidden = YES;
    self.noPromoCodeLabel.hidden = YES;
    if (APP_DELEGATE.userInfo) {
        DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
        if (promoCode) {
            self.promoCodeValueLabel.text = promoCode.promoCode;
            self.promoCodeLabel.hidden = NO;
            self.promoCodeValueLabel.hidden = NO;
        } else {
            self.noPromoCodeLabel.hidden = NO;
        }
    }
}

-(void)handleRefresh : (id)sender
{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager] postOrderListOnSuccess:^{
        self.orderList = APP_DELEGATE.orderList;
        [self.scrollViewRefreshControl endRefreshing];
        [self hideMBProgressHUD];
        [self.ordersTableView reloadData];
    }];
}
#pragma mark - Actions

- (IBAction)segmentControlValueChanged:(UISegmentedControl *)sender {
    if (self.segmentControl.selectedSegmentIndex == 0) {
        self.ordersTableView.hidden = YES;
        self.scrollView.hidden = NO;
    } else {
        self.ordersTableView.hidden = NO;
        self.scrollView.hidden = YES;
        [self.ordersTableView reloadData];
    }
}

- (IBAction)changePasswordButtonPressed:(id)sender {
    RPChangePasswordAlerVC *changePasswordAlertVC = [[RPChangePasswordAlerVC alloc]initWithNib];
    changePasswordAlertVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    changePasswordAlertVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:changePasswordAlertVC animated:YES completion:nil];
}

- (IBAction)logOutButtonPressed:(id)sender {
    [RPRouter presentLogOutVC:self];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self addMBProgressHUD];
    RPOrder *selectedOrder;
    if (self.segmentControl.selectedSegmentIndex == 1) {
        selectedOrder = self.orderList.order_active_list[indexPath.row];
    } else {
        selectedOrder = self.orderList.order_arhive_list[indexPath.row];
    }
    [[RPServerManager sharedManager]postOrderInfoWithID:selectedOrder.ID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            [RPRouter pushToOrderDescriptionControllerWithOrderInfo:orderInfo isRepeat:NO fromController:self];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentControl.selectedSegmentIndex == 1) {
        return self.orderList.order_active_list.count;
    } else if (self.segmentControl.selectedSegmentIndex == 2){
        return self.orderList.order_arhive_list.count;
    }
    return 0;
}

- (RPOrderTableViewCell *)tableView:(UITableView *)tableView
              cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPOrderTableViewCell class]) forIndexPath:indexPath];
    if (self.segmentControl.selectedSegmentIndex == 1) {
        [cell configureCellWith:self.orderList.order_active_list[indexPath.row]];
    } else if (self.segmentControl.selectedSegmentIndex == 2){
        [cell configureCellWith:self.orderList.order_arhive_list[indexPath.row]];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64.f;
}

@end
