//
//  RPMainViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPCollectionViewCellProtocol.h"
#import "RPSearchViewController.h"
#import "RPHamburgerViewController.h"

@class RPMain;

@interface RPMainViewController : UIViewController <RPCollectionViewCellProtocol>

@property (weak, nonatomic) IBOutlet UICollectionView *newsCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *blocksCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *bannersCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *advantageCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *brandCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blocksCollectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *menuBackgroundView;

@property (weak, nonatomic) IBOutlet UILabel *newsLabel;

@property (weak, nonatomic) IBOutlet UIPageControl *bannersPageControl;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
