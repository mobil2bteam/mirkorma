//
//  RPPriceTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPriceTableViewCell.h"
#import "RPMain.h"

@implementation RPPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)fillCellWithInfo:(RPPrice *)price{
    self.weightLabel.text = @"";
    self.currentPriceLabel.text = @"";
    self.oldPriceLabel.text = @"";
    self.weightLabel.text = price.weight;
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)price.price];
    if (price.old_price) {
        NSString *oldPrice = [NSString stringWithFormat:@"%ld руб.", (long)price.old_price];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:oldPrice];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        self.oldPriceLabel.attributedText = attributeString;
    }
}

@end
