//
//  RPSelectDeliveryCityViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPLocation;

@interface RPOrderStep2VC : UIViewController

@property (strong, nonatomic) RPLocation *location;

@property (strong, nonatomic) NSString *promoCode;

@end
