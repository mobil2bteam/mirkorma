//
//  RPCategoryCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPCatalog;

@interface RPCategoryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageViewLabel;

- (void)configureCellWith:(RPCatalog *)catalogue;

@end
