//
//  UIImage+PRV_Crop.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIImage+PRV_Crop.h"

@implementation UIImage (PRV_Crop)

- (UIImage *)cropToSquare{
    CGFloat newImageWidth = self.size.width;
    CGFloat newImageHeight = self.size.height;
    
    if (newImageWidth > newImageHeight){
        newImageWidth = newImageHeight;
    } else {
        newImageHeight = newImageWidth;
    }
    
    CGFloat imageWidth = self.size.width;
    CGFloat imageHeight = self.size.height;

    CGFloat x = (imageWidth - newImageWidth) / 2;
    CGFloat y = (imageHeight - newImageHeight) / 2;
    
    CGRect cropRect = CGRectMake(x, y, newImageWidth, newImageHeight);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

@end
