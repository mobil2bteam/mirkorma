//
//  RPAdressedViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAdressedViewController.h"
#import "DBAddress+CoreDataProperties.h"
#import "RPAddressTableViewCell.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPUserInfo.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"
#import "DBPromoCode+CoreDataProperties.h"
#import "RPRouter.h"
#import "RPLocations.h"
#import "DBProduct+CoreDataProperties.h"

@interface RPAdressedViewController ()

@property (strong, nonatomic) NSArray <DBAddress *> *addressArray;

@property (weak, nonatomic) IBOutlet UITableView *addressTableView;

@end

@implementation RPAdressedViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Выбирите адрес";
    [self.addressTableView registerNib:[UINib nibWithNibName:@"RPAddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPAddressTableViewCell"];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Выбирите адрес";
    [self getAddresses];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (IBAction)nextButtonPressed:(id)sender {
    [RPRouter pushOrderStep1VC:self withPromoCode:self.promoCode];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.addressArray.count;
}

- (RPAddressTableViewCell *)tableView:(UITableView *)tableView
                        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPAddressTableViewCell class]) forIndexPath:indexPath];
    __weak typeof(self) weakSelf = self;
    MGSwipeButton *deleteButton = [MGSwipeButton buttonWithTitle:@"Удалить" backgroundColor:RPDimRedColor callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
        [weakSelf removeAddressAtIndex:indexPath.row];
        return YES;
    }];
    cell.rightButtons = @[deleteButton];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    [cell configureCellWithAddress:self.addressArray[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DBAddress *selectedAddress = self.addressArray[indexPath.row];
    NSMutableDictionary *orderData = [[NSMutableDictionary alloc]init];
    orderData[@"adress"] = [NSString stringWithFormat:@"%@, %@", selectedAddress.index, selectedAddress.fullAddress];
    orderData[@"bag"] = [self bagValue];
    orderData[@"token"] = APP_DELEGATE.userInfo.token;
    orderData[@"location"] = @(selectedAddress.locationID);
    orderData[@"city"] = selectedAddress.city;
    
    //если пользователь ввел промо-код, то передаем его, иначе проверяем есть ли промо-код в БД
    if (self.promoCode.length) {
        orderData[RPPromoCodeKey] = self.promoCode;
    } else {
        DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
        if (promoCode) {
            orderData[RPPromoCodeKey] = promoCode.promoCode;
        }
    }
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postDeliveryListWithParams:orderData onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (deliveryList){
            [RPRouter pushOrderStep4VC:self withDeliveryList:deliveryList location:[self locationByID:(NSInteger)selectedAddress.locationID] data:orderData];
        } else {
            [self showMessage:RPErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}

#pragma mark - Methods

- (void)getAddresses{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %ld", APP_DELEGATE.userInfo.ID];
    self.addressArray = [DBAddress MR_findAllWithPredicate:predicate];
    [self.addressTableView reloadData];
}

- (RPLocation *)locationByID:(NSInteger)ID{
    for (RPLocation *location in APP_DELEGATE.locations.locations) {
        if (location.ID == ID) {
            return location;
        }
    }
    return nil;
}

- (NSString *)bagValue{
    NSArray <DBProduct *> *reservedProducts = [DBProduct MR_findAll];
    NSMutableString *bagString = [@"" mutableCopy];
    for (NSInteger i = 0; i < reservedProducts.count; i++){
        DBProduct *currentProduct = reservedProducts[i];
        if (i != reservedProducts.count - 1) {
            [bagString appendString:[NSString stringWithFormat:@"%ld-%hd;", (long)currentProduct.priceID, currentProduct.number]];
        } else {
            [bagString appendString:[NSString stringWithFormat:@"%ld-%hd", (long)currentProduct.priceID, currentProduct.number]];
        }
    }
    return [bagString copy];
}

- (void)removeAddressAtIndex:(NSInteger)index{
    DBAddress *selectedAddress = self.addressArray[index];
    [selectedAddress MR_deleteEntity];
    __weak typeof(self) weakSelf = self;
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            
        } else {
            [weakSelf getAddresses];
        }
    }];
}


@end
