//
//  RPFilterViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPFilterViewController.h"
#import "RPProductList.h"
#import "RPFilterCollectionViewCell.h"
#import "RPFilterHeaderCollectionViewCell.h"
#import "RPClearFiltersAlert.h"

@interface RPFilterViewController()

@property (nonatomic, strong) NSMutableArray *showSectionKeysArray;

@end

@implementation RPFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Фильтры";
    
    // добавляем кнопку 'Очистить фильтры'
    UIBarButtonItem *clearFilrtersBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Сбросить" style:UIBarButtonItemStylePlain target:self action:@selector(clearFiltersButtonTapped:)];
    self.navigationItem.rightBarButtonItem = clearFilrtersBarButton;
    
    // регистрируем ячейку и хедер для коллекции с фильтрами
    [self.filtersCollectionView registerNib:[UINib nibWithNibName:@"RPFilterHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPFilterHeaderCollectionViewCell class])];
    
    [self.filtersCollectionView registerNib:[UINib nibWithNibName:@"RPFilterCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RPFilterCollectionViewCell"];

    self.showSectionKeysArray = [NSMutableArray array];
    for (NSInteger i = 0; i < self.productList.filters.count; i++) {
        self.showSectionKeysArray[i] = @(YES);
    }
}

#pragma mark - UICollectionViewDataSource

- (RPFilterHeaderCollectionViewCell *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    RPFilterHeaderCollectionViewCell *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPFilterHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPFilterHeaderCollectionViewCell class]) forIndexPath:indexPath];
        headerView.headerLabel.text = self.productList.filters[indexPath.section].name;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
        [headerView addGestureRecognizer:tapGesture];
        headerView.tag = indexPath.section;
        reusableview = headerView;
    }
    return reusableview;
}

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer{
    self.showSectionKeysArray[gestureRecognizer.view.tag] = @(![self.showSectionKeysArray[gestureRecognizer.view.tag] boolValue]);
    [self.filtersCollectionView reloadData];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CGRectGetWidth(self.filtersCollectionView.frame), 44);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([self.showSectionKeysArray[section] boolValue] ) {
        return self.productList.filters[section].values.count;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.productList.filters.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPFilterCollectionViewCell *cell = (RPFilterCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPFilterCollectionViewCell class]) forIndexPath:indexPath];
    cell.filterLabel.text = self.productList.filters[indexPath.section].values[indexPath.row].name;
    [cell checkFilter:self.productList.filters[indexPath.section].values[indexPath.row].checked];
    return cell;
}

#pragma mark - Collection view delegates

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.productList.filters[indexPath.section].values[indexPath.row].checked = !self.productList.filters[indexPath.section].values[indexPath.row].checked;
    RPFilterCollectionViewCell *cell = (RPFilterCollectionViewCell *)[self.filtersCollectionView cellForItemAtIndexPath:indexPath];
    [cell checkFilter:self.productList.filters[indexPath.section].values[indexPath.row].checked];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *text = self.productList.filters[indexPath.section].values[indexPath.row].name;
    CGSize size = CGSizeMake(CGRectGetWidth(self.filtersCollectionView.frame), 40);
    CGRect textRect = [self calculateRectForText:text withSize:size];
    if (textRect.size.width + 20 < CGRectGetWidth(collectionView.bounds) - 16){
        return CGSizeMake(textRect.size.width + 20, 40);
    } else {
        return CGSizeMake(CGRectGetWidth(collectionView.bounds) - 16, 40);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(8, 8, 0, 8);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 8.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 8.0;
}

#pragma mark - Actions

- (IBAction)showResultButtonPressed:(id)sender {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    for (RPFilter *filter in self.productList.filters) {
        for (RPFilterItem *value in filter.values) {
            if (value.checked) {
                params[value.key] = value.value;
            }
        }
    }
    [self.delegate reloadProductListWithParams:[params copy] pop:YES];
}

#pragma mark - Methods

- (CGRect)calculateRectForText:(NSString *)text withSize:(CGSize)size{
    CGRect textRect = [text boundingRectWithSize:size
                                            options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                            context:nil];
    return textRect;
}

- (void)clearFilters{
    [self.delegate reloadProductListWithParams:@{} pop:YES];
}

#pragma mark - Actions

- (IBAction)clearFiltersButtonTapped:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPClearFiltersAlert *vc = [[RPClearFiltersAlert alloc]initWithNibName:@"RPClearFiltersAlert" bundle:nil];
    vc.callback = ^{
        [weakSelf clearFilters];
    };
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
