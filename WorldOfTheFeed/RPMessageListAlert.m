//
//  RPMessageListAlert.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessageListAlert.h"
#import "RPMessagePreviewCollectionViewCell.h"
#import "RPNewsListServerModel.h"

@interface RPMessageListAlert ()

@property (weak, nonatomic) IBOutlet UICollectionView *messageCollectionView;

@end

@implementation RPMessageListAlert

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.messageCollectionView registerNib:[UINib nibWithNibName:@"RPMessagePreviewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RPMessagePreviewCollectionViewCell"];
}

- (IBAction)cancelTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionView

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = CGRectGetWidth(self.messageCollectionView.bounds);
    return CGSizeMake(width - 16, 80);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 8, 0, 8); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.promoCodes.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPMessagePreviewCollectionViewCell *cell = (RPMessagePreviewCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPMessagePreviewCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithNews:self.promoCodes[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:NO completion:^{
        if (weakSelf.callback) {
            weakSelf.callback(weakSelf.promoCodes[indexPath.row].promocode.value);
        }
    }];
}
@end
