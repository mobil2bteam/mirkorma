//
//  UILabel+HTML.m
//  BeautyGroup
//
//  Created by Ruslan on 4/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UILabel+HTML.h"
#import "UIColor+HEX.h"

@implementation UILabel (HTML)

- (void)setHTML:(NSString *)htmlString{
    
    NSString *htmlString2 = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:%@; font-size:%fpx;}</style>", self.font.fontName, [self.textColor toHex], self.font.pointSize]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                            initWithData: [htmlString2 dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    self.attributedText = attributedString;
}

@end
