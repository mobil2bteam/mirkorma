//
//  RPEditProductAlertVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPEditProductAlertVC.h"
#import "DBProduct+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "UIViewController+Alert.h"

@interface RPEditProductAlertVC ()

@property (weak, nonatomic) IBOutlet UIStepper *productStepper;

@property (weak, nonatomic) IBOutlet UILabel *prodcutNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *prodcutCountLabel;

@property (assign, nonatomic) NSInteger productCount;

@end

@implementation RPEditProductAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions

- (void)initialize{
    // add gesture to dismiss VC
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    self.productCount = self.editProduct.number;
    self.weightLabel.text = self.editProduct.weight;
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.editProduct.price];
    self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.editProduct.number];
    NSString *description = [NSString stringWithFormat:@"Можно заказать количество, кратное %ld шт.", (long)self.editProduct.partyMin];
    self.orderDescriptionLabel.text = description;
    if (self.editProduct.oldPrice) {
        NSString *oldPrice = [NSString stringWithFormat:@"%ld руб.", (long)self.editProduct.oldPrice];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:oldPrice];
        [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
        self.oldPriceLabel.attributedText = attributeString;
    }

//    self.prodcutNameLabel.text = self.editProduct.name;
//    self.productStepper.minimumValue = self.editProduct.partyMin;
//    self.productStepper.stepValue = self.editProduct.partyMin;
//    self.productStepper.value = self.editProduct.number;
//    self.prodcutCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productStepper.value];
}

#pragma mark - Actions

- (IBAction)saveButtonPressed:(id)sender {
    self.editProduct.number = self.productCount;
    __weak typeof(self) weakSelf = self;
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            [weakSelf showMessage:@"Попробуйте, пожалуйста, еще раз!" withTitle:@"Произошла ошибка"];
        } else {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
                if (weakSelf.saveCallback) {
                    weakSelf.saveCallback();
                }
            }];
        }
    }];
}

- (IBAction)removeProductButtonPressed:(id)sender {
    if (self.productCount > self.editProduct.partyMin) {
        self.productCount = self.productCount - self.editProduct.partyMin;
        self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productCount];
    }
}

- (IBAction)addProductButtonPressed:(id)sender {
    self.productCount = self.productCount + self.editProduct.partyMin;
    self.orderCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productCount];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (IBAction)productStepperValueChanged:(id)sender {
//    self.prodcutCountLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)self.productStepper.value];
//}

@end
