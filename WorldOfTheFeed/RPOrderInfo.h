//
//  RPOrderInfo.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
@class RPBag;

@interface RPOrderInfo : NSObject <EKMappingProtocol>

+(EKObjectMapping *)objectMapping;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger number;

@property (nonatomic, assign) NSInteger summ;

@property (nonatomic, assign) NSInteger total;

@property (nonatomic, assign) NSInteger discount;

@property (nonatomic, assign) NSInteger delivery_price;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *city;

@property (nonatomic, copy) NSString *promo_code;

@property (nonatomic, strong) NSArray<RPBag *> *bag;

@property (nonatomic, copy) NSString *url_payment;

@end



@interface RPBag : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, assign) NSInteger summ;

@property (nonatomic, assign) NSInteger total;

@property (nonatomic, assign) NSInteger discount;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *add_text;

@end
