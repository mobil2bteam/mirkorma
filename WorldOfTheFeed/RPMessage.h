//
//  RPMessage.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPMessage : NSObject

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *text;

+(EKObjectMapping *)objectMapping;

@end
