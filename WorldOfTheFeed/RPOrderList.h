//
//  RPOrederList.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPOrder;

@interface RPOrderList : NSObject <EKMappingProtocol>

+(EKObjectMapping *)objectMapping;

@property (nonatomic, strong) NSArray<RPOrder *> *order_active_list;

@property (nonatomic, strong) NSArray<RPOrder *> *order_arhive_list;

@end


@interface RPOrder : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger number;

@property (nonatomic, assign) NSInteger total;

@property (nonatomic, assign) NSInteger discount;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *status;

@end
