//
//  RPLogOutVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPLogOutVC.h"

@implementation RPLogOutVC

- (IBAction)yesButtonPressed:(id)sender {
    [UserDefaults removeObjectForKey:RPTokenKey];
    [UserDefaults removeObjectForKey:RPPasswordKey];
    [UserDefaults removeObjectForKey:RPLoginKey];
    [UserDefaults synchronize];
        
    APP_DELEGATE.userInfo = nil;
    APP_DELEGATE.orderList = nil;
    [self dismissViewControllerAnimated:YES completion:^{
        [APP_DELEGATE.menuVC reloadMenu];
        [APP_DELEGATE.menuVC showSignInController];
    }];
}

- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
