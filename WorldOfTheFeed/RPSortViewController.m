//
//  RPSortViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSortViewController.h"
#import "RPOptions.h"
#import "RPFAQTableViewCell.h"

@interface RPSortViewController ()

@property (weak, nonatomic) IBOutlet UIView *blurView;

@property (weak, nonatomic) IBOutlet UIView *sortView;

@property (weak, nonatomic) IBOutlet UITableView *sortTableView;

@end

@implementation RPSortViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.sortTableView registerNib:[UINib nibWithNibName:@"RPFAQTableViewCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([RPFAQTableViewCell class])];

    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.blurView.backgroundColor = [UIColor clearColor];
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
}

- (void)viewDidLayoutSubviews{
    self.sortView.layer.masksToBounds = NO;
    self.sortView.layer.shadowOffset = CGSizeMake(-2.5f, -2.5f);
    self.sortView.layer.shadowRadius = 5;
    self.sortView.layer.shadowOpacity = 0.5;
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return APP_DELEGATE.options.sort_type.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
              cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPFAQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPFAQTableViewCell class]) forIndexPath:indexPath];
    cell.faqLabel.text = APP_DELEGATE.options.sort_type[indexPath.row].name;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPSort *selectedSort = APP_DELEGATE.options.sort_type[indexPath.row];
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:NO completion:^{
        [weakSelf.delegate selectedSort:selectedSort];
    }];
}

@end
