//
//  RPMessageListVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessageListVC.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPServerManager.h"
#import "RPNewsListServerModel.h"
#import "UIViewController+Alert.h"
#import "RPError.h"

@interface RPMessageListVC ()

@property (weak, nonatomic) IBOutlet UILabel *warningLabel;

@end

@implementation RPMessageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.messageCollectionView registerNib:[UINib nibWithNibName:@"RPMessageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RPMessageCollectionViewCell"];
    
    // добавляем логотип в navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];

    UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
    self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    self.newsList = APP_DELEGATE.newsList;
    if (self.newsList.items.count) {
        self.warningLabel.hidden = YES;
    } else {
        self.warningLabel.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}
//
//#pragma mark - Methods
//
//- (void)loadData{
//    [self addMBProgressHUD];
//    [[RPServerManager sharedManager]getMessageListOnSuccess:^(RPNewsListServerModel *newsList, RPError *error) {
//        [self hideMBProgressHUD];
//        if (error) {
//            [self showMessage:error.msg withTitle:nil];
//        } else if (newsList){
//            self.newsList = newsList;
//            [self.messageCollectionView reloadData];
//        } else {
//            [self showMessage:ErrorMessage withTitle:nil];
//        }
//    } onFailure:^(NSError *error, NSInteger statusCode) {
//        [self hideMBProgressHUD];
//        [self showMessage:ErrorMessage withTitle:nil];
//    }];
//}

#pragma mark - Actions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
