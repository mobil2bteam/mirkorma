//
//  RPShoppingCartViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

static const NSTimeInterval kGiftButtonShakeAnimationInterval = 2.5;

#import "RPShoppingCartViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPResultCollectionViewCell.h"
#import "RPRouter.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPReservedProductTableViewCell.h"
#import "RPOrderAlertViewController.h"
#import "RPServerManager.h"
#import "UIViewController+Alert.h"
#import "RPEditProductAlertVC.h"
#import "DBPromoCode+CoreDataProperties.h"
#import "RPUserInfo.h"
#import "RPDeliveryList.h"
#import "DBAddress+CoreDataProperties.h"
#import "RPAdressedViewController.h"
#import "RPMain.h"
#import "RPNewsListServerModel.h"
#import "RPMessageListAlert.h"

@interface RPShoppingCartViewController ()

@property (weak, nonatomic) IBOutlet UIButton *giftButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productsTableViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneClickViewHeightZeroConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderViewHeightZeroConstraint;

@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;

@property (strong, nonatomic) NSArray <RPBag *> *bagArray;

@property (weak, nonatomic) IBOutlet UIView *promoCodeView;

@property (weak, nonatomic) IBOutlet UILabel *salelLabel;

@property (assign, nonatomic) NSInteger totalSum;

@property (strong, nonatomic) NSArray <RPNewsServerModel *> *promoCodes;

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation RPShoppingCartViewController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [self initialize];
    [self updateViews];
    [self loadBag];
    if (!self.isMenuBarButtonHidden) {
        UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
        self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.isMenuBarButtonHidden) {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initializeTimer];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

- (void) dealloc
{
    @try {
        [self removeObserver:self forKeyPath:@"productsTableView.contentSize"];
    } @catch (NSException *exception) {
    } @finally {
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Methods

- (void)initializeTimer{
    if (!self.promoCodes.count) {
        return;
    }
    [self.timer invalidate];
    self.timer = nil;
    self.timer =  [NSTimer scheduledTimerWithTimeInterval:kGiftButtonShakeAnimationInterval
                                                   target:self
                                                 selector:@selector(shakeGiftButton)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (void)shakeGiftButton{
    [self.giftButton shakeWithOffset:10];
}

- (NSArray <RPNewsServerModel *> *)promoCodes{
    if (_promoCodes) {
        return _promoCodes;
    }
    NSMutableArray <RPNewsServerModel *> *codes = [[NSMutableArray alloc]init];
    
    for (RPNewsServerModel *news in APP_DELEGATE.newsList.items) {        
        if (news.promo) {
            [codes addObject:news];
        }
    }
    
    _promoCodes = [codes copy];
    return _promoCodes;
}

- (void)updateViews{
    if (!self.productsArray.count) {
        self.orderViewHeightZeroConstraint.priority = 750;
    } else {
        self.orderViewHeightZeroConstraint.priority = 250;
    }
    if (!APP_DELEGATE.userInfo){
        self.oneClickViewHeightZeroConstraint.priority = 750;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

- (void)loadBag{
    self.productsArray = [DBProduct MR_findAll];
    [self.productsTableView reloadData];
    // если товаров нет, то обнуляем значения и скрываем вью заказа
    if (self.productsArray.count == 0) {
        self.bagArray = nil;
        self.salelLabel.text = @"";
        self.totalSumLabel.text = @"0";
        [self updateViews];
        return;
    }
    NSDictionary *params = @{@"bag": APP_DELEGATE.bagValue,
                             @"promo_code": self.promoCodeTextField.text};
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postDeliveryListWithParams:[params mutableCopy] onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (deliveryList){
            if (!deliveryList.order.promo_code.length && self.promoCodeTextField.text.length) {
                [self showMessage:@"Введенный промо-код недействителен" withTitle:nil];
            }
            if (deliveryList.order.total != self.totalSum) {
                [self.totalSumLabel countFrom:self.totalSum to:deliveryList.order.total withDuration:1.0f];
            }
            if (deliveryList.order.discount) {
                self.salelLabel.text = [NSString stringWithFormat:@"Вы экономите: %ld руб.", (long)deliveryList.order.discount];
            } else {
                self.salelLabel.text = @"";
            }
            self.bagArray = deliveryList.bag;
            [self.totalSumLabel countFrom:self.totalSum to:deliveryList.order.total withDuration:1.0f];
            self.totalSum = deliveryList.order.total;
            self.productsArray = [DBProduct MR_findAll];
            [self.productsTableView reloadData];
            [self updateViews];

        } else {
            [self showMessage:RPErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}

- (void)initialize{
    
    [self.productsTableView registerNib:[UINib nibWithNibName:NSStringFromClass([RPReservedProductTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RPReservedProductTableViewCell class])];
     
    //если пользователь авторизован и есть промо код, то подставляем его
    if (APP_DELEGATE.userInfo) {
        DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
        if (promoCode) {
            self.promoCodeTextField.text = promoCode.promoCode;
        }
    }
    // добавляем логотип в navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    // добавляем обсервера к коллекции блоков для динамической высоты
    [self addObserver:self forKeyPath:@"productsTableView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recieveOrderNotification:)
                                                 name:@"OrderNotification"
                                               object:nil];
    
    self.totalSumLabel.method = UILabelCountingMethodEaseInOut;
    self.totalSumLabel.format = @"%d";
    self.totalSum = 0;
    
    // если нет промо-кодов, то скрываем кнопку
    if (!self.promoCodes.count) {
        self.giftButton.hidden = YES;
        [self.giftButton removeFromSuperview];
    }
}

- (void)recieveOrderNotification:(NSNotification *) notification{
    [APP_DELEGATE.menuVC reloadMenu];
    if (self.promoCodeTextField.text.length) {
        [RPRouter pushOrderStep1VC:self withPromoCode:self.promoCodeTextField.text];
    } else {
        [RPRouter pushOrderStep1VC:self withPromoCode:nil];
    }
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"productsTableView.contentSize"]) {
        CGFloat newHeight = self.productsTableView.contentSize.height;
        self.productsTableViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Actions

- (IBAction)giftButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPMessageListAlert *vc = [[RPMessageListAlert alloc]initWithNibName:@"RPMessageListAlert" bundle:nil];
    vc.callback = ^(NSString *promoCode){
        weakSelf.promoCodeTextField.text = promoCode;
        [weakSelf promoCodeButtonPresssed:nil];
    };
    vc.promoCodes = self.promoCodes;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (IBAction)promoCodeButtonPresssed:(id)sender {
    [self.view endEditing:YES];
    if (self.promoCodeTextField.text.length == 0) {
        [self.promoCodeView shake];
        return;
    }
    [self loadBag];
}

- (IBAction)quickOrderButtonPressed:(id)sender {
    NSArray <DBProduct *> *productsArray = [DBProduct MR_findAll];
    if (!productsArray.count) {
        return;
    }
    if (self.promoCodeTextField.text.length) {
        [RPRouter presentQuickOrderViewControllerFrom:self withPromoCode:self.promoCodeTextField.text];
    } else {
        [RPRouter presentQuickOrderViewControllerFrom:self withPromoCode:nil];
    }
}

- (IBAction)orderButtonPressed:(id)sender {
    NSArray <DBProduct *> *productsArray = [DBProduct MR_findAll];
    if (!productsArray.count) {
        return;
    }
    // если пользователь авторизован, то переходим на выбор региона доставки
    if (APP_DELEGATE.userInfo) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %ld", APP_DELEGATE.userInfo.ID];
        NSArray *addresses = [DBAddress MR_findAllWithPredicate:predicate];
        if (addresses.count) {
            RPAdressedViewController *adsdf = [[RPAdressedViewController alloc]initWithNib];
            adsdf.promoCode = self.promoCodeTextField.text;
            [self.navigationController pushViewController:adsdf animated:YES];
            return;
        } else {
            // если сохраненных адресов нет
            [RPRouter pushOrderStep1VC:self withPromoCode:self.promoCodeTextField.text];
        }
    } else {
        [RPRouter presentOrderAlertViewControllerFrom:self];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.productsArray.count;
}

- (RPReservedProductTableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPReservedProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPReservedProductTableViewCell class]) forIndexPath:indexPath];
    __weak typeof(self) weakSelf = self;
    cell.tag = indexPath.row;
    MGSwipeButton *deleteButton = [MGSwipeButton buttonWithTitle:@"Удалить" backgroundColor:RPDimRedColor callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
        [weakSelf removeProductAtIndex:cell.tag];
        return YES;
    }];
    MGSwipeButton *editButton = [MGSwipeButton buttonWithTitle:@"Изменить" backgroundColor:RPDarkBlueColor callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
        [weakSelf editProductAtIndex:cell.tag];
        return YES;
    }];
    cell.rightButtons = @[deleteButton, editButton];
    cell.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    [cell configureCellWith:self.productsArray[indexPath.row]];
    NSInteger price = [self priceForID:self.productsArray[indexPath.row].priceID];
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)price];
    if (price != 0) {
        if (self.bagArray[indexPath.row].discount) {
            NSInteger saleProcent = ((self.bagArray[indexPath.row].discount * 100) / self.bagArray[indexPath.row].summ);
            cell.saleLabel.text = [NSString stringWithFormat:@"%ld%c", (long)saleProcent, '%'];
            cell.saleView.hidden = NO;
        }
    }
    return cell;
}

- (NSInteger)priceForID:(NSInteger)ID{
    for (RPBag *bag in self.bagArray) {
        if (bag.ID == ID) {
            return bag.total;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPLink *link = [[RPLink alloc]init];
    link.name = @"url_product";
    RPParam *param = [[RPParam alloc]init];
    param.name = @"id";
    param.value = [NSString stringWithFormat:@"%ld", (long)self.productsArray[indexPath.row].productID];
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

#pragma mark - Methods

- (void)editProductAtIndex:(NSInteger)index{
    __weak typeof(self) weakSelf = self;
    RPEditProductAlertVC *editProductAlertVC = [[RPEditProductAlertVC alloc]initWithNib];
    editProductAlertVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    editProductAlertVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    editProductAlertVC.editProduct = self.productsArray[index];
    editProductAlertVC.saveCallback = ^{
        [weakSelf loadBag];
    };
    [self presentViewController:editProductAlertVC animated:NO completion:nil];
}

- (void)removeProductAtIndex:(NSInteger)index{
    
    __weak typeof(self) weakSelf = self;
    DBProduct *selectedProduct = self.productsArray[index];
    [selectedProduct MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            [weakSelf showMessage:@"Попробуйте, пожалуйста, еще раз!" withTitle:@"Произошла ошибка"];
        } else {
            [weakSelf loadBag];
        }
    }];
}

@end
