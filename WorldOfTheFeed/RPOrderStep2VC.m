//
//  RPSelectDeliveryCityViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderStep2VC.h"
#import "RPDeliveryRegionTableViewCell.h"
#import "RPLocations.h"
#import "RPRouter.h"

@interface RPOrderStep2VC ()

@property (weak, nonatomic) IBOutlet UIView *shadowLineView;

@property (weak, nonatomic) IBOutlet UITableView *deliveryCitiesTableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSArray <RPLocation *> *allCitiesArray;

@property (strong, nonatomic) NSArray <RPLocation *> *filteredCitiesArray;

@end

@implementation RPOrderStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Выбрать город";
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Выбрать город";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.shadowLineView.layer.masksToBounds = NO;
    self.shadowLineView.layer.shadowOffset = CGSizeMake(0.f, 1.f);
    self.shadowLineView.layer.shadowRadius = 1;
    self.shadowLineView.layer.shadowOpacity = 0.5;
}

- (void)initialize{
    [self.deliveryCitiesTableView registerNib:[UINib nibWithNibName:@"RPDeliveryRegionTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPDeliveryRegionTableViewCell"];
    [self.searchBar setTintColor:[UIColor whiteColor]];
    self.allCitiesArray = [[NSArray alloc]init];
    self.filteredCitiesArray = [[NSArray alloc]init];
    NSMutableArray *citiesArray = [[NSMutableArray alloc]init];
    for (RPLocation *location in APP_DELEGATE.locations.locations) {
        if ([self.location.region isEqualToString:location.region] && location.city.length) {
            [citiesArray addObject:location];
        }
    }
    self.filteredCitiesArray = [citiesArray copy];
    self.allCitiesArray = [citiesArray copy];
    [self.deliveryCitiesTableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.filteredCitiesArray.count;
}

- (RPDeliveryRegionTableViewCell *)tableView:(UITableView *)tableView
                       cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPDeliveryRegionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPDeliveryRegionTableViewCell class]) forIndexPath:indexPath];
    cell.deliveryRegionLabel.text = self.filteredCitiesArray[indexPath.row].city;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [RPRouter pushOrderStep3VC:self withLocation:self.filteredCitiesArray[indexPath.row] addressType:AddressTypeCity promoCode:self.promoCode];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO];
    [self.searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self filterRegions];
}

- (void)filterRegions{
    if ([self.searchBar.text isEqualToString:@""]) {
        self.filteredCitiesArray = self.allCitiesArray;
        [self.deliveryCitiesTableView reloadData];
        return;
    }
    self.filteredCitiesArray = [[NSArray alloc]init];
    NSMutableArray *regionsArray = [[NSMutableArray alloc]init];
    for (RPLocation *location in self.allCitiesArray) {
        if ([location.city rangeOfString:self.searchBar.text options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [regionsArray addObject:location];
        }
    }
    self.filteredCitiesArray = [regionsArray copy];
    [self.deliveryCitiesTableView reloadData];
}

#pragma mark - Actions

- (IBAction)addressButtonPressed:(id)sender {
    [RPRouter pushOrderStep3VC:self withLocation:self.location addressType:AddressTypeRegion promoCode:self.promoCode];
}
@end
