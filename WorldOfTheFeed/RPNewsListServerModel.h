//
//  RPNewsListServerModel.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPNewsServerModel;
@class RPPromoCodeServerModel;

@interface RPNewsListServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPNewsServerModel *> *items;

- (NSArray<RPNewsServerModel *> *)promoCodes;

@end


@interface RPNewsServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger cityId;

@property (nonatomic, assign) BOOL promo;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *add_text;

@property (nonatomic, copy) NSString *image_big;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *date_from;

@property (nonatomic, copy) NSString *date_to;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, strong) RPPromoCodeServerModel *promocode;

@end


@interface RPPromoCodeServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger discount;

@property (nonatomic, copy) NSString *value;

@end


