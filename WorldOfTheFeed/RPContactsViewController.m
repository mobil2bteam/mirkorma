//
//  RPContactsViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPContactsViewController.h"
#import "UIViewController+Alert.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPPhoneTableViewCell.h"
#import "RPOptions.h"
#import "RPServerManager.h"
#import "RPMain.h"
#import "RPOptions.h"
#import "RPRouter.h"
#import "RPFeedbackVC.h"

@interface RPContactsViewController ()

//кнопка с главным номером компании
@property (weak, nonatomic) IBOutlet UIButton *phone1Button;

//лейбл физического лица
@property (weak, nonatomic) IBOutlet UILabel *addressFizLabel;

//лейбл юридического лица
@property (weak, nonatomic) IBOutlet UILabel *addressJurLabel;

//таблица номеров компании
@property (weak, nonatomic) IBOutlet UITableView *phonesTableView;

//констрейнт высоты таблицы номеров
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phonesTableViewHeightConstraint;

@end

@implementation RPContactsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Помощь и контакты";
    if (self.showHamburgerButton) {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"phonesTableView.contentSize"];
    } @catch (NSException *exception) {
    } @finally {
    }
}

#pragma mark - Methods

- (void)initialize{
    [self.phonesTableView registerNib:[UINib nibWithNibName:@"RPPhoneTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPPhoneTableViewCell"];
    RPPhone *phone1 = APP_DELEGATE.options.phones[0];
    [self.phone1Button setTitle:phone1.value forState:UIControlStateNormal];
    self.addressFizLabel.text = APP_DELEGATE.options.address_fiz;
    self.addressJurLabel.text = APP_DELEGATE.options.address_jur;
    //добавляем обсервер для динамического изменения высоты таблицы
    [self addObserver:self forKeyPath:@"phonesTableView.contentSize" options:NSKeyValueObservingOptionOld context:nil];
    if (self.showHamburgerButton) {
        UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
        self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    }
}

#pragma mark - Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    CGFloat newHeight = self.phonesTableView.contentSize.height;
    self.phonesTableViewHeightConstraint.constant = newHeight;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

#pragma mark - Actions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

// действие кнопки [О нас]
- (IBAction)aboutUsButtonPressed:(id)sender {
    [RPRouter pushAboutUsVC:self];
}

// действие кнопки [Часто задаваемые вопросы]
- (IBAction)faqButtonPressed:(id)sender {
    [RPRouter pushFAQVC:self];
}

// действие кнопки [Заказать звонок]
- (IBAction)orderCallButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPFeedbackVC *feedbackVC = [[RPFeedbackVC alloc]initWithNib];
    feedbackVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    feedbackVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    feedbackVC.successCallback = ^{
        [weakSelf showMessage:@"Оператор свяжется с вами в ближайшее время" withTitle:nil];
    };
    [self presentViewController:feedbackVC animated:YES completion:nil];
}

// дозвон на главный номер комапнии
- (IBAction)phone1ButtonPressed:(id)sender {
    NSString *phoneNumber = APP_DELEGATE.options.phones[0].value;
    NSArray* words = [phoneNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

#pragma mark - UITableViewDataSource

// все номера, кроме первого (он отображается отдельно)
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return APP_DELEGATE.options.phones.count - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPPhoneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPPhoneTableViewCell class]) forIndexPath:indexPath];
    cell.phoneLabel.text = APP_DELEGATE.options.phones[indexPath.row + 1].value;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *phoneNumber = APP_DELEGATE.options.phones[indexPath.row + 1].value;
    NSArray* words = [phoneNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

@end
