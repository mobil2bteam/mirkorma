//
//  UIView+Shake.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/26/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shake)

- (void)shake;

- (void)shakeWithOffset:(CGFloat)offset;

@end
