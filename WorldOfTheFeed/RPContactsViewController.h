//
//  RPContactsViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPContactsViewController : UIViewController

@property (assign, nonatomic) BOOL showHamburgerButton;

@end
