//
//  RPMessagePreviewCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessagePreviewCollectionViewCell.h"
#import "RPNewsListServerModel.h"

@implementation RPMessagePreviewCollectionViewCell

- (void)configureCellWithNews:(RPNewsServerModel *)news{
    self.promoCodeLabel.text = news.promocode.value;
    self.messageLabel.text = news.add_text;
}

@end
