//
//  RPMenu.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPMain.h"
#import "RPOptions.h"
@implementation RPMain

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[RPMenu class] forKeyPath:@"menu" forProperty:@"menu"];
        [mapping hasMany:[RPBanner class] forKeyPath:@"banners" forProperty:@"banners"];
        [mapping hasMany:[RPBanner class] forKeyPath:@"advantage" forProperty:@"advantage"];
        [mapping hasMany:[RPBlock class] forKeyPath:@"blocks" forProperty:@"blocks"];
        [mapping hasMany:[RPNews class] forKeyPath:@"news" forProperty:@"news"];
    }];
}

@end

@implementation RPMenu

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPContent class] forKeyPath:@"content" forProperty:@"content"];
    }];
}

@end

@implementation RPContent

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"icon_1",
                                          @"icon_2",
                                          @"icon_3"]];
    }];
}

@end

@implementation RPBrands

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPBrand class] forKeyPath:@"brands" forProperty:@"brands"];
        [mapping hasMany:[RPBrand class] forKeyPath:@"top" forProperty:@"top"];
    }];
}

@end


@implementation RPBanner

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"background_img",
                                          @"font_color",
                                          @"text",
                                          @"background_color"]];
        [mapping hasOne:[RPLink class] forKeyPath:@"link" forProperty:@"link"];

    }];
}

@end

@implementation RPBlock

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"name" toProperty:@"name"];
        [mapping hasOne:[RPLink class] forKeyPath:@"link" forProperty:@"link"];
        [mapping hasMany:[RPItem class] forKeyPath:@"items" forProperty:@"items"];
    }];
}

@end

@implementation RPItem

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"image",
                                          @"additional_text"]];
        [mapping hasMany:[RPPrice class] forKeyPath:@"prices" forProperty:@"prices"];
    }];
}

@end

@implementation RPPrice

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"weight",
                                          @"price",
                                          @"party_min",
                                          @"old_price"]];
    }];
}

@end

@implementation RPBrand

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping hasOne:[RPLink class] forKeyPath:@"link" forProperty:@"link"];
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"image"]];
    }];
}

@end

@implementation RPNews

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[RPLink class] forKeyPath:@"link" forProperty:@"link"];
        [mapping mapPropertiesFromArray:@[@"date",
                                          @"text",
                                          @"image"]];
    }];
}

@end

@implementation RPLink

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPParam class] forKeyPath:@"param" forProperty:@"param"];
        [mapping mapKeyPath:@"name" toProperty:@"name"];
        [mapping mapKeyPath:@"url" toProperty:@"url"];
    }];
}

+ (RPLink *) initWithName:(NSString *)name{
    RPLink *link = [[RPLink alloc]init];
    link.name = name;
    return link;
}

//возвращает параметры из элемента RPLink
- (NSDictionary *)buildParams{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    if (self.param) {
        for (RPParam *param in self.param) {
            params[param.name] = param.value;
        }
    }
    for (RPUrl *url in APP_DELEGATE.options.urls) {
        if ([url.name isEqualToString:self.name]) {
            if (url.param) {
                for (RPParam *param in url.param) {
                    params[param.name] = param.value;
                }
            }
        }
    }
    return [params copy];
}

@end

@implementation RPParam

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"value"]];
    }];
}

@end





