//
//  RPOrderStep3VC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPLocation;

typedef NS_ENUM(NSUInteger, AddressType) {
    AddressTypeCountry,
    AddressTypeRegion,
    AddressTypeCity
};

@interface RPOrderStep3VC : UIViewController

@property (assign, nonatomic) AddressType addressType;

@property (strong, nonatomic) RPLocation *selectedLocation;

@property (strong, nonatomic) NSString *promoCode;

@end
