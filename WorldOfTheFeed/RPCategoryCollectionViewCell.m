//
//  RPCategoryCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCategoryCollectionViewCell.h"
#import "RPCatalogList.h"

@implementation RPCategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.categoryImageViewLabel.layer.masksToBounds = YES;
    self.categoryImageViewLabel.layer.borderColor = RPDimRedColor.CGColor;
    self.categoryImageViewLabel.layer.borderWidth = 2.f;
    self.categoryImageViewLabel.layer.cornerRadius = 5.f;
}

- (void)configureCellWith:(RPCatalog *)catalogue{
    self.categoryNameLabel.text = catalogue.name;
    self.categoryImageViewLabel.image = nil;
    [self.categoryImageViewLabel RP_setImageWithURL:catalogue.image];
}

@end
