//
//  RPSelectPetsViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRegistrationStep3VC.h"
#import "RPMain.h"
#import "UIButton+AFNetworking.h"
#import "RPServerManager.h"
#import "MBProgressHUD.h"
#import "RPRouter.h"
#import "UIViewController+Alert.h"

@interface RPRegistrationStep3VC ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray <UIButton *> *petsButtonArray;

@end

@implementation RPRegistrationStep3VC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Регистрация";
    for (NSInteger i = 1; i < APP_DELEGATE.main.menu.content.count; i++){
        NSURL *icon1Url = [NSURL URLWithString: APP_DELEGATE.main.menu.content[i].icon_1];
        NSURL *icon2Url = [NSURL URLWithString: APP_DELEGATE.main.menu.content[i].icon_2];
        NSURLRequest *imageRequestIcon1 = [NSURLRequest requestWithURL:icon1Url
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        NSURLRequest *imageRequestIcon2 = [NSURLRequest requestWithURL:icon2Url
                                                      cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval:60];
        [self.petsButtonArray[i-1] setBackgroundImageForState:UIControlStateNormal withURLRequest:imageRequestIcon2 placeholderImage:nil success:nil failure:nil];
        [self.petsButtonArray[i-1] setBackgroundImageForState:UIControlStateSelected withURLRequest:imageRequestIcon1 placeholderImage:nil success:nil failure:nil];
    }
}

#pragma mark - Methods

- (void)successfulRegistration{
    if (self.isOrderAuthorization) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderNotification" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [APP_DELEGATE.menuVC reloadMenu];
        [RPRouter pushRegistrationFinishViewControllerFrom:self];
    }
}

#pragma mark - Actions

- (IBAction)petButtonPressed:(UIButton *)button {
    button.selected = !button.selected;
}

- (IBAction)registrationButtonPressed:(id)sender {
    NSDictionary *params = [@{@"login":self.login,
                              @"password":self.password,
                              @"email":self.email,
                              @"confirm_password":self.password,
                              @"phone":self.phone,
                              @"animals": [self animalsValue],
                              @"first_name":self.firstName} copy];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postSignInWithParams:params onSuccess:^(RPUserInfo *userInfo, NSString *error) {
        if (userInfo) {
            [[RPServerManager sharedManager]postOrderListOnSuccess:^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self successfulRegistration];
            }];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (error) {
                [self showMessage:error withTitle:nil];
            } else {
                [self showMessage:ErrorMessage withTitle:nil];
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (NSString *)animalsValue{
    NSString *animals = @"";
    for (NSInteger i = 1; i < APP_DELEGATE.main.menu.content.count; i++){
        if (self.petsButtonArray[i-1].selected) {
            if (animals.length){
                animals = [NSString stringWithFormat:@"%@-%ld", animals, (long)APP_DELEGATE.main.menu.content[i].ID];
            } else {
                animals = [NSString stringWithFormat:@"%ld", (long)APP_DELEGATE.main.menu.content[i].ID];
            }
        }
    }
    return animals;
}

@end
