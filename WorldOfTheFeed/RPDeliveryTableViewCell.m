//
//  RPDeliveryTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDeliveryTableViewCell.h"
#import "RPDeliveryList.h"
#import "UIImageView+AFNetworking.h"

@implementation RPDeliveryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)configureCellWithDelivery:(RPDelivery *)delivery{
    self.deliveryPriceLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)delivery.price];
    self.deliveryNameLabel.text = delivery.name;
    self.deliveryDescriptionLabel.text = delivery.deliveryDescription;
    NSURL *url = [NSURL URLWithString: delivery.picture];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    UIImage *placeholderImage = [UIImage imageNamed:@"placeholder.jpg"];
    [self.deliveryImageView setImageWithURLRequest:imageRequest
                                 placeholderImage:placeholderImage
                                          success:nil
                                          failure:nil];
}

@end
