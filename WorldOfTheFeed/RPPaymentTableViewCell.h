//
//  RPPaymentTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPPayment;

@interface RPPaymentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *paymentImageView;

@property (weak, nonatomic) IBOutlet UILabel *paymentNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *paymentDescriptionLabel;

- (void)configureCellWithPayment:(RPPayment *)payment;

@end
