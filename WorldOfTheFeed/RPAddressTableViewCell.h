//
//  RPAddressTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@class DBAddress;

@interface RPAddressTableViewCell : MGSwipeTableCell

@property (weak, nonatomic) IBOutlet UILabel *regionLabel;

@property (weak, nonatomic) IBOutlet UILabel *fullAddressLabel;

- (void)configureCellWithAddress:(DBAddress *)address;

@end
