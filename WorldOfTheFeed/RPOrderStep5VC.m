//
//  RPOrderStep5VC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderStep5VC.h"
#import "RPDeliveryList.h"
#import "RPPaymentTableViewCell.h"
#import "RPDeliveryList.h"
#import "RPLocations.h"
#import "RPRouter.h"
#import "RPServerManager.h"
#import "UIViewController+Alert.h"

@interface RPOrderStep5VC ()

@property (weak, nonatomic) IBOutlet UITableView *paymentsTableView;

@end

@implementation RPOrderStep5VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Способ оплаты";
    [self.paymentsTableView registerNib:[UINib nibWithNibName:@"RPPaymentTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPPaymentTableViewCell"];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Способ оплаты";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.deliveryList.payment_list.count;
}

- (RPPaymentTableViewCell *)tableView:(UITableView *)tableView
                 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPPaymentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPPaymentTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithPayment:self.deliveryList.payment_list[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RPPayment *selectedPayment = self.deliveryList.payment_list[indexPath.row];
    [self continueOrderWithPayment:selectedPayment];
}

- (void)continueOrderWithPayment:(RPPayment *)payment{
    
    self.orderData[@"delivery"] = @(self.selectedDelivery.ID);
    self.orderData[@"payment"] = @(payment.ID);

    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postDeliveryListWithParams:self.orderData onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (deliveryList){
            [RPRouter pushOrderStep6VC:self withDeliveryList:deliveryList location:self.selectedLocation data:self.orderData delivery:self.selectedDelivery payment:payment];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

@end
