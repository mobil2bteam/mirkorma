//
//  RPProductCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductCollectionViewCell.h"
#import "RPMain.h"
#import "RPPriceTableViewCell.h"


@implementation RPProductCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.pricesTableView.delegate = self;
    self.pricesTableView.dataSource = self;
    [self.pricesTableView registerNib:[UINib nibWithNibName:@"RPPriceTableViewCell" bundle:nil]  forCellReuseIdentifier:NSStringFromClass([RPPriceTableViewCell class])];
}

- (void)clearData{
    self.item = nil;
    self.productImageView.image = nil;
}

- (void)fillCellWithInfo:(RPItem *)item{
    [self clearData];
    self.item = item;
    self.productNameLabel.text = item.name;
    self.productDescriptionLabel.text = item.additional_text;
    self.productImageView.image = nil;
    [self.productImageView RP_setImageWithURL:item.image];
    [self.pricesTableView reloadData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.item.prices.count;
}

- (RPPriceTableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPPriceTableViewCell class]) forIndexPath:indexPath];
    [cell fillCellWithInfo:self.item.prices[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.f;
}

@end
