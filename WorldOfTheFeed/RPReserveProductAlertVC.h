//
//  RPReserveProductAlertVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPProduct;

@interface RPReserveProductAlertVC : UIViewController

@property (strong, nonatomic) RPProduct *product;

@property (nonatomic, copy) void (^shoppingCallback)(void);

@end
