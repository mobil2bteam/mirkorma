//
//  RPRegisterFinishViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/2/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRegisterFinishViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface RPRegisterFinishViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation RPRegisterFinishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Регистрация";
    self.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern1"]];
    //hide back button
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    [self.navigationItem setHidesBackButton:YES];
    [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
}

#pragma mark - Actions

- (IBAction)userOfficeButtonPressed:(id)sender {
    [APP_DELEGATE.menuVC showUserOffice];
}

- (IBAction)moreAboutUsButtonPressed:(id)sender {
    [APP_DELEGATE.menuVC showAboutUsController];
}

- (IBAction)buyWithDiscountButtonPressed:(id)sender {
    [APP_DELEGATE.menuVC showMainController];
}

- (IBAction)inviteFriendsButtonPressed:(id)sender {
    
}

@end
