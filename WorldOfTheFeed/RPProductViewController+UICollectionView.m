//
//  RPProductViewController+UICollectionView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductViewController+UICollectionView.h"
#import "RPImageCollectionViewCell.h"
#import "RPBlockCollectionViewCell.h"
#import "RPProduct.h"
#import "RPServerManager.h"
#import "RPMain.h"

@implementation RPProductViewController (UICollectionView)

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.imagesCollectionView) {
        return self.product.images.count;
    }
    if (collectionView == self.blocksCollectionView) {
        return self.product.blocks.count;
    }
    if (collectionView == self.pricecCollectionView) {
        return self.product.prices.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.imagesCollectionView) {
        RPImageCollectionViewCell *cell = (RPImageCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWithImageUrl:self.product.images[indexPath.row].image];
        cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
        return cell;
    }
    if (collectionView == self.blocksCollectionView) {
        RPBlockCollectionViewCell *cell = (RPBlockCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPBlockCollectionViewCell class]) forIndexPath:indexPath];
        [cell fillCellWithInfo:self.product.blocks[indexPath.row]];
        cell.block = self.product.blocks[indexPath.row];
        cell.delegate = self;
        return cell;
    }
    if (collectionView == self.pricecCollectionView) {
        RPOrderCollectionViewCell *cell = (RPOrderCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPOrderCollectionViewCell class]) forIndexPath:indexPath];
        [cell configureCellWith:self.product.prices[indexPath.row]];
        cell.price = self.product.prices[indexPath.row];
        cell.delegate = self;
        return cell;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView != self.blocksCollectionView) {
        return;
    }
    [[RPServerManager sharedManager]postRequestWithLink:self.product.blocks[indexPath.row].link fromController:self];
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = width = 0;
    if (collectionView == self.blocksCollectionView) {
        height = 400;
        width = CGRectGetWidth(self.blocksCollectionView.bounds);
    }
    if (collectionView == self.imagesCollectionView) {
        height = CGRectGetHeight(self.imagesCollectionView.bounds);
        width = CGRectGetWidth(self.imagesCollectionView.bounds);
    }
    if (collectionView == self.pricecCollectionView) {
        height = 115;
        width = CGRectGetWidth(self.pricecCollectionView.bounds);
    }    
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.pricecCollectionView) {
        return 10;
    }
    return 0.0;
}

@end
