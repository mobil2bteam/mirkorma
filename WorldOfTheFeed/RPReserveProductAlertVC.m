//
//  RPReserveProductAlertVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPReserveProductAlertVC.h"
#import "UIImageView+AFNetworking.h"
#import "RPProduct.h"

@interface RPReserveProductAlertVC ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

@property (weak, nonatomic) IBOutlet UILabel *productLabel;

@end

@implementation RPReserveProductAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.productLabel.text = self.product.name;
    NSURL *url = [NSURL URLWithString: self.product.images[0].image];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.productImageView setImageWithURLRequest:imageRequest
                              placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                       success:nil
                                       failure:nil];

}

#pragma mark - Actions

- (IBAction)shoppingCartButtonPressed:(id)sender{
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:YES completion:^{
        weakSelf.shoppingCallback();
    }];
}

- (IBAction)closeButtonPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
