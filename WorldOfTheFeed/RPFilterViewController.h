//
//  RPFilterViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RPFilterViewControllerDelegate <NSObject>

- (void)reloadProductListWithParams:(NSDictionary *)params pop:(BOOL)pop;

@end

@class RPProductList;

@interface RPFilterViewController : UIViewController

@property (nonatomic, strong) RPProductList *productList;

@property (weak, nonatomic) IBOutlet UICollectionView *filtersCollectionView;

@property (weak, nonatomic) id <RPFilterViewControllerDelegate> delegate;

@end
