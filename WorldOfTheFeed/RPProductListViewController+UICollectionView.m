//
//  RPProductListViewController+UICollectionView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductListViewController+UICollectionView.h"
#import "RPProductCollectionViewCell.h"
#import "RPQuickFilterCollectionViewCell.h"
#import "RPMain.h"
#import "RPProductList.h"
#import "RPServerManager.h"
#import "MBProgressHUD.h"
#import "UIViewController+Alert.h"

@implementation RPProductListViewController (UICollectionView)

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.productsCollectionView) {
        return self.productList.items.count;
    } else {
        if ([self quickFilter] != nil) {
            return [self quickFilter].values.count;
        }
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = width = 0;
    if (collectionView == self.productsCollectionView) {
        height = 300;
        width = CGRectGetWidth(self.productsCollectionView.bounds) / 2;
    } else {
        NSString *text = [self quickFilter].values[indexPath.row].name;
        CGSize size = CGSizeMake(CGRectGetWidth(self.filtersCollectionView.frame), 30);
        CGRect textRect = [self calculateRectForText:text withSize:size];
        return CGSizeMake(textRect.size.width + 20, 30);
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.filtersCollectionView) {
        return 10;
    }
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.productsCollectionView) {
        RPLink *link = [[RPLink alloc]init];
        link.name = @"url_product";
        RPParam *param = [[RPParam alloc]init];
        param.name = @"id";
        param.value = [NSString stringWithFormat:@"%ld", (long)self.productList.items[indexPath.row].ID];
        link.param = @[param];
        [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
    }
    if (collectionView == self.filtersCollectionView) {
        RPFilterItem *value = [self quickFilter].values[indexPath.row];
        value.checked = YES;
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        for (RPFilter *filter in self.productList.filters) {
            for (RPFilterItem *value in filter.values) {
                if (value.checked) {
                    params[value.key] = value.value;
                }
            }
        }

        [self reloadProductListWithParams:[params copy] pop:NO];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.productsCollectionView) {
        RPProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPProductCollectionViewCell class]) forIndexPath:indexPath];
        [cell fillCellWithInfo:self.productList.items[indexPath.row]];
        return cell;

    } else {
        RPQuickFilterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPQuickFilterCollectionViewCell class]) forIndexPath:indexPath];
        cell.filterLabel.text = [self quickFilter].values[indexPath.row].name;
        return cell;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)
    {
        if (self.currentPage < self.productList.count_page) {
            
            self.currentPage++;
            
            // обьединяем базовые параметры запроса с новыми параметрами фильтров
            NSMutableDictionary *mutableParams = [NSMutableDictionary dictionaryWithDictionary:self.params];
            mutableParams[@"page"] = [NSString stringWithFormat:@"%ld", (long)self.currentPage];
            if (self.filterParams) {
                [mutableParams addEntriesFromDictionary:self.filterParams];
            }
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [[RPServerManager sharedManager]postProductListParams:mutableParams isBrands:self.isBrands onSuccess:^(RPProductList *productList, RPError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (error) {
                    [self showMessage:error.msg withTitle:nil];
                } else {
                    self.productList.items = [self.productList.items arrayByAddingObjectsFromArray:productList.items];
                    [self.productsCollectionView reloadData];
                    self.emptyResultView.hidden = self.productList.items.count;
                }
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self showMessage:ErrorMessage withTitle:nil];
            }];
        }
    }
}

- (CGRect)calculateRectForText:(NSString *)text withSize:(CGSize)size{
    CGRect textRect = [text boundingRectWithSize:size
                                         options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                      attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                         context:nil];
    return textRect;
}

@end
