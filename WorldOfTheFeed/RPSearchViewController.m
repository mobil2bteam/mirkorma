//
//  RPSearchViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSearchViewController.h"
#import "RPMain.h"
#import "RPServerManager.h"
#import "RPSearchView.h"
#import "Masonry.h"
#import "RPSuggest.h"

@interface RPSearchViewController () <UISearchBarDelegate>

// текущая страница с подсказками, котрую нужно показывать, по умолчанию = 1
@property (assign, nonatomic) NSInteger currentPage;

@end

@implementation RPSearchViewController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    self.currentPage = 1;
    // разделитетельная линия под навигейшн баром
    UIView *topSeparatorLine = [[UIView alloc]init];
    [self.view addSubview:topSeparatorLine];
    topSeparatorLine.backgroundColor = [UIColor lightGrayColor];
    [topSeparatorLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@1);
        make.width.equalTo(self.view.mas_width);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.searchBar.mas_bottom).with.offset(10);
    }];
    
    // загружаем вьюху поиска с nib'a
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"RPSearchView" owner:self options:nil];
    self.searchView  = [subviewArray objectAtIndex:0];
    self.searchView.delegate = self;
    [self.view addSubview:self.searchView];
    self.searchView.hidden = YES;
    [self.searchView  showSegmentView:NO];
    [self.searchView  showLastSearchView:YES];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topSeparatorLine.mas_bottom);
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.searchBar.layer.masksToBounds = YES;
    self.searchBar.layer.cornerRadius = 10;
    self.searchBar.layer.borderWidth = 1.f;
    self.searchBar.layer.borderColor = RPDimRedColor.CGColor;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
    RPLink *link = [[RPLink alloc]init];
    link.name = @"url_products";
    RPParam *param = [[RPParam alloc]init];
    param.name = @"search";
    param.value = self.searchBar.text;
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO];
    self.searchView.hidden = YES;
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.searchView.hidden = NO;
    [self.searchBar setShowsCancelButton:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    //если текст поиска кратный 3, то обновляем подсказки
    if ((self.searchBar.text.length + 1) % 3 == 0 && ![text isEqualToString:@""]) {
        self.currentPage = 1;
        [self.searchView showAllResultsView:YES];

        [self loadSuggestsInCatalogue:self.inCatalogueSelected reload:YES];
    }
    return YES;
}

- (void)loadSuggestsInCatalogue:(BOOL)inCatalogue reload:(BOOL)reload{
    RPLink *link;
    if (inCatalogue) {
        link = [[RPLink alloc]init];
        link.name = @"url_suggest";
        RPParam *term = [[RPParam alloc]init];
        term.name = @"term";
        term.value = self.searchBar.text;
        RPParam *page = [[RPParam alloc]init];
        page.name = @"page";
        page.value = [NSString stringWithFormat:@"%ld", (long)self.currentPage];
        RPParam *catalogue = [[RPParam alloc]init];
        catalogue.name = @"catalogue_id";
        catalogue.value = [NSString stringWithFormat:@"%ld", (long)self.catalogueID];
        link.param = @[term, page, catalogue];
    } else {
        link = [[RPLink alloc]init];
        link.name = @"url_suggest";
        RPParam *term = [[RPParam alloc]init];
        term.name = @"term";
        term.value = self.searchBar.text;
        RPParam *page = [[RPParam alloc]init];
        page.name = @"page";
        page.value = [NSString stringWithFormat:@"%ld", (long)self.currentPage];
        link.param = @[term, page];
    }
    [[RPServerManager sharedManager]postSuggestWithLink:link onSuccess:^(RPSuggest *suggest, RPError *error) {
        if (!error) {
            [self.searchView showLastSearchView:NO];
            [self.searchView.resultsCollectionView reloadData];
            if (reload) {
                self.searchView.suggest = suggest;
            } else {
                [self.searchView.suggest addSuggests:suggest];
            }
            [self.searchView.resultsCollectionView reloadData];
            self.currentPage++;
        }
    } onFailure:nil];
}

#pragma mark - RPSearchViewProtocol

- (void)didSelectSuggestAtIndex:(NSInteger)index{
    RPLink *link = [[RPLink alloc]init];
    link.name = @"url_product";
    RPParam *param = [[RPParam alloc]init];
    param.name = @"id";
    param.value = [NSString stringWithFormat:@"%ld", (long)self.searchView.suggest.suggestList[index].ID];
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

- (void)loadMoreSuggestsInCatalogue:(BOOL)inCatalogue reload:(BOOL)reload{
    // если reload, то обнуляем подсказки
    if (reload) {
        self.currentPage = 1;
    }
    self.inCatalogueSelected = inCatalogue;
    [self loadSuggestsInCatalogue:self.inCatalogueSelected reload:reload];
}

- (void)loadAllResults{
    RPLink *link = [[RPLink alloc]init];
    link.name = @"url_products";
    RPParam *param = [[RPParam alloc]init];
    param.name = @"search";
    param.value = self.searchBar.text;
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

@end
