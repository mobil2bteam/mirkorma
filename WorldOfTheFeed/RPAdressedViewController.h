//
//  RPAdressedViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPAdressedViewController : UIViewController

@property (nonatomic, strong) NSString *promoCode;

@end
