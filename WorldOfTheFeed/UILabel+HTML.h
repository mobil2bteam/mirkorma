//
//  UILabel+HTML.h
//  BeautyGroup
//
//  Created by Ruslan on 4/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTML)

- (void)setHTML:(NSString *)htmlString;

@end
