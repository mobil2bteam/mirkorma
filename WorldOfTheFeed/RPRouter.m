//
//  RPRouter.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRouter.h"
#import "RPMain.h"
#import "RPCatalogList.h"
#import "RPMainViewController.h"
#import "UIViewController+Alert.h"
#import "RPWebViewController.h"
#import "RPProductViewController.h"
#import "RPCatalogViewController.h"
#import "RPProductListViewController.h"
#import "RPHtmlPage.h"
#import "RPProduct.h"
#import "RPCacheManager.h"
#import "RPProductList.h"
#import "RPMessage.h"
#import "RPQuickOrderViewController.h"
#import "RPOrderAlertViewController.h"
#import "RPRegisterFinishViewController.h"
#import "RPOrderDescriptionViewController.h"
#import "RPAboutViewController.h"
#import "RPFAQViewController.h"
#import "RPOnboardViewController.h"
#import "RPContactsViewController.h"
#import "AppDelegate.h"
#import <ECSlidingViewController.h>
#import "RPMenuViewController.h"
#import "RPOrderStep1VC.h"
#import "RPOrderStep2VC.h"
#import "RPLogOutVC.h"
#import "RPOrderStep4VC.h"
#import "RPOrderStep5VC.h"
#import "RPEditProductAlertVC.h"
#import "RPShoppingCartViewController.h"

@implementation RPRouter

+ (void)pushToCatalogueControllerWithContentIndex:(NSInteger) contentIndex fromController:(UIViewController *)controller{
    RPCatalogViewController* catalogueVC = [[RPCatalogViewController alloc] initWithNib];
    catalogueVC.selectedContentIndex = contentIndex;
    [controller.navigationController pushViewController:catalogueVC animated:YES];
}

+ (void)pushToOrderDescriptionControllerWithOrderInfo:(RPOrderInfo *) orderInfo isRepeat:(BOOL)repeat fromController:(UIViewController *)controller{
    RPOrderDescriptionViewController* orderDescriptionVC = [[RPOrderDescriptionViewController alloc] initWithNib];
    orderDescriptionVC.orderInfo = orderInfo;
    orderDescriptionVC.isRepeatOrder = repeat;
    [controller.navigationController pushViewController:orderDescriptionVC animated:YES];
}

+ (void)pushQuickOrderViewControllerFrom:(UIViewController *)controller{
    RPQuickOrderViewController *quickOrderVC = [[RPQuickOrderViewController alloc] initWithNib];
    [controller.navigationController pushViewController:quickOrderVC animated:YES];
}

+ (void)pushRegistrationFinishViewControllerFrom:(UIViewController *)controller{
    RPRegisterFinishViewController *quickOrderVC = [[RPRegisterFinishViewController alloc] initWithNib];
    [controller.navigationController pushViewController:quickOrderVC animated:YES];
}

+ (void)pushShoppingCartVC:(UIViewController *)fromController{
    RPShoppingCartViewController *shoppingCartVC = [[RPShoppingCartViewController alloc]initWithNib];
    shoppingCartVC.isMenuBarButtonHidden = YES;
    [fromController.navigationController pushViewController:shoppingCartVC animated:YES];
}

+ (void)presentQuickOrderViewControllerFrom:(UIViewController *)controller withPromoCode:(NSString *)promoCode{
    RPQuickOrderViewController *quickOrderVC = [[RPQuickOrderViewController alloc] initWithNib];
    quickOrderVC.promoCode = promoCode;
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:quickOrderVC];
    [controller presentViewController:navVC animated:YES completion:nil];
}

+ (void)presentOrderAlertViewControllerFrom:(UIViewController *)controller{
    RPOrderAlertViewController *orderAlertVC = [[RPOrderAlertViewController alloc] initWithNib];
    orderAlertVC.fromController = controller;
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:orderAlertVC];
    [controller presentViewController:navVC animated:YES completion:nil];
}

+ (void)pushOrderStep1VC:(UIViewController *)fromController withPromoCode:(NSString *)promoCode{
    RPOrderStep1VC *orderStep1VC = [[RPOrderStep1VC alloc]initWithNib];
    orderStep1VC.promoCode = promoCode;
    [fromController.navigationController pushViewController:orderStep1VC animated:YES];
}

+ (void)pushOrderStep2VC:(UIViewController *)fromController withLocation:(RPLocation *)location promoCode:(NSString *)promoCode{
    RPOrderStep2VC *orderStep2VC = [[RPOrderStep2VC alloc]initWithNib];
    orderStep2VC.location = location;
    orderStep2VC.promoCode = promoCode;
    [fromController.navigationController pushViewController:orderStep2VC animated:YES];
}

+ (void)pushOrderStep3VC:(UIViewController *)fromController withLocation:(RPLocation *)location addressType:(AddressType)addressType promoCode:(NSString *)promoCode{
    RPOrderStep3VC *orderStep3VC = [[RPOrderStep3VC alloc]initWithNib];
    orderStep3VC.selectedLocation = location;
    orderStep3VC.addressType = addressType;
    orderStep3VC.promoCode = promoCode;
    [fromController.navigationController pushViewController:orderStep3VC animated:YES];
}

+ (void)pushOrderStep4VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data{
    RPOrderStep4VC *orderStep4VC = [[RPOrderStep4VC alloc]initWithNib];
    orderStep4VC.deliveryList = deliveryList;
    orderStep4VC.selectedLocation = location;
    orderStep4VC.orderData = [data mutableCopy];
    [fromController.navigationController pushViewController:orderStep4VC animated:YES];
}

+ (void)pushOrderStep5VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data delivery:(RPDelivery *)delivery{
    RPOrderStep5VC *orderStep5VC = [[RPOrderStep5VC alloc]initWithNib];
    orderStep5VC.deliveryList = deliveryList;
    orderStep5VC.selectedLocation = location;
    orderStep5VC.selectedDelivery = delivery;
    orderStep5VC.orderData = [data mutableCopy];
    [fromController.navigationController pushViewController:orderStep5VC animated:YES];
}

+ (void)pushOrderStep6VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data delivery:(RPDelivery *)delivery payment:(RPPayment *)payment{
    RPOrderStep6VC *orderStep6VC = [[RPOrderStep6VC alloc]initWithNib];
    orderStep6VC.deliveryList = deliveryList;
    orderStep6VC.selectedLocation = location;
    orderStep6VC.orderData = [data mutableCopy];
    orderStep6VC.selectedPayment = payment;
    orderStep6VC.selectedDelivery = delivery;
    [fromController.navigationController pushViewController:orderStep6VC animated:YES];
}

+ (void)presentSortVC:(UIViewController <RPSortViewControllerDelegate> *)fromController{
    RPSortViewController *sortVC = [[RPSortViewController alloc]initWithNib];
    sortVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    sortVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    sortVC.delegate = fromController;
    [fromController presentViewController:sortVC animated:NO completion:nil];
}

+ (void)presentLogOutVC:(UIViewController *)fromController{
    RPLogOutVC *logOutVC = [[RPLogOutVC alloc]initWithNib];
    logOutVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    logOutVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [fromController presentViewController:logOutVC animated:NO completion:nil];
}

+ (void)presentEditProductAlertVC:(UIViewController *)fromController withProduct:(DBProduct *)product{
    RPEditProductAlertVC *editProductAlertVC = [[RPEditProductAlertVC alloc]initWithNib];
    editProductAlertVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    editProductAlertVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    editProductAlertVC.editProduct = product;
    [fromController presentViewController:editProductAlertVC animated:NO completion:nil];
}

+ (void)pushFilterVC:(UIViewController <RPFilterViewControllerDelegate> *)fromController productList:(RPProductList *)productList{
    RPFilterViewController* filterVC = [[RPFilterViewController alloc]initWithNib];
    filterVC.productList = productList;
    filterVC.delegate = fromController;
    [fromController.navigationController pushViewController:filterVC animated:YES];
}

+ (void)pushContactsVC:(UIViewController *)fromController{
    RPContactsViewController *contactsVC = [[RPContactsViewController alloc]initWithNib];
    [fromController.navigationController pushViewController:contactsVC animated:YES];
}

+ (void)pushAboutUsVC:(UIViewController *)fromController{
    RPAboutViewController* aboutVC = [[RPAboutViewController alloc]initWithNib];
    [fromController.navigationController pushViewController:aboutVC animated:YES];
}

+ (void)pushFAQVC:(UIViewController *)fromController{
    RPFAQViewController* faqVC = [[RPFAQViewController alloc]initWithNib];
    [fromController.navigationController pushViewController:faqVC animated:YES];
}

+ (void)setUpStartController{
    // если это первый запуск, то показываем заставки
    if ([UserDefaults valueForKey:RPWasFirstLoad]) {
        RPMenuViewController* menuVC = [[RPMenuViewController alloc]     initWithNibName:@"RPMenuViewController" bundle:nil];
        
        RPMainViewController* mainVC = [[RPMainViewController alloc]     initWithNibName:@"RPMainViewController" bundle:nil];
        
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:mainVC];
        
        ECSlidingViewController *slidingVC = [[ECSlidingViewController alloc]initWithTopViewController:navController];
        slidingVC.underLeftViewController = menuVC;
        APP_DELEGATE.window.rootViewController = slidingVC;
        APP_DELEGATE.menuVC = menuVC;
    } else {
        [UserDefaults setBool:YES forKey:RPWasFirstLoad];
        [UserDefaults synchronize];
        RPOnboardViewController* onboardingVC = [[RPOnboardViewController alloc]     initWithNibName:@"RPOnboardViewController" bundle:nil];
        APP_DELEGATE.window.rootViewController = onboardingVC;
    }
}

+ (void)pushToControllerWithData:(NSDictionary *)data withLink:(RPLink *)link withParams:(NSDictionary *)params fromController:(UIViewController *)controller{
    RPMessage *message = [EKMapper objectFromExternalRepresentation:data[@"message"] withMapping:[RPMessage objectMapping]];
    if (data[@"cache"]) {
        [RPCacheManager cacheRequestWithUrl:link.name andParams:params andData:data];
    }
    if (data[@"html_page"]) {
        [controller hideMBProgressHUD];
        RPHtmlPage *page = [EKMapper objectFromExternalRepresentation:data[@"html_page"] withMapping:[RPHtmlPage objectMapping]];
        RPWebViewController *webVC = [[RPWebViewController alloc] init];
        webVC.page = page;
        [controller.navigationController pushViewController:webVC animated:YES];
        return;
    }

    if ([link.name isEqualToString:@"url_product"]) {
        if (data[@"item"]) {
            [controller hideMBProgressHUD];
            RPProduct *product = [EKMapper objectFromExternalRepresentation:data[@"item"] withMapping:[RPProduct objectMapping]];
            [product removeEmptyProducts];
            RPProductViewController *productVC = [[RPProductViewController alloc]     initWithNibName:@"RPProductViewController" bundle:nil];
            productVC.product = product;
            productVC.message = message;
            [controller.navigationController pushViewController:productVC animated:YES];
            return;
        }
    }
    if ([link.name isEqualToString:@"url_products"]) {
        if (data[@"products"]) {
            [controller hideMBProgressHUD];
            RPProductList *productList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPProductList objectMapping]];
            [productList removeEmptyProducts];
            
            RPProductListViewController *productListVC = [[RPProductListViewController alloc]     initWithNibName:@"RPProductListViewController" bundle:nil];
            productListVC.productList = productList;
            productListVC.message = message;
            productListVC.params = [params mutableCopy];
            [controller.navigationController pushViewController:productListVC animated:YES];
            return;
        }
    }
    if ([link.name isEqualToString:@"url_brand"]) {
        if (data[@"products"]) {
            [controller hideMBProgressHUD];
            RPProductList *productList = [EKMapper objectFromExternalRepresentation:data withMapping:[RPProductList objectMapping]];
            [productList removeEmptyProducts];
            RPProductListViewController *productListVC = [[RPProductListViewController alloc]     initWithNibName:@"RPProductListViewController" bundle:nil];
            productListVC.productList = productList;
            productListVC.message = message;
            productListVC.params = [params mutableCopy];
            productListVC.isBrands = YES;
            [controller.navigationController pushViewController:productListVC animated:YES];
            return;
        }
    }
    [controller hideMBProgressHUD];
}

@end
