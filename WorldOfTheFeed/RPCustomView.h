//
//  RPCustomView.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface RPCustomView : UIView

@property (nonatomic) IBInspectable UIColor *contextColor;

@property (nonatomic) IBInspectable CGFloat height;

@end
