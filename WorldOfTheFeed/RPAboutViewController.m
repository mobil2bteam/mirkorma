//
//  RPAboutViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAboutViewController.h"
#import "UIViewController+Alert.h"
#import "UIViewController+ECSlidingViewController.h"

@interface RPAboutViewController()

@property (weak, nonatomic) IBOutlet UILabel *aboutUsLabel;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation RPAboutViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.needHamburgerButton) {
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

#pragma mark - Methods

- (void)initialize{
    NSString *aboutUsText = self.textLabel.text;
    [self addMBProgressHUD];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [aboutUsText dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.scrollView.hidden = NO;
        [self hideMBProgressHUD];
        self.aboutUsLabel.attributedText = attributedString;
    });
    self.navigationItem.title = @"О нас";
    if (self.needHamburgerButton) {
        UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
        self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    }
}

#pragma mark - Actions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
