//
//  UIViewController+TextFieldDelegate.m
//
//  Created by Johannes Luderschmidt on 04.02.15.
//

#import "UIViewController+TextFieldDelegate.h"
#import "UITextField+RuntimeExtension.h"

@implementation UIViewController (TextFieldDelegate)
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL shouldChange = YES;
    
    if(textField.maxLength){
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
            shouldChange = (newLength > [textField.maxLength integerValue]) ? NO : YES;
        if(!shouldChange){
            return shouldChange;
        }
    }
    
    if(textField.isDecimalField){
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        NSNumber *number = [formatter numberFromString:string];
        
        NSNumber *currentNumber = [formatter numberFromString:textField.text];
        NSString *text = textField.text;
        text = [NSString stringWithFormat:@"%@%@",text, string];
        NSNumber *newNumber = [formatter numberFromString:text];
        if((number && (!currentNumber || ([currentNumber integerValue] != [newNumber integerValue])) && [newNumber integerValue] < 999999999) || [string isEqualToString:@""]){
            shouldChange = YES;
        }else{
            shouldChange = NO;
        }
        if(!shouldChange){
            return shouldChange;
        }
    }
    return shouldChange;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.isEMailAddress){
        if(![self stringIsValidEmail:textField.text] && ![textField.text isEqualToString:@""]){
            UIAlertView *noMailAddressAlert = [[UIAlertView alloc]
                           initWithTitle:@"Not a valid E-Mail address" message:[NSString stringWithFormat:@"%@ ist not a valid E-Mail address. Please enter a valid E-Mail address.", textField.text] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [noMailAddressAlert show];
            textField.text = @"";
        }
    }
}

-(BOOL) stringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
