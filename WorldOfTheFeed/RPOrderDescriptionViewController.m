//
//  RPOrderDescriptionViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderDescriptionViewController.h"
#import "RPReservedProductTableViewCell.h"
#import "RPOrderInfo.h"
#import "RPOptions.h"
#import "RPPaymentVC.h"
#import "RPServerManager.h"
#import "UIViewController+Alert.h"
#import "RPRepeatOrderAlertVC.h"
#import "RPRouter.h"

@interface RPOrderDescriptionViewController ()

@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;

@property (weak, nonatomic) IBOutlet UILabel *summOrderLabel;

@property (weak, nonatomic) IBOutlet UILabel *summDiscountLabel;

@property (weak, nonatomic) IBOutlet UILabel *summDeliveryLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLastUpdate;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@property (weak, nonatomic) IBOutlet UITableView *bagTableView;

@property (weak, nonatomic) IBOutlet UIButton *payButton;

@end

@implementation RPOrderDescriptionViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    [self.bagTableView registerNib:[UINib nibWithNibName:@"RPReservedProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPReservedProductTableViewCell"];
    [self configureViews];
    if (self.isRepeatOrder && self.orderInfo.url_payment.length) {
        [self showMessage:@"Вам необходимо оплатить заказ! Перейдите к оплате нажав на кнопку - Перейти к оплате" withTitle:nil];
    }
}

- (IBAction)segmentControlPressed:(UISegmentedControl *)sender {
    self.bagTableView.hidden = !sender.selectedSegmentIndex;
}

#pragma mark - Methods

- (void)configureViews{
    self.navigationItem.title = [NSString stringWithFormat:@"Заказ №%ld", (long)self.orderInfo.number];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%ld Руб.", (long)self.orderInfo.total];
    self.summOrderLabel.text = [NSString stringWithFormat:@"%ld Руб.", (long)self.orderInfo.summ];
    self.summDeliveryLabel.text = [NSString stringWithFormat:@"%ld Руб.", (long)self.orderInfo.delivery_price];
    self.summDiscountLabel.text = [NSString stringWithFormat:@"%ld Руб.", (long)self.orderInfo.discount];
    self.dateLastUpdate.text = self.orderInfo.date;
    self.cityLabel.text = self.orderInfo.city;
    self.addressLabel.text = self.orderInfo.address;
    self.orderStatusLabel.text = self.orderInfo.status;
    if (!self.orderInfo.url_payment.length) {
        [self.payButton removeFromSuperview];
    }
}

- (void)updateOrder{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postOrderInfoWithID:self.orderInfo.ID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            self.orderInfo = orderInfo;
            [self configureViews];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (void)repeatOrder{
    __weak typeof(self) weakSelf = self;
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postRepeatOrderWithID:self.orderInfo.ID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            [self showMessage:@"Заказ оформлен повторно" withOkHandler:^(UIAlertAction *action) {
                [weakSelf loadNewOrderWithID:orderInfo.ID];
            }];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (void)loadNewOrderWithID:(NSInteger)orderID{
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postOrderInfoWithID:orderID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            [RPRouter pushToOrderDescriptionControllerWithOrderInfo:orderInfo isRepeat:YES fromController:self];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

#pragma mark - Actions

- (IBAction)callButtonPressed:(id)sender {
    NSString *phoneNumber = APP_DELEGATE.options.phones[0].value;
    NSArray* words = [phoneNumber componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* phone = [words componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phone]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (IBAction)payButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPPaymentVC *paymentVC = [[RPPaymentVC alloc]initWithNib];
    paymentVC.orderID = self.orderInfo.ID;
    paymentVC.paymentURL = self.orderInfo.url_payment;
    paymentVC.successCallback = ^(){
        [weakSelf updateOrder];
    };
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:paymentVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (IBAction)repeatOrderButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPRepeatOrderAlertVC *vc = [[RPRepeatOrderAlertVC alloc]initWithNib];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    vc.order = self.orderInfo;
    vc.callback = ^{
        [weakSelf repeatOrder];
    };
    [self presentViewController:vc animated:NO completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.orderInfo.bag.count;
}

- (RPReservedProductTableViewCell *)tableView:(UITableView *)tableView
                        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPReservedProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPReservedProductTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithBag:self.orderInfo.bag[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

@end
