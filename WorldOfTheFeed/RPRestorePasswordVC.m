//
//  RPRestorePasswordVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/26/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPRestorePasswordVC.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"

@interface RPRestorePasswordVC ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation RPRestorePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.loginTextField) {
        [self.emailTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)restorePasswordButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (!self.emailTextField.text.length || !self.loginTextField.text.length) {
        [self.loginTextField shake];
        return;
    }
    NSMutableDictionary *params = [@{@"action":@"forgotpassword"} mutableCopy];
    if (self.emailTextField.text.length) {
        params[@"email"] = self.emailTextField.text;
    }
    if (self.loginTextField.text.length) {
        params[@"login"] = self.loginTextField.text;
    }
    [self addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager]postChangePasswordWithParams:params onSuccess:^(BOOL isSuccess, NSString *error) {
        [self hideMBProgressHUD];
        if (isSuccess) {
            [weakSelf dismissViewControllerAnimated:YES completion:^{
                weakSelf.successCallback();
            }];
        } else{
            [self showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:RPErrorMessage withTitle:nil];
    }];
}
@end
