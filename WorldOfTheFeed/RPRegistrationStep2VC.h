//
//  RPRegistrationStep2VC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRegistrationStep2VC : UIViewController

@property (strong, nonatomic) NSString *firstName;

@property (strong, nonatomic) NSString *password;

@property (strong, nonatomic) NSString *login;

@property (assign, nonatomic) BOOL isOrderAuthorization;

@end
