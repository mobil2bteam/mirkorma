//
//  RPBlockCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPBlockCollectionViewCell.h"
#import "RPProductCollectionViewCell.h"
#import "RPMain.h"

@implementation RPBlockCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.blockCollectionView.delegate = self;
    self.blockCollectionView.dataSource = self;
    self.blockCollectionView.layer.shouldRasterize = YES;
    self.blockCollectionView.layer.rasterizationScale = [UIScreen mainScreen].nativeScale;
    [self.blockCollectionView registerNib:[UINib nibWithNibName:@"RPProductCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPProductCollectionViewCell class])];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.block.items.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = width = 0;
    if (collectionView == self.blockCollectionView) {
        height = CGRectGetHeight(self.blockCollectionView.bounds);
        width = CGRectGetWidth(self.blockCollectionView.bounds) * 0.5;
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate pressedOnBlock:self.tag andProduct:indexPath.row];
}

- (RPProductCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPProductCollectionViewCell class]) forIndexPath:indexPath];
    [cell fillCellWithInfo:self.block.items[indexPath.row]];
    cell.item = self.block.items[indexPath.row];
    return cell;
}

- (void)fillCellWithInfo:(RPBlock *)block{
    if ([self.block.name isEqualToString:@"РАСПРОДАЖА"]) {
        UIImage *tiledImage = [UIImage imageNamed:@"pattern2"];
        self.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:tiledImage];
        self.backgroundColor = [UIColor colorWithRed:0.973 green:0.365 blue:0.384 alpha:1.0];
    } else {
        UIImage *tiledImage = [UIImage imageNamed:@"pattern1"];
        self.backgroundImageView.backgroundColor = [UIColor colorWithPatternImage:tiledImage];
        self.backgroundColor = [UIColor colorWithRed:0.290 green:0.792 blue:0.902 alpha:1.0];
    }
    self.blockTitleLabel.text = block.name;
}

@end
