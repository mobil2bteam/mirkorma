//
//  RPEditProductAlertVC.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DBProduct;

@interface RPEditProductAlertVC : UIViewController

@property (strong, nonatomic) DBProduct *editProduct;

@property (nonatomic, copy) void (^saveCallback)(void);

@property (weak, nonatomic) IBOutlet UILabel *orderDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *weightLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderCountLabel;

@end
