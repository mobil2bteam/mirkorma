//
//  RPSelectPromoCodeAlert.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPSelectPromoCodeAlert.h"
#import "AppDelegate.h"
#import "RPNewsListServerModel.h"
#import "RPFAQTableViewCell.h"

@interface RPSelectPromoCodeAlert ()

@property (weak, nonatomic) IBOutlet UITableView *promoCodeTableView;

@end

@implementation RPSelectPromoCodeAlert


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.promoCodeTableView registerNib:[UINib nibWithNibName:@"RPFAQTableViewCell" bundle:nil] forCellReuseIdentifier:@"RPFAQTableViewCell"];
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.promoCodes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPFAQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPFAQTableViewCell class]) forIndexPath:indexPath];
    cell.faqLabel.text = self.promoCodes[indexPath.row].promocode.value;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:NO completion:^{
        if (weakSelf.callback) {
            weakSelf.callback(weakSelf.promoCodes[indexPath.row].promocode.value);
        }
    }];
}

@end
