//
//  RPRegistrationStep2VC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

static NSString * const RPEmptyTextFieldMessage = @"Заполните все поля!";

#import "RPRegistrationStep2VC.h"
#import "RPRegistrationStep3VC.h"
#import "UIViewController+Alert.h"
#import "RPServerManager.h"
#import "SHSPhoneLibrary.h"

@interface RPRegistrationStep2VC ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneTextField;

@end

@implementation RPRegistrationStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.phoneTextField.formatter setDefaultOutputPattern:@"+7 (###) ###-##-##"];
}

//меняем заголовок у navigationItem для того, чтоб на следующем экране он не сдвигал текст
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Регистрация";
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}

- (IBAction)continueRegistration:(id)sender {
    if (!self.emailTextField.text.length || !self.phoneTextField.text.length) {
        [self showMessage:RPEmptyTextFieldMessage withTitle:nil];
        return;
    }
    if (self.phoneTextField.phoneNumberWithoutPrefix.length != 11) {
        [self showMessage:RPIncorrectPhoneMessage withTitle:nil];
        return;
    }
    NSDictionary *params = [@{@"email":self.emailTextField.text,
                              @"phone":[self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1],
                              @"action":@"validate"} copy];
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postSignInWithParams:params onSuccess:^(RPUserInfo *userInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else {
            RPRegistrationStep3VC *step3VC = [[RPRegistrationStep3VC alloc]initWithNib];
            step3VC.password = self.password;
            step3VC.firstName = self.firstName;
            step3VC.login = self.login;
            step3VC.email = self.emailTextField.text;
            step3VC.phone = [self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1];
            step3VC.isOrderAuthorization = self.isOrderAuthorization;
            [self.navigationController pushViewController:step3VC animated:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.emailTextField) {
        [self.phoneTextField becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

@end
