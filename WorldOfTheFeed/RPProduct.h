//
//  RPProduct.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
@class RPPrice;
@class RPBlock;
@class RPImage;
@class RPComment;

@interface RPProduct : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *composition;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *additional_text;

@property (nonatomic, copy) NSString *descriptionProduct;

@property (nonatomic, strong) NSArray<RPPrice *> *prices;

@property (nonatomic, strong) NSArray<RPImage *> *images;

@property (nonatomic, assign) BOOL check_comment;

@property (nonatomic, strong) NSArray<RPBlock *> *blocks;

@property (nonatomic, strong) NSArray<RPComment *> *comments;

- (void)removeEmptyProducts;

@end


@interface RPImage : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *image;

@end


@interface RPComment : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *comment;

@property (nonatomic, assign) NSInteger bal;

@end
