//
//  RPSearchViewProtocol.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPSearchViewProtocol <NSObject>

- (void)didSelectSuggestAtIndex:(NSInteger)index;

- (void)loadMoreSuggestsInCatalogue:(BOOL)inCatalogue reload:(BOOL)reload;

- (void)loadAllResults;

@end
