//
//  RPResultCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPItem;

@interface RPResultCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *resultImageView;

@property (weak, nonatomic) IBOutlet UILabel *resultNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *resultDescriptionLabel;

- (void)configureCellWith:(RPItem *)item;

@end
