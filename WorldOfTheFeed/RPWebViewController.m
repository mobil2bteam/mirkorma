//
//  RPWebViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPWebViewController.h"
#import "RPHtmlPage.h"
#import "Masonry.h"

@implementation RPWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //добавляем webView и устанавливаем констрейнты
    UIWebView *webView = [[UIWebView alloc]init];
    [self.view addSubview:webView];
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    self.navigationItem.title = self.page.title;
    NSURL *url = [NSURL URLWithString:self.page.url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
}

@end
