//
//  RPQuickOrderViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPQuickOrderViewController.h"
#import "UIViewController+Alert.h"
#import "SHSPhoneLibrary.h"
#import "RPUserInfo.h"
#import "RPServerManager.h"
#import "DBProduct+CoreDataProperties.h"
#import "DBPromoCode+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPDeliveryList.h"
#import "RPRouter.h"

static CGFloat const RPAnimationDuration = 0.5f;

@interface RPQuickOrderViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *orderView;

@property (weak, nonatomic) IBOutlet UIView *gratitudeView;

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneTextField;

@property (strong, nonatomic) RPDeliveryList *deliveryList;

@end


@implementation RPQuickOrderViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    [self.phoneTextField.formatter setDefaultOutputPattern:@"+7 (###) ###-##-##"];
    if ([UserDefaults stringForKey:RPPhoneKey]) {
        [self.phoneTextField setFormattedText:[UserDefaults stringForKey:RPPhoneKey]];
    } else {
        [self.phoneTextField setFormattedText:APP_DELEGATE.userInfo.phone];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)changeView{
    [UIView animateWithDuration:RPAnimationDuration animations:^{
        self.orderView.alpha = 0.f;
    } completion:^(BOOL finished) {
        self.orderView.hidden = YES;
        self.gratitudeView.alpha = 0.f;
        self.gratitudeView.hidden = NO;
        [UIView animateWithDuration:RPAnimationDuration animations:^{
            self.gratitudeView.alpha = 1.f;
        }];
    }];
}

- (void)clearShoppingCartAndPromoCode{
    //очищаем корзину
    NSArray *productsArray = [DBProduct MR_findAll];
    for (DBProduct *product in productsArray) {
        [product MR_deleteEntity];
    }
    //удаляем промо код
    DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
    if (promoCode) {
        [promoCode MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
    }];
}

#pragma mark - Actions

- (IBAction)orderButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.phoneTextField.phoneNumberWithoutPrefix.length == 11) {
        // save phone
        [UserDefaults setObject:[self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1] forKey:RPPhoneKey];
        [UserDefaults synchronize];
        
        // send request
        [self addMBProgressHUD];
        NSMutableDictionary *orderData = [@{@"bag":APP_DELEGATE.bagValue,
                                    @"action":@"fast_order",
                                    @"phone":[self.phoneTextField.phoneNumberWithoutPrefix substringFromIndex:1],
                                    @"token":APP_DELEGATE.userInfo.token} mutableCopy];
        if (APP_DELEGATE.userInfo.first_name) {
            orderData[@"name"] = APP_DELEGATE.userInfo.first_name;
        }
        if (APP_DELEGATE.userInfo.email) {
            orderData[@"email"] = APP_DELEGATE.userInfo.email;
        }
        if (self.promoCode) {
            orderData[RPPromoCodeKey] = self.promoCode;
        } else {
            DBPromoCode *promoCode = [DBPromoCode MR_findFirstByAttribute:@"userID" withValue:@(APP_DELEGATE.userInfo.ID)];
            if (promoCode) {
                orderData[RPPromoCodeKey] = promoCode.promoCode;
            }
        }
        [[RPServerManager sharedManager]postDeliveryListWithParams:[orderData mutableCopy] onSuccess:^(RPDeliveryList *deliveryList, NSString *error) {
            [self hideMBProgressHUD];
            if (error) {
                [self showMessage:error withTitle:nil];
            } else {
                self.deliveryList = deliveryList;
                [self changeView];
                [self clearShoppingCartAndPromoCode];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self hideMBProgressHUD];
            [self showMessage:RPErrorMessage withTitle:nil];
        }];
    } else {
        [self showMessage:RPIncorrectPhoneMessage withTitle:nil];
    }
}

- (IBAction)continueShoppingButtonPressed:(id)sender {
    [APP_DELEGATE.menuVC showMainController];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showOrderButtonPressed:(id)sender {
    [self addMBProgressHUD];
    [[RPServerManager sharedManager]postOrderInfoWithID:self.deliveryList.order.ID onSuccess:^(RPOrderInfo *orderInfo, NSString *error) {
        [self hideMBProgressHUD];
        if (error) {
            [self showMessage:error withTitle:nil];
        } else if (orderInfo){
            [RPRouter pushToOrderDescriptionControllerWithOrderInfo:orderInfo isRepeat:NO fromController:self];
        } else {
            [self showMessage:ErrorMessage withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self hideMBProgressHUD];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
