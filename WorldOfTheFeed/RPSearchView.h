//
//  RPSearchView.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPSearchViewProtocol.h"
@class RPSuggest;

@interface RPSearchView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *blurView;

@property (weak, nonatomic) IBOutlet UIView *segmentView;

@property (weak, nonatomic) IBOutlet UIView *lastSearchesView;

@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *catalogueSegmentControl;

@property (strong, nonatomic) RPSuggest *suggest;

@property (weak, nonatomic) id <RPSearchViewProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIView *allResultsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allResultsViewHeightConstraint;

- (void)showSegmentView:(BOOL)show;

- (void)showLastSearchView:(BOOL)show;

- (void)showAllResultsView:(BOOL)show;

@end
