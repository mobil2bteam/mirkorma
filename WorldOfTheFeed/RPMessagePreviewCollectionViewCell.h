//
//  RPMessagePreviewCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsServerModel;

@interface RPMessagePreviewCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

- (void)configureCellWithNews:(RPNewsServerModel *)news;

@end
