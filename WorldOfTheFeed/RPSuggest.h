//
//  RPSuggest.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
@class RPItem;

@interface RPSuggest : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPItem *> *suggestList;

@property (nonatomic, assign) NSInteger cache;

+ (EKObjectMapping *)objectMapping;

- (void)addSuggests:(RPSuggest *)suggest;

@end
