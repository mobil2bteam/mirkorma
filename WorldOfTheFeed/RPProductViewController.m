//
//  RPProductViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductViewController.h"
#import "RPImageCollectionViewCell.h"
#import "RPBlockCollectionViewCell.h"
#import "RPOrderCollectionViewCell.h"
#import "RPProduct.h"
#import "RPServerManager.h"
#import "RPMain.h"
#import "RPMessage.h"
#import "UIViewController+Alert.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DBProduct+CoreDataProperties.h"
#import "RPRouter.h"
#import "RPReserveProductAlertVC.h"
#import "MWPhotoBrowser.h"

@interface RPProductViewController() <MWPhotoBrowserDelegate>
@end

@implementation RPProductViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imagesCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class])];
    
    [self.pricecCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPOrderCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPOrderCollectionViewCell class])];
    
    [self.blocksCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPBlockCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPBlockCollectionViewCell class])];
    
    // fill info about product
    self.productNameLabel.text = self.product.name;
    self.descriptionLabel.text = self.product.descriptionProduct;
    self.navigationItem.title = self.product.name;
    
    // add observer to automatic update collectionViews's heights
    [self addObserver:self forKeyPath:@"blocksCollectionView.contentSize" options:NSKeyValueObservingOptionOld context:nil];
    [self addObserver:self forKeyPath:@"pricecCollectionView.contentSize" options:NSKeyValueObservingOptionOld context:nil];
    
    // add shppingCartBarButton
    UIBarButtonItem *shoppingCartBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart"] style:UIBarButtonItemStylePlain target:self action:@selector(shoppingCartButtonTapped:)];
    self.navigationItem.rightBarButtonItem = shoppingCartBarButton;
    
    [self calculateSaleForProduct];
    
    if (self.product.descriptionProduct.length) {
        [self addMBProgressHUD];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [self.product.descriptionProduct dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideMBProgressHUD];
            self.descriptionLabel.attributedText = attributedString;
        });
    }
    
    self.buyWithOneClickViewZeroHeightConstraint.priority = 750;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view layoutIfNeeded];
    });
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.message) {
        [self showMessage:self.message.text withTitle:self.message.title];
        self.message = nil;
    }
    // update product's count in shopping cart
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu", (long)APP_DELEGATE.shoppingCartItemsCount];
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"blocksCollectionView.contentSize"];
        [self removeObserver:self forKeyPath:@"pricecCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark - Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"blocksCollectionView.contentSize"]) {
        CGFloat newHeight = self.blocksCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.blocksCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
    if ([keyPath isEqualToString:@"pricecCollectionView.contentSize"]) {
        CGFloat newHeight = self.pricecCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.pricecCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Methods

//check if product has sale
- (void)calculateSaleForProduct{
    NSInteger sale = 0;
    for (RPPrice *price in self.product.prices) {
        if (!price.old_price) {
            continue;
        }
        NSInteger currentProcent = ((price.old_price - price.price) * 100.f) / price.old_price;
        if (currentProcent > sale) {
            sale = currentProcent;
        }
    }
    if (sale) {
        self.saleView.hidden = NO;
        self.saleLabel.text = [NSString stringWithFormat:@"-%ld%c", (long)sale, '%'];
    } else {
        self.saleView.hidden = YES;
    }
}

#pragma mark - Actions

- (IBAction)shoppingCartButtonTapped:(id)sender {
        [RPRouter pushShoppingCartVC:self];
}

- (void)presentImages{
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = NO; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:browser];
    
    [self presentViewController:navVC animated:NO completion:nil];
    // Present
//    [self.navigationController pushViewController:browser animated:NO];
}

- (IBAction)imageTapped:(id)sender {
    [self presentImages];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count) {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

- (NSArray *)photos{
    if (_photos) {
        return _photos;
    }
    NSMutableArray *temp = [NSMutableArray array];
    for (RPImage *image in self.product.images) {
        [temp addObject:[MWPhoto photoWithURL:[NSURL URLWithString:image.image]]];
    }
    _photos = [temp copy];
    return _photos;
}

- (IBAction)segmentControlChanged:(UISegmentedControl *)sender {
    if (!sender.selectedSegmentIndex) {
        [self showDescriptionView:NO];
    } else if (sender.selectedSegmentIndex == 1){
        [self showDescriptionView:YES];
    }
}

- (void)showDescriptionView:(BOOL)show{
    self.descriptionViewHeightConstraint.priority = !show ? 900 : 500;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark - RPCollectionViewCellProtocol

- (void)pressedOnBlock:(NSInteger)block andProduct: (NSInteger)product{
    RPLink *link = [[RPLink alloc]init];
    link.name = @"url_product";
    RPParam *param = [[RPParam alloc]init];
    param.name = @"id";
    param.value = [NSString stringWithFormat:@"%ld", (long)self.product.blocks[block].items[product].ID];
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

#pragma mark - RPOrderCollectionViewCellDelegate

- (void)buyProductWithNumber:(NSInteger)number price:(RPPrice *)price{
    // если продукт уже есть в корзине, то обновляем его
    DBProduct *existProduct = [DBProduct MR_findFirstByAttribute:@"priceID" withValue:@(price.ID)];
    if (!existProduct) {
        DBProduct *product = [DBProduct MR_createEntity];
        product.priceID = price.ID;
        if (self.product.images.count) {
            product.imageUrl = self.product.images[0].image;
        }
        product.name = self.product.name;
        product.productDescription = self.product.additional_text;
        product.price = price.price;
        product.oldPrice = price.old_price;
        product.number = number;
        product.productID = self.product.ID;
        product.partyMin = price.party_min;
        product.weight = price.weight;
    } else {
        existProduct.number = existProduct.number + number;
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            
        }
    }];
    // update product's count in shopping cart
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu", (long)APP_DELEGATE.shoppingCartItemsCount];
    RPReserveProductAlertVC *reserveProductVC = [[RPReserveProductAlertVC alloc]initWithNib];
    reserveProductVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    reserveProductVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    reserveProductVC.product = self.product;
    __weak typeof(self) weakSelf = self;
    reserveProductVC.shoppingCallback = ^{
        [weakSelf shoppingCartButtonTapped:nil];
    };
    [self presentViewController:reserveProductVC animated:YES completion:nil];
}


//#pragma mark - Test
//
//- (void)scrollViewDidScroll:(UIScrollView*)scrollView{
//    
//    // this is just a demo method on how to compute the scale factor based on the current contentOffset
//    float scale = 1.0f + fabsf(scrollView.contentOffset.y)  / scrollView.frame.size.height;
//    
//    //Cap the scaling between zero and 1
//    scale = MAX(0.0f, scale);
//    
//    // Set the scale to the imageView
//    self.imagesCollectionView.transform = CGAffineTransformMakeScale(scale, scale);
//}

@end

