//
//  RPMessageListVC+UICollectionView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessageListVC+UICollectionView.h"
#import "RPMessageCollectionViewCell.h"
#import "RPNewsListServerModel.h"
#import "RPMessageDetailVC.h"

@implementation RPMessageListVC (UICollectionView)

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.newsList.items.count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.messageCollectionView.bounds) - 20, 140);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 10, 0, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPMessageCollectionViewCell *cell = (RPMessageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPMessageCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWithItem:self.newsList.items[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    RPMessageDetailVC *vc = [[RPMessageDetailVC alloc]initWithNibName:@"RPMessageDetailVC" bundle:nil];
    vc.selectedNews = self.newsList.items[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
