//
//  RPProductCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPItem;

@interface RPProductCollectionViewCell : UICollectionViewCell <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *pricesTableView;

@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

@property (strong, nonatomic) RPItem *item;

- (void)fillCellWithInfo:(RPItem *)item;

@end
