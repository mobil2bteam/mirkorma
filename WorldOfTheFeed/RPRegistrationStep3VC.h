//
//  RPSelectPetsViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPRegistrationStep3VC : UIViewController

@property (strong, nonatomic) NSString *firstName;

@property (strong, nonatomic) NSString *email;

@property (strong, nonatomic) NSString *password;

@property (strong, nonatomic) NSString *login;

@property (strong, nonatomic) NSString *phone;

@property (assign, nonatomic) BOOL isOrderAuthorization;

@end

