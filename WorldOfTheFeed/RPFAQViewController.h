//
//  RPFAQViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/7/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPFAQViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *faqTableView;

@end
