//
//  RPProductListViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPProductListViewController.h"
#import "RPProductCollectionViewCell.h"
#import "RPQuickFilterCollectionViewCell.h"
#import "RPRouter.h"
#import "RPMain.h"
#import "RPOptions.h"
#import "RPProductList.h"
#import "RPSearchView.h"
#import "RPServerManager.h"
#import "RPCacheManager.h"
#import "RPString+Convert.h"
#import "RPSuggest.h"
#import "UIViewController+Alert.h"
#import "RPMessage.h"
#import "MBProgressHUD.h"

@interface RPProductListViewController() <RPFilterViewControllerDelegate, RPSortViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterCollectionViewWidthConstraint;

@end

@implementation RPProductListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Товары";

    [self addObserver:self forKeyPath:@"filtersCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    self.emptyResultView.hidden = self.productList.items.count;
    self.productsCollectionView.layer.shouldRasterize = YES;
    self.productsCollectionView.layer.rasterizationScale = [UIScreen mainScreen].nativeScale;
   // self.catalogueID = self.productList.catalogue_id;
    self.currentPage = 1;
    
    [self.productsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPProductCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPProductCollectionViewCell class])];

    [self.filtersCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPQuickFilterCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPQuickFilterCollectionViewCell class])];

    // add filterBarButton and shoppingCartBarButton to rightBarButtonItems
    UIBarButtonItem *filterBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter"] style:UIBarButtonItemStylePlain target:self action:@selector(filterBarButtonPressed:)];
    UIBarButtonItem *shoppingCartBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart"] style:UIBarButtonItemStylePlain target:self action:@selector(shoppingCartButtonTapped:)];
    self.navigationItem.rightBarButtonItems = @[shoppingCartBarButton, filterBarButton];

    // set sort type
    for (RPSort *sort in APP_DELEGATE.options.sort_type) {
        if ([sort.dir isEqualToString:self.productList.dir] && [sort.sort isEqualToString:self.productList.sort]) {
            if (![self.sortButton.titleLabel.text isEqualToString:sort.name]) {
                [self.sortButton setTitle:sort.name forState:UIControlStateNormal];
            }
        }
    }
}

- (RPFilter *)quickFilter{
    NSArray *filterNames = @[@"Для кого", @"Производитель", @"Вид товара"];
    for (NSString *filterName in filterNames) {
        for (RPFilter *filter in self.productList.filters) {
            if ([filter.name isEqualToString:filterName]) {
                // проверяем не выбран ли этот фильтр
                NSInteger items = 0;
                NSInteger real_count = 0;
                BOOL checked = NO;
                for (RPFilterItem *item in filter.values) {
                    if (item.checked) {
                        checked = YES;
                        break;
                    } else {
                        items ++;
                        real_count += item.count;
                    }
                }
                if (!checked && items > 1 && real_count > 10) {
                    // возвращаем массив
                    self.quickFilterLabel.text = [[NSString stringWithFormat:@"%@:", filter.name] uppercaseString];
                    return filter;
                }
            }
        }
    }
    [self.filterScrollView removeFromSuperview];
    [self.view layoutIfNeeded];
    @try {
        [self removeObserver:self forKeyPath:@"filtersCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    return nil;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // if we have message to show to user, show it and remove after
    if (self.message) {
        [self showMessage:self.message.text withTitle:self.message.title];
        self.message = nil;
    }
    self.navigationItem.rightBarButtonItems[0].badgeValue = [NSString stringWithFormat:@"%ld", (long)APP_DELEGATE.shoppingCartItemsCount];
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"filtersCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"filtersCollectionView.contentSize"]) {
        CGFloat newWidth = self.filtersCollectionView.collectionViewLayout.collectionViewContentSize.width;
        self.filterCollectionViewWidthConstraint.constant = newWidth;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}


#pragma mark - Actions

// [Выбрать фильтры]
- (IBAction)filterBarButtonPressed:(id)sender {
    [RPRouter pushFilterVC:self productList:self.productList];
}

- (IBAction)sortButtonPressed:(id)sender {
    [RPRouter presentSortVC:self];
}

- (IBAction)shoppingCartButtonTapped:(id)sender {
    [RPRouter pushShoppingCartVC:self];
}

#pragma mark - RPFilterViewControllerDelegate

- (void)selectedSort:(RPSort *)sort{
    self.params[@"dir"] = sort.dir;
    self.params[@"sort"] = sort.sort;
    // обьединяем базовые параметры запроса с новыми параметрами фильтров
    NSMutableDictionary *mutableParams = [NSMutableDictionary dictionaryWithDictionary:self.params];
    [mutableParams addEntriesFromDictionary:self.filterParams];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postProductListParams:mutableParams isBrands:self.isBrands onSuccess:^(RPProductList *productList, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg withTitle:nil];
        } else {
            [self.sortButton setTitle:sort.name forState:UIControlStateNormal];
            [self.productsCollectionView setContentOffset:CGPointZero animated:NO];
            self.productList = productList;
            [self.productsCollectionView reloadData];
            self.emptyResultView.hidden = self.productList.items.count;
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

#pragma mark - RPFilterViewControllerDelegate

// обновляет товары, после смены фильтров
- (void)reloadProductListWithParams:(NSDictionary *)filterParams pop:(BOOL)pop{
    if (pop) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    self.currentPage = 1;
    self.filterParams = filterParams;
    // обьединяем базовые параметры запроса с новыми параметрами фильтров
    NSMutableDictionary *mutableParams = [NSMutableDictionary dictionaryWithDictionary:self.params];
    [mutableParams addEntriesFromDictionary:filterParams];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postProductListParams:mutableParams isBrands:self.isBrands onSuccess:^(RPProductList *productList, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg withTitle:nil];
        } else {
            [self.productsCollectionView setContentOffset:CGPointZero animated:NO];
            self.productList = productList;
            self.emptyResultView.hidden = self.productList.items.count;
            [self.productsCollectionView reloadData];
            [self.filtersCollectionView reloadData];
            [self.filtersCollectionView setScrollsToTop:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:ErrorMessage withTitle:nil];
    }];
}

@end
