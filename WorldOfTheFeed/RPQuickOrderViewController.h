//
//  RPQuickOrderViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPQuickOrderViewController : UIViewController

@property (strong, nonatomic) NSString *promoCode;

@end
