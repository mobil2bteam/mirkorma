//
//  RPCatalogViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPMain.h"
#import "RPCatalogList.h"
#import "RPSearchViewController.h"
#import "RPMenuViewProtocol.h"

@class RPSearchView;
@class RPMessage;
@class RPMenuView;
@class MASConstraint;

@interface RPCatalogViewController : RPSearchViewController <RPMenuViewProtocol>

// вью для категорий, кроме бренда
@property (weak, nonatomic) IBOutlet UIView *categoryView;

// вью для бренда
@property (weak, nonatomic) IBOutlet UIView *brandView;

@property (weak, nonatomic) IBOutlet RPMenuView *menuView;

// коллекция подкатегорий товаров
@property (weak, nonatomic) IBOutlet UICollectionView *categoryCollectionView;

// коллекция брендов
@property (weak, nonatomic) IBOutlet UICollectionView *brandCollectionView;

// лейбл для указания названия категории
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

//отфильтрированный массив подкатегорий
//@property (nonatomic, strong) NSArray<RPCatalog *> *filteredCatalogArray;

//отфильтрированный массив брэндов по алфавиту
@property (nonatomic, strong) NSArray <NSArray<RPBrand *> *> *sortedBrandArray;

//@property (strong, nonatomic) RPCatalogList *catalogList;

// сообщение для пользователя, может и не быть
@property (strong, nonatomic) RPMessage *message;

// выбранный элемент из массива [contents]
@property (assign, nonatomic) NSInteger selectedContentIndex;

@end
