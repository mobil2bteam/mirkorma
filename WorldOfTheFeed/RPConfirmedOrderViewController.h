//
//  RPConfirmedOrderViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPDeliveryList;

@interface RPConfirmedOrderViewController : UIViewController

@property (strong, nonatomic) RPDeliveryList *deliveryList;

@property (strong, nonatomic) NSMutableDictionary *orderData;

@end
