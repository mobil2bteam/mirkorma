//
//  RPMenuCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPMain.h"
@class RPContent;

@interface RPMenuCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;

@property (weak, nonatomic) IBOutlet UILabel *menuTitleLabel;

- (void)fillCellWithInfo:(RPContent *)content;

@end
