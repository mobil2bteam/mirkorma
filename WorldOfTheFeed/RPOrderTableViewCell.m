//
//  RPOrderTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOrderTableViewCell.h"
#import "RPOrderList.h"

@implementation RPOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWith:(RPOrder *)order{
    self.orderStatusLabel.text = order.status;
    self.orderPriceLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)order.total];
    self.orderNumberLabel.text = [NSString stringWithFormat:@"%ld", (long)order.number];
    self.orderDateLabel.text = order.date;
}

@end
