//
//  RPDeliveryTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPDelivery;

@interface RPDeliveryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *deliveryDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *deliveryNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *deliveryPriceLabel;

@property (weak, nonatomic) IBOutlet UIImageView *deliveryImageView;

- (void)configureCellWithDelivery:(RPDelivery *)delivery;

@end
