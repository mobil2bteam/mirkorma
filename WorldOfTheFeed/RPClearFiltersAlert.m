//
//  RPClearFiltersAlert.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPClearFiltersAlert.h"

@interface RPClearFiltersAlert ()

@end

@implementation RPClearFiltersAlert

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clearButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.callback) {
            weakSelf.callback();
        }
    }];
}

@end
