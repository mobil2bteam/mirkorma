//
//  RPRouter.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/26/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RPSortViewController.h"
#import "RPFilterViewController.h"
#import "RPOrderStep3VC.h"
#import "RPOrderStep6VC.h"

@class RPMain;
@class RPLink;
@class RPOrderInfo;
@class RPProductList;
@class RPLocation;
@class RPDeliveryList;
@class DBProduct;
@class RPDelivery;
@class RPPayment;

@interface RPRouter : NSObject

+ (void)setUpStartController;

+ (void)pushToControllerWithData:(NSDictionary *)data withLink:(RPLink *)link withParams:(NSDictionary *)params fromController:(UIViewController *)controller;

+ (void)pushToCatalogueControllerWithContentIndex:(NSInteger) contentIndex fromController:(UIViewController *)controller;

+ (void)pushToOrderDescriptionControllerWithOrderInfo:(RPOrderInfo *) orderInfo isRepeat:(BOOL)repeat fromController:(UIViewController *)controller;

+ (void)pushQuickOrderViewControllerFrom:(UIViewController *)controller;

+ (void)pushRegistrationFinishViewControllerFrom:(UIViewController *)controller;

+ (void)presentQuickOrderViewControllerFrom:(UIViewController *)controller withPromoCode:(NSString *)promoCode;

+ (void)presentOrderAlertViewControllerFrom:(UIViewController *)controller;

+ (void)presentSortVC:(UIViewController <RPSortViewControllerDelegate> *)fromController;

+ (void)presentLogOutVC:(UIViewController *)fromController;

+ (void)presentEditProductAlertVC:(UIViewController *)fromController withProduct:(DBProduct *)product;

+ (void)pushFilterVC:(UIViewController <RPFilterViewControllerDelegate> *)fromController productList:(RPProductList *)productList;

+ (void)pushContactsVC:(UIViewController *)fromController;

+ (void)pushAboutUsVC:(UIViewController *)fromController;

+ (void)pushFAQVC:(UIViewController *)fromController;

+ (void)pushShoppingCartVC:(UIViewController *)fromController;

// order

+ (void)pushOrderStep1VC:(UIViewController *)fromController withPromoCode:(NSString *)promoCode;

+ (void)pushOrderStep2VC:(UIViewController *)fromController withLocation:(RPLocation *)location promoCode:(NSString *)promoCode;

+ (void)pushOrderStep3VC:(UIViewController *)fromController withLocation:(RPLocation *)location addressType:(AddressType)addressType promoCode:(NSString *)promoCode;

+ (void)pushOrderStep4VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data;

+ (void)pushOrderStep5VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data delivery:(RPDelivery *)delivery;

+ (void)pushOrderStep6VC:(UIViewController *)fromController withDeliveryList:(RPDeliveryList *)deliveryList location:(RPLocation *) location data:(NSMutableDictionary *)data delivery:(RPDelivery *)delivery payment:(RPPayment *)payment;

// end order
@end
