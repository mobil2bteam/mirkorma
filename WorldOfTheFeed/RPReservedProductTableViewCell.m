//
//  RPReservedProductTableViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 12/8/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPReservedProductTableViewCell.h"
#import "DBProduct+CoreDataProperties.h"
#import "RPOrderInfo.h"

@implementation RPReservedProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.productNameLabel.textColor = RPLightBlueColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.productImageView.layer.masksToBounds = YES;
    self.productImageView.layer.borderColor = RPLightBlueColor.CGColor;
    self.productImageView.layer.borderWidth = 2.f;
    self.productImageView.layer.cornerRadius = 10.f;
}

- (void)configureCellWith:(DBProduct *)product{
//    if (product.oldPrice > product.price) {
//        NSInteger saleProcent = ((product.oldPrice - product.price) * 100.f) / product.oldPrice;
//        self.saleLabel.text = [NSString stringWithFormat:@"%ld%c", (long)saleProcent, '%'];
//        self.saleView.hidden = NO;
//    }
    NSInteger totalPrice = (NSInteger)(product.number * product.price);
    self.productPriceLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)totalPrice];
    self.productNumberLabel.text = [NSString stringWithFormat:@"%hd шт.", product.number];
    self.productNameLabel.text = product.name;
    [self.productImageView RP_setImageWithURL:product.imageUrl];
}

- (void)configureCellWithBag:(RPBag *)bag{
    self.productPriceLabel.text = [NSString stringWithFormat:@"%ld Р.", (long)bag.total];
    self.productNumberLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)bag.count];
    self.productNameLabel.text = bag.name;
    [self.productImageView RP_setImageWithURL:bag.image];
}

- (void)prepareForReuse{
    self.productPriceLabel.text = [NSString stringWithFormat:@"0 Р."];
    self.productNumberLabel.text = [NSString stringWithFormat:@"0 шт."];
    self.productNameLabel.text = @"";
    self.productImageView.image = nil;
    self.saleLabel.text = @"";
    self.saleView.hidden = YES;
}

@end
