//
//  RPPhoneTableViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPPhoneTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end
