//
//  RPHamburgerViewController.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPHamburgerViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@implementation RPHamburgerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
    self.navigationItem.leftBarButtonItem = hamburgerBarButton;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
