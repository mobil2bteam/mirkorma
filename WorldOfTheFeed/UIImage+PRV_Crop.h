//
//  UIImage+PRV_Crop.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PRV_Crop)

- (UIImage *)cropToSquare;

@end
