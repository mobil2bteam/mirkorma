//
//  RPShoppingCartViewController+UICollectionViewDelegate.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 9/1/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPShoppingCartViewController+UICollectionViewDelegate.h"
#import "RPResultCollectionViewCell.h"

@implementation RPShoppingCartViewController (UICollectionViewDelegate)

#pragma mark - UICollectionView

//- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    CGFloat width = CGRectGetWidth([UIScreen mainScreen].bounds);
//    return CGSizeMake(width - 2,80);
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 4, 0, 0); // top, left, bottom, right
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 0.0;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    return 0.0;
//}
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return self.productsArray.count;
//}
//
//- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    RPResultCollectionViewCell *cell = (RPResultCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPResultCollectionViewCell class]) forIndexPath:indexPath];
//    DBProduct *currentProduct = self.productsArray[indexPath.row];
//    cell.resultNameLabel.text = currentProduct.name;
//    cell.resultDescriptionLabel.text = currentProduct.productDescription;
//    [cell.resultImageView RP_setImageWithURL:currentProduct.imageUrl];
//    return cell;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//
//}

@end
