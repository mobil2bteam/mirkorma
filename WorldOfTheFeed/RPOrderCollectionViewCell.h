//
//  RPOrderCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPPrice;

@protocol RPOrderCollectionViewCellDelegate <NSObject>

- (void)buyProductWithNumber:(NSInteger)number price:(RPPrice *)price;

@end

@interface RPOrderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *weightLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderCountLabel;

@property (strong, nonatomic) RPPrice *price;

@property (assign, nonatomic) NSInteger productCount;

@property (weak, nonatomic) id <RPOrderCollectionViewCellDelegate> delegate;

- (void)configureCellWith:(RPPrice *)price;

@end
