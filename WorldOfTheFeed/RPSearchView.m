//
//  RPSearchView.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSearchView.h"
#import "RPResultCollectionViewCell.h"
#import "RPMain.h"
#import "RPSuggest.h"
#import "RPCacheManager.h"
#import "RPString+Convert.h"
#import "Masonry.h"

static NSInteger const RPSegmentViewHeight = 44;

static NSInteger const RPLastResultViewHeight = 44;

@implementation RPSearchView

- (void)awakeFromNib{
    [super awakeFromNib];
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.blurView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    [self.resultsCollectionView registerNib:[UINib nibWithNibName:@"RPResultCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPResultCollectionViewCell class])];
    self.resultsCollectionView.delegate = self;
    self.resultsCollectionView.dataSource = self;
    }

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    // загружается кэш последнего поиска, если такой есть
    RPCache *lastResultsCache = [RPCacheManager cachedRequestWithUrl:@"url_suggest" andParams:nil];
    if (lastResultsCache) {
        RPSuggest *suggest = [EKMapper objectFromExternalRepresentation:[lastResultsCache.json dictionaryValue] withMapping:[RPSuggest objectMapping]];
        self.suggest = suggest;
        [self.resultsCollectionView reloadData];
    }
}

#pragma mark - UICollectionView

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = CGRectGetWidth([UIScreen mainScreen].bounds);
    return CGSizeMake(width,80);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {

    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.suggest.suggestList.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPResultCollectionViewCell *cell = (RPResultCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPResultCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWith:self.suggest.suggestList[indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate didSelectSuggestAtIndex:indexPath.row];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)
    {
        BOOL inCatalogue = self.catalogueSegmentControl.selectedSegmentIndex;
        [self.delegate loadMoreSuggestsInCatalogue:inCatalogue reload:NO];
    }
}

#pragma mark - Actions

- (IBAction)segmentChanged:(id)sender {
    BOOL inCatalogue = self.catalogueSegmentControl.selectedSegmentIndex == 1;
    [self.delegate loadMoreSuggestsInCatalogue:inCatalogue reload:YES];
}

#pragma mark - Methods

- (void)showLastSearchView:(BOOL)show{
    if (show) {
        [self.lastSearchesView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(RPLastResultViewHeight));
        }];
    } else {
        [self.lastSearchesView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
    self.lastSearchesView.hidden = !show;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
    });
}

- (void)showSegmentView:(BOOL)show{
    if (show) {
        [self.segmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(RPSegmentViewHeight));
        }];
    } else {
        [self.segmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
    self.segmentView.hidden = !show;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
    });
}

- (void)showAllResultsView:(BOOL)show{
    self.allResultsViewHeightConstraint.constant = show ? 30 : 0;
    [self layoutIfNeeded];
}

- (void)loadMoreSuggests{
    BOOL inCatalogue = self.catalogueSegmentControl.selectedSegmentIndex;
    [self.delegate loadMoreSuggestsInCatalogue:inCatalogue reload:NO];
}

- (IBAction)showAllResultsButtonPressed:(id)sender {
    [self.delegate loadAllResults];
}

@end
