//
//  AppDelegate.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPMenuViewController.h"

@class RPOptions;
@class RPMain;
@class RPBrands;
@class RPCatalogList;
@class RPCatalog;
@class RPUserInfo;
@class RPLocations;
@class RPOrderList;
@class RPNewsListServerModel;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

/*!
 * @brief Options for application (urls, phone numbers etc)
 */
@property (strong, nonatomic) RPOptions *options;

/*!
 * @brief Data for main controller (banners, news, menu etc)
 */
@property (strong, nonatomic) RPMain *main;

/*!
 * @brief Catalogue list
 */
@property (strong, nonatomic) RPCatalogList *catalogueList;

/*!
 * @brief All brands
 */
@property (strong, nonatomic) RPBrands *brands;

/*!
 * @brief Info about current user
 */
@property (strong, nonatomic) RPUserInfo *userInfo;

/*!
 * @brief Locations for order
 */
@property (strong, nonatomic) RPLocations *locations;

/*!
 * @brief List with orders, if user is logged in
 */
@property (strong, nonatomic) RPOrderList *orderList;

/*!
 * @brief Dictionary with filtered catalogues
 */
@property (strong, nonatomic) NSDictionary <NSString *,  NSArray<RPCatalog *> *> *filteredCatalogueDictionary;

/*!
 * @brief Side menu view controller
 */
@property (strong, nonatomic) RPMenuViewController *menuVC;

@property (strong, nonatomic) RPNewsListServerModel *newsList;

#pragma mark - Methods

/*!
 @discussion Filter catalogue list
 */
- (void)filterCatalogueList;

/*!
 * @discussion Get array with catalogues by ID
 * @param ID catalogue identifier
 * @return Array with catalogues
 */
- (NSArray <RPCatalog *> *)catalogueArrayByID:(NSInteger)ID;

/*!
 * @discussion Get url address by name
 * @param name Url name
 * @return Url address
 */
- (NSString *)urlFromName:(NSString *)name;

/*!
 * @discussion Get formatted string with reserved products for order (format: "productID-productCount")
 * @return String for order
 */
- (NSString *)bagValue;

/*!
 * @discussion Get number of reserved products
 * @return Reserved products count
 */- (NSInteger)shoppingCartItemsCount;


@end

