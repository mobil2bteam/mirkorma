//
//  RPMenuViewProtocol.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/9/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPMenuViewProtocol <NSObject>

- (void)didSelectItemAtIndex:(NSInteger )index;

@end
