//
//  RPMessageCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPNewsServerModel;

@interface RPMessageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIImageView *messageImageView;

@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

- (void)configureCellWithItem:(RPNewsServerModel *)item;

@end
