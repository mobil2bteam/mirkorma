//
//  FacebookActivity.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 2/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "FacebookActivity.h"

@implementation FacebookActivity

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (NSString *)activityType
{
    return @"ru.suslin.mirkorma";
}

- (NSString *)activityTitle
{
    return @"Facebook";
}

+ (UIActivityCategory)activityCategory{
    return UIActivityCategoryShare;
}

- (UIImage *)activityImage
{
    // Note: These images need to have a transparent background and I recommend these sizes:
    // iPadShare@2x should be 126 px, iPadShare should be 53 px, iPhoneShare@2x should be 100
    // px, and iPhoneShare should be 50 px. I found these sizes to work for what I was making.
    
    return [UIImage imageNamed:@"Facebook"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
}

- (UIViewController *)activityViewController
{
    return nil;
}

- (void)performActivity
{
    self.successCallback();
    [self activityDidFinish:YES];
}

@end
