//
//  RPMessageCollectionViewCell.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 4/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPMessageCollectionViewCell.h"
#import "RPNewsListServerModel.h"
#import "UILabel+HTML.h"

@implementation RPMessageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.messageImageView.layer.borderWidth = 2.f;
    self.messageImageView.layer.borderColor = [UIColor colorWithRed:1.000 green:0.208 blue:0.318 alpha:1.0].CGColor;
}

- (void)configureCellWithItem:(RPNewsServerModel *)item{
    [self.messageImageView RP_setImageWithURL:item.image];
    [self.messageLabel setHTML:item.text];
    self.discountLabel.text = item.add_text;
}

@end
