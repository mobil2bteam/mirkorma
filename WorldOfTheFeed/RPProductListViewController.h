//
//  RPProductListViewController.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPItem;
@class RPProductList;
@class RPMessage;
@class RPFilter;

@interface RPProductListViewController : UIViewController

@property (nonatomic, strong) RPProductList * productList;

@property (weak, nonatomic) IBOutlet UIView *emptyResultView;

@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *filtersCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *quickFilterLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *filterScrollView;

@property (weak, nonatomic) IBOutlet UIButton *sortButton;

@property (assign, nonatomic) BOOL isBrands;

@property (strong, nonatomic) RPMessage *message;

// параметры для товаров
@property (strong, nonatomic) NSMutableDictionary *params;

// параметры для фильтров (могут отсутствовать)
@property (strong, nonatomic) NSDictionary  *filterParams;

// текущая страница с подсказками, котрую нужно показывать, по умолчанию = 1
@property (assign, nonatomic) NSInteger currentPage;

- (RPFilter *)quickFilter;

- (void)reloadProductListWithParams:(NSDictionary *)filterParams pop:(BOOL)pop;

@end
