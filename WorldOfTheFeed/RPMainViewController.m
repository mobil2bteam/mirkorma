#import "RPMainViewController.h"
#import "UIViewController+Alert.h"
#import "RPMain.h"
#import "RPOptions.h"
#import "RPServerManager.h"
#import "RPString+Convert.h"
#import "RPNewsCollectionViewCell.h"
#import "RPMenuCollectionViewCell.h"
#import "RPBlockCollectionViewCell.h"
#import "RPProductViewController.h"
#import "RPProductViewController.h"
#import "RPImageCollectionViewCell.h"
#import "UIViewController+ECSlidingViewController.h"
#import "RPSuggest.h"
#import "RPRouter.h"
#import <MagicalRecord/MagicalRecord.h>
#import "DBProduct+CoreDataClass.h"

static const NSTimeInterval RPAutoScrollDuration = 3.0;

@interface RPMainViewController ()<UIApplicationDelegate>

@property (strong, nonatomic) NSTimer *timer;

@end

@implementation RPMainViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bannersPageControl.numberOfPages = APP_DELEGATE.main.banners.count;
    
    [self.newsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPNewsCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPNewsCollectionViewCell class])];
    
    [self.blocksCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPBlockCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPBlockCollectionViewCell class])];

    [self.bannersCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class])];
    
    [self.advantageCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class])];
    
    [self.menuCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPMenuCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPMenuCollectionViewCell class])];
   
    [self.brandCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPImageCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPImageCollectionViewCell class])];
    
    // add logo to navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];
    
    // add observer to automatic update blocksCollectionView's height
    [self addObserver:self forKeyPath:@"blocksCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    // add observers to start/stop timer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:[UIApplication sharedApplication]];
    
    // add hamburger button
    UIBarButtonItem *hamburgerBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"hamburger"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonTapped:)];
    self.navigationItem.leftBarButtonItem = hamburgerBarButton;
    
    // add shppingCartBarButton
    UIBarButtonItem *shoppingCartBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"cart"] style:UIBarButtonItemStylePlain target:self action:@selector(shoppingCartButtonTapped:)];
    self.navigationItem.rightBarButtonItem = shoppingCartBarButton;
    [self updateViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    // update product's count in shopping cart
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu", (long)APP_DELEGATE.shoppingCartItemsCount];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initializeTimer];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"blocksCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:[UIApplication sharedApplication]];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"blocksCollectionView.contentSize"]) {
        CGFloat newHeight = self.blocksCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.blocksCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Methods

- (void)initializeTimer{
    // if we don't have banners than return, else run timer to autoscroll
    if (!APP_DELEGATE.main.banners.count) {
        return;
    }
    [self.timer invalidate];
    self.timer = nil;
    self.timer =  [NSTimer scheduledTimerWithTimeInterval:RPAutoScrollDuration
                                                   target:self
                                                 selector:@selector(startAutoScrollBrandCollectionView)
                                                 userInfo:nil
                                                  repeats:YES];
}

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (IBAction)shoppingCartButtonTapped:(id)sender {
    [RPRouter pushShoppingCartVC:self];
}

- (void)startAutoScrollBrandCollectionView{
    if (self.bannersPageControl.currentPage < APP_DELEGATE.main.banners.count - 1) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.bannersPageControl.currentPage + 1 inSection:0];
        self.bannersPageControl.currentPage = self.bannersPageControl.currentPage + 1;
        [self.bannersCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    } else {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        self.bannersPageControl.currentPage = 0;
        [self.bannersCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
}

// update all views
- (void)updateViews{
//    // if some structurs are empty than remove related views
//    if (!APP_DELEGATE.main.news.count) {
//        [self.newsLabel removeFromSuperview];
//        [self.newsCollectionView removeFromSuperview];
//    }
//    if (!APP_DELEGATE.main.banners.count) {
//        [self.bannersPageControl removeFromSuperview];
//        [self.bannersCollectionView removeFromSuperview];
//    }
//    if (!APP_DELEGATE.main.advantage.count) {
//        [self.advantageCollectionView removeFromSuperview];
//    }
//    // start timer
    [self initializeTimer];
//
//    // update main view
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.view layoutIfNeeded];
//    });
    self.scrollView.hidden = NO;
}

#pragma mark - Actions 

// [Помощь и контакты]
- (IBAction)contactsButtonPressed:(id)sender {
    [RPRouter pushContactsVC:self];
}

#pragma mark - RPCollectionViewCellProtocol

- (void)pressedOnBlock:(NSInteger)block andProduct: (NSInteger)product{
    RPLink *link = [RPLink initWithName:@"url_product"];
    RPParam *param = [[RPParam alloc]init];
    param.name = @"id";
    param.value = [NSString stringWithFormat:@"%ld", (long)APP_DELEGATE.main.blocks[block].items[product].ID];
    link.param = @[param];
    [[RPServerManager sharedManager]postRequestWithLink:link fromController:self];
}

#pragma mark - UIApplicationDelegate

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [self initializeTimer];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    [self.timer invalidate];
    self.timer = nil;
}

@end
