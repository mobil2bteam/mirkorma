//
//  FacebookActivity.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 2/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookActivity : UIActivity

@property (nonatomic, copy) void (^successCallback)(void);

@end
