//
//  RPPaymentVC.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 1/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

static NSString * const kSuccessUrl = @"http://www.mirkorma.ru/personal/order/payment/finish.php";

#import "RPPaymentVC.h"
#import "RPUserInfo.h"

@interface RPPaymentVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation RPPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // add logo to navigation item
    UIImage* logoImage = [UIImage imageNamed:@"Logo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:logoImage];

    // add close UIBarButtonItem
    UIBarButtonItem *closeBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped:)];
    self.navigationItem.leftBarButtonItem = closeBarButton;

    self.paymentURL = @"http://www.mirkorma.ru/app/payment.php?";
    NSString *urlString = [NSString stringWithFormat:@"%@token=%@&order_id=%ld",self.paymentURL, APP_DELEGATE.userInfo.token, (long)self.orderID];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    self.webView.delegate = self;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *URLString = [[request URL] absoluteString];
    if ([URLString hasPrefix:kSuccessUrl]) { // if payment was successful
        __weak typeof(self) weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            if (weakSelf.successCallback) {
                weakSelf.successCallback();
            }
        }];
        return NO;
    }
    return YES;
}

#pragma mark - Actions

- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
