//
//  RPCustomButton.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 8/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCustomButton.h"

@implementation RPCustomButton

- (void)drawRect:(CGRect)rect {
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 0.0);
    CGRect rrect = self.bounds;
    CGFloat radius = 5.0;
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
    
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx-self.length, miny, maxx, maxy, radius);
    CGContextAddArcToPoint(context, maxx-4, midy, maxx-4, maxy, radius);
    CGContextAddArcToPoint(context, maxx-self.length, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    CGContextSetFillColorWithColor(context,  self.contextColor.CGColor);
    CGContextDrawPath(context, kCGPathFillStroke);
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (!self.contextColor) {
        self.contextColor = self.backgroundColor;
        self.backgroundColor = [UIColor clearColor];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
