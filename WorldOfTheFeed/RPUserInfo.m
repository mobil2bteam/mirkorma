//
//  RPUserInfo.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 11/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPUserInfo.h"

@implementation RPUserInfo

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[ @"first_name", @"last_name", @"second_name", @"status", @"active", @"token", @"phone", @"email"]];
    }];
}

@end
