//
//  RPServerManager.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RPError.h"

@class RPMain;
@class RPLink;
@class RPOptions;
@class RPSuggest;
@class RPMessage;
@class RPUserInfo;
@class RPProductList;
@class RPOrderList;
@class RPOrderInfo;
@class RPDeliveryList;
@class RPNewsListServerModel;

@interface RPServerManager : NSObject

+ (RPServerManager*) sharedManager;

- (void)postSuggestWithLink:(RPLink *) link
                 onSuccess:(void(^)(RPSuggest *suggest, RPError *error)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRequestWithLink:(RPLink *)link fromController:(UIViewController *)controller;

- (void)postProductListParams:(NSDictionary *) params
                     isBrands:(BOOL)brands
                    onSuccess:(void(^)(RPProductList *productList, RPError *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postAddCommentWithParams:(NSDictionary *) params
                  onSuccess:(void(^)(RPMessage *message, RPError *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postLogInWithParams:(NSDictionary *) params
                       onSuccess:(void(^)(RPUserInfo *userInfo, NSString *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postSignInWithParams:(NSDictionary *) params
                  onSuccess:(void(^)(RPUserInfo *userInfo, NSString *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postOrderListOnSuccess:(void(^)()) success;

- (void)postOrderInfoWithID:(NSInteger) ID
                   onSuccess:(void(^)(RPOrderInfo *orderInfo, NSString *error)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

//- (void)postConfirmOrderWithParams:(NSMutableDictionary *) params
//                         validation:(BOOL)validation
//                  onSuccess:(void(^)(RPDeliveryList *orderInfo, NSString *error)) success
//                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postDeliveryListWithParams:(NSMutableDictionary *) params
                         onSuccess:(void(^)(RPDeliveryList *deliveryList, NSString *error)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postChangePasswordWithParams:(NSDictionary *) params
                         onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postFeedbackWithParams:(NSDictionary *) params
                           onSuccess:(void(^)(BOOL isSuccess, NSString *error)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)getOptionsOnSuccess:(void(^)()) success
                  onFailure:(void(^)()) failure;

- (void)getMessageListOnSuccess:(void(^)(RPNewsListServerModel *newsLsit, RPError *error)) success
                      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRepeatOrderWithID:(NSInteger) ID
                    onSuccess:(void(^)(RPOrderInfo *orderInfo, NSString *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

@end
