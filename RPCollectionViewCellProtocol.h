//
//  RPCollectionVeiwCellProtocol.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/21/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPCollectionViewCellProtocol <NSObject>

- (void)pressedOnBlock:(NSInteger)block andProduct: (NSInteger)product;

@end
