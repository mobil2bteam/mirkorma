//
//  Constants.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#ifndef Constants_h
#define Constants_h

//   CP-C6EQM-HLAHLUH

//------------------------------------------- API Keys ----------------------------------------------------

static NSString * const APIKeyCatalogue = @"url_catalogue";

static NSString * const APIKeyMain = @"url_main";

static NSString * const APIKeyOptions = @"url_options";

static NSString * const APIKeyProductComment = @"url_product_comment";

static NSString * const APIKeyProducts = @"url_products";

static NSString * const APIKeyBrands = @"url_brands";

static NSString * const APIKeyBrand = @"url_brand";

static NSString * const APIKeyLogin = @"url_user_login";

static NSString * const APIKeyRegistration = @"url_user_registration";

static NSString * const APIKeyFeedback = @"url_feedback";

static NSString * const APIKeyUserData = @"url_user_data";

static NSString * const APIKeyOrderList = @"url_user_order_list";

static NSString * const APIKeyOrderDelivery = @"url_order";

static NSString * const APIKeyOrder = @"url_user_order";

static NSString * const APIKeyLocations = @"url_locations";

//

//------------------------------------ Identifiers for keys------------------------------------------------

static NSString * const OptionsID = @"options";

static NSString * const MainID = @"main";

static NSString * const CatalogueID = @"catalog";

static NSString * const ProductsID = @"products";

static NSString * const BrandsID = @"brands";

static NSString * const SuggestID = @"suggest";

static NSString * const CacheID = @"cache";

static NSString * const ErrorID = @"error";

static NSString * const MessageID = @"message";

static NSString * const UserInfoID = @"user_info";

static NSString * const OrderInfoID = @"order_info";

static NSString * const OrderActiveList = @"order_active_list";

static NSString * const LocationsID = @"locations";

//


//--------------------------------------------- Defines ---------------------------------------------------

#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication]delegate])

#define UserDefaults [NSUserDefaults standardUserDefaults]

#define IDIOM UI_USER_INTERFACE_IDIOM()

#define IPAD UIUserInterfaceIdiomPad

#define RPLightBlueColor [UIColor colorWithRed:0.000 green:0.706 blue:0.831 alpha:1.0]

#define RPDarkBlueColor [UIColor colorWithRed:0.00 green:0.54 blue:0.63 alpha:1.0]

#define RPDimRedColor [UIColor colorWithRed:0.97 green:0.36 blue:0.38 alpha:1.0]

#define RPGreyColor [UIColor colorWithRed:0.600 green:0.600 blue:0.600 alpha:1.0]

#define kVKIdentifier @"5828252"

//


//------------------------------------------ Error Messages -----------------------------------------------

static NSString * const ErrorMessage = @"Произошла ошибка";

static NSString * const ErrorMessageEmptyTextField = @"Заполните все поля!";

static NSString * const RPIncorrectPhoneMessage = @"Номер телефона введен неправильно";

static NSString * const RPVKIdentifier = @"5828252";

static NSString * const RPAddressSite =  @"http://www.mirkorma.ru/app/";

//------------------------------------------ Error Messages -----------------------------------------------


static NSString * const RPWasFirstLoad = @"wasFirstLoad";

static NSString * const RPLoginKey = @"login";

static NSString * const RPPasswordKey = @"password";

static NSString * const RPTokenKey = @"token";

static NSString * const RPPhoneKey = @"phone";

static NSString * const RPPromoCodeKey = @"promo_code";

#endif /* Constants_h */
